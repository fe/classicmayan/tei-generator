package org.classicmayan.tools;

/**
 *
 */
public class TEIFileDescription {

  TEIinscriptionTitle teiTitleStmt;
  TEIsourceDescription sourceDesc = new TEIsourceDescription();
  PublicationStatement pubStmt = new PublicationStatement();
  SeriesStatement seriesStmt = new SeriesStatement();

  private final String fileDescriptionBeginn = "<fileDesc>";
  private final String fileDescriptionEnd = "</fileDesc>";

  private String title;
  private String transcriberName;

  /**
   * 
   */
  public TEIFileDescription() {

  }

  /**
   * @param title
   * @param transcriberName
   */
  public TEIFileDescription(String title, String transcriberName) {
    this.title = title;
    this.transcriberName = transcriberName;
    this.addTitleStatement(new TEIinscriptionTitle(title, this.getTranscriberName()));
  }

  /**
   * @param teiTitleStmt
   * @return
   */
  public TEIinscriptionTitle addTitleStatement(TEIinscriptionTitle teiTitleStmt) {
    this.teiTitleStmt = teiTitleStmt;
    return teiTitleStmt;
  }

  /**
   * @return
   */
  public TEIsourceDescription addSourceDescription() {
    return this.sourceDesc;
  }

  /**
   * @return
   */
  public String getTEIinscriptionsTitle() {
    return this.fileDescriptionBeginn
        + this.teiTitleStmt.getCompleteInscriptionTitleinXML()
        + this.pubStmt.getPublicationStatement()
        + this.seriesStmt.getSeriesStmt()
        + this.sourceDesc.getSourceDescription()
        + this.fileDescriptionEnd;
  }

  /**
   * @return
   */
  public String getTitle() {
    return this.title;
  }

  /**
   * @param title
   */
  public void setTitle(String title) {
    this.title = title;
  }

  /**
   * @return
   */
  public String getTranscriberName() {
    return this.transcriberName;
  }

  /**
   * @param transcriberName
   */
  public void setTranscriberName(String transcriberName) {
    this.transcriberName = transcriberName;
  }

}

