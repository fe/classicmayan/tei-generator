package org.classicmayan.tools;

public class Paragraph {

  private final String pargraphBegin = "<p>";
  private final String pargraphEnd = "</p>";
  MsDescription msDesc = new MsDescription();

  public MsDescription addMsDescription() {
    return this.msDesc;
  }

  public String getParagraph() {
    return this.pargraphBegin + this.msDesc.getMsDescription() + this.pargraphEnd;
  }

}
