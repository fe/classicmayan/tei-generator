package org.classicmayan.tools;

public class PublicationStatement {

  private final String publicationStatement = "<publicationStmt>\n" +
      "            <p>\n" +
      "               <listBibl>\n" +
      "                  <bibl xml:lang=\"en\">Digital Corpus of Classic Mayan. Edited by \"Textdatabase and\n"
      +
      "                     Dictionary of Classic Mayan\", Research Center of the North Rhine-Westphalian\n"
      +
      "                     Academy of Sciences, Humanities and the Arts. http://www.classicmayan.org,\n"
      +
      "                     [year], accessed online: [date of access]</bibl>\n" +
      "                  <bibl xml:lang=\"de\">Digitales Korpus des Klassischen Maya. Herausgegeben von\n"
      +
      "                     \"Textdatenbank und Wörterbuch des Klassischen Maya\", Arbeitsstelle der\n"
      +
      "                     Nordrhein-Westfälischen Akademie der Wissenschaft und Künste.\n" +
      "                     http://www.classicmayan.org, [Jahr], abgerufen am: [Datum des Abrufs]</bibl>\n"
      +
      "                  <bibl xml:lang=\"es\">Corpus digital del Maya Clásico. Editado por el centro de\n"
      +
      "                     investigación \"Base de Datos de Texto y Diccionario del Maya Clásico\" de la\n"
      +
      "                     Academia de Renania del Norte-Westfalia de las Ciencias y de las Artes.\n"
      +
      "                     http://www.classicmayan.org, [año], último acceso: [fecha del acceso]</bibl>\n"
      +
      "                  <bibl>\n" +
      "                     <availability>\n" +
      "                        <licence target=\"http://creativecommons.org/licenses/by/4.0/\">The corpus is\n"
      +
      "                           available under licence \"CC BY 4.0\". You are free to share (copy and\n"
      +
      "                           redistribute the material in any medium or format) and adapt (remix,\n"
      +
      "                           transform, and build upon the material for any purpose, even\n" +
      "                           commercially) under the following terms: Attribution: You must give\n"
      +
      "                           appropriate credit, provide a link to the license, and indicate if\n"
      +
      "                           changes were made. You may do so in any reasonable manner, but not in any\n"
      +
      "                           way that suggests the licensor endorses you or your use. No additional\n"
      +
      "                           restrictions: You may not apply legal terms or technological measures\n"
      +
      "                           that legally restrict others from doing anything the license permits.\n"
      +
      "                           Note that the images are licenced differently. </licence>\n" +
      "                     </availability>\n" +
      "                  </bibl>\n" +
      "               </listBibl>\n" +
      "            </p>\n" +
      "         </publicationStmt>";

  /**
   * @return
   */
  public String getPublicationStatement() {
    return this.publicationStatement;
  }

}
