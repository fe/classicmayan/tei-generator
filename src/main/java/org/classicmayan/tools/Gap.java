package org.classicmayan.tools;

public class Gap {

  private String reason;
  private String quantity;
  private String extent;
  private String unit;

  public Gap(String reason, String extent, String quantity, String unit) {
    this.reason = reason;
    this.extent = extent;
    this.quantity = quantity;
    this.unit = unit;
  }

  public Gap(String reason, String extent, String unit) {
    this.reason = reason;
    this.extent = extent;
    this.unit = unit;
  }

  public String getGap() {
    if (getQuantity() == null) {
      return "<gap reason=\"" + this.reason + "\""
          + " extent=\"" + this.extent + "\""
          + " unit=\"" + this.unit + "\"/>";
    } else {
      return "<gap reason=\"" + this.reason + "\""
          + " extent=\"" + this.extent + "\""
          + " quantity=\"" + this.quantity + "\""
          + " unit=\"" + this.unit + "\"/>";
    }
  }

  public String getReason() {
    return this.reason;
  }

  public void setReason(String reason) {
    this.reason = reason;
  }

  public String getQuantity() {
    return this.quantity;
  }

  public void setQuantity(String quantity) {
    this.quantity = quantity;
  }

  public String getUnit() {
    return this.unit;
  }

  public void setUnit(String unit) {
    this.unit = unit;
  }

  public String getExtent() {
    return this.extent;
  }

  public void setExtent(String extent) {
    this.extent = extent;
  }

}
