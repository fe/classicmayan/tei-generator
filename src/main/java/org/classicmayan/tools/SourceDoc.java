package org.classicmayan.tools;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class SourceDoc {

  private List<Surface> surfaces = new ArrayList<Surface>();

  /**
   * @param surface
   * @return
   */
  public List<Surface> addSurface(Surface surface) {
    this.surfaces.add(surface);
    return this.surfaces;
  }

  /**
   * @return
   */
  public String getSourceDocInXML() {

    String sourceDoc = "<sourceDoc>";
    for (Surface surface : this.surfaces) {
      sourceDoc = sourceDoc.concat(surface.getSurfaceXML());
    }

    return sourceDoc + "</sourceDoc>";
  }

}
