package org.classicmayan.tools;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 *
 */
public class Transcription {

  public List<String> elementsOfTranscriptionCode = new ArrayList<String>();
  private String TranscriptionCode;
  private String glyphBlockID;
  private String teiCode;

  private int glyphCounter = 0;
  private int segmentCounter = 0;

  private boolean isRenditionOperatorValid;

  /**
   * Constructor of the transcription class.
   * 
   * @param numericalTransliteration contains the numerical transliteration
   */
  public Transcription(String numericalTransliteration) {
    this.TranscriptionCode = numericalTransliteration;
  }

  /**
   * Checks if the numerical transliteration is valid. If not returns XML-code containing the error.
   * If valid start the calculation for each element of the numerical transliteration in the
   * List<String>
   * 
   * @return
   */
  public Transcription parseTranscriptionCode() {

    // split the single elements of the numerical transliteration and save them into the
    // List<String>
    this.elementsOfTranscriptionCode = ParserUtilities.unitDivider(this.TranscriptionCode);

    String teiCode = "";
    int indexOfActualElementGoingToParse = 0;
    // Check if the numerical transliteration is valid
    ParserError parserError = new ParserError();
    parserError = ParserUtilities.validateNumericalTransliteration(this.TranscriptionCode);
    if (!parserError.isNumericalTransliterationValid()) {
      this.teiCode = parserError.getParserError();
      return this;
    } else {
      // start parsing the numerical transliteration
      for (String element : this.elementsOfTranscriptionCode) {

        // the actual element indicates a gap on the text carrier
        if (element.equals("#") || element.equals("…")) {
          teiCode = teiCode.concat(addGap(indexOfActualElementGoingToParse));
        }
        // the actual element indicates the beginning of a segment
        if (element.equals("[")) {
          teiCode = teiCode.concat(addSegmentObject(indexOfActualElementGoingToParse));
        }
        // if an element has has just alphanumeric signs, a glyph will be added to the tei code
        if (ParserUtilities.isAlphaNumeric(element)) {
          teiCode = teiCode.concat(addGlyphObject(indexOfActualElementGoingToParse));
        }
        // indicdates the end of a segment
        if (element.endsWith("]")) {
          teiCode = teiCode.concat(closeSegment());
        }
        // a single questionmark for a sign indicates a damage on the text carrier
        // prefixes and suffixes also indicates those damages
        if (element.equals("?") || element.startsWith("*") || element.startsWith("??")
            || element.endsWith("?")) {
          teiCode = teiCode.concat(addDamage(indexOfActualElementGoingToParse));
        }
        // the actual index is important to get the index of the List<String>
        // to all other function working on the same index
        indexOfActualElementGoingToParse++;
      }
    }
    this.teiCode = teiCode;

    return this;
  }

  /**
   * @param i
   * @return
   */
  public String addSegmentObject(int i) {

    int renditionCheckIndexForSegment = 0;
    String rendition;
    this.segmentCounter++;
    // if a segment is opened and the correspondent element for the segment comes after the segment
    // i.e.: [134st:12so].678po

    if (i == 0 && this.elementsOfTranscriptionCode.get(i).equals("[")) {
      int j = 0;
      do {
        j++;
      } while (!this.elementsOfTranscriptionCode.get(j).equals("]"));
      renditionCheckIndexForSegment =
          ParserUtilities.getRenditionIndex(this.elementsOfTranscriptionCode,
              renditionCheckIndexForSegment);
      rendition = ParserUtilities.checkRendition(this.elementsOfTranscriptionCode,
          renditionCheckIndexForSegment);
    } else {
      // System.out.println(i);
      renditionCheckIndexForSegment =
          ParserUtilities.getRenditionIndex(this.elementsOfTranscriptionCode, i);;
      rendition = ParserUtilities.checkRendition(this.elementsOfTranscriptionCode,
          renditionCheckIndexForSegment);
    }

    // rendition =
    // ParserUtilities.checkRendition(elementsOfTranscriptionCode,renditionCheckIndexForSegment);
    // System.out.println(elementsOfTranscriptionCode.get(renditionCheckIndexForSegment));
    // System.out.println(ParserUtilities.getRenditionIndex(elementsOfTranscriptionCode,
    // renditionCheckIndexForSegment));
    // System.out.println(ParserUtilities.checkRendition(elementsOfTranscriptionCode,renditionCheckIndexForSegment));

    /*
     * if(i==0 && elementsOfTranscriptionCode.get(i).equals("[")) { rendition =
     * ParserUtilities.checkRendition(elementsOfTranscriptionCode, renditionCheckIndexForSegment);
     * System.out.println(renditionCheckIndexForSegment); }else { rendition =
     * ParserUtilities.checkRendition(elementsOfTranscriptionCode,
     * ParserUtilities.getRenditionIndex(elementsOfTranscriptionCode,
     * renditionCheckIndexForSegment)); }
     */

    // System.out.println(ParserUtilities.checkRendition(elementsOfTranscriptionCode,
    // ParserUtilities.getRenditionIndex(elementsOfTranscriptionCode,
    // renditionCheckIndexForSegment)));
    // System.out.println(renditionCheckIndexForSegment + " || " +
    // ParserUtilities.checkRendition(elementsOfTranscriptionCode,
    // ParserUtilities.getRenditionIndex(elementsOfTranscriptionCode,
    // renditionCheckIndexForSegment)));

    // System.out.println(ParserUtilities.checkRendition(elementsOfTranscriptionCode,4));
    Segment segment =
        new Segment(getGlyphBlockID() + "S" + getSegmentCounter(), "glyph-group", rendition,

            // ParserUtilities.checkRendition(elementsOfTranscriptionCode,
            // ParserUtilities.getRenditionIndex(elementsOfTranscriptionCode,
            // renditionCheckIndexForSegment)),
            // ParserUtilities.checkRendition(elementsOfTranscriptionCode,
            // renditionCheckIndexForSegment),
            calculateCorresp(i));

    return segment.getTeiCode();
  }

  /**
   * @param i
   * @return
   */
  public String addGlyphObject(int i) {

    String ref = "";
    String damage = "";

    this.glyphCounter++;
    try {
      // Searching for a reference in Sign-Catalogue just possible for alphanumeric values. No
      // special sign are allowed
      if (ParserUtilities.isAlphaNumeric(this.elementsOfTranscriptionCode.get(i))) {

        String graphVariant = "";
        // To get the graph variation type the last two signs must must be taken from the string.
        // e.g. 512bt -> bt
        // So just string with length larger then two are possible.
        // e.g. 1b -> b is no graph variation type
        // e.g. 12 -> no graph variation type is attached

        // graphVariant =
        // elementsOfTranscriptionCode.get(i).substring(elementsOfTranscriptionCode.get(i).length()-2,
        // elementsOfTranscriptionCode.get(i).length() );
        if (ParserUtilities.isAlphaNumeric(this.elementsOfTranscriptionCode.get(i))) {
          if (this.elementsOfTranscriptionCode.get(i).length() > 2) {
            graphVariant = this.elementsOfTranscriptionCode.get(i).substring(
                this.elementsOfTranscriptionCode.get(i).length() - 2,
                this.elementsOfTranscriptionCode.get(i).length());
            // Graph variants consists strictly of alphabetic values no numbers or other sign.
            // if the garphVariant is alphabetic, the algorithm has to look for a graph in the sign
            // catalogue
          }
          if (ParserUtilities.isAlpha(graphVariant)) {
            ref = ParserUtilities.getTGURIforGraphNumber(this.elementsOfTranscriptionCode.get(i));
          }
          // In other cases no graph type is attached: e.g. 134 -> looking for a sign in the sign
          // catalogue
          else {
            ref = ParserUtilities.getSignNumberFromURI(this.elementsOfTranscriptionCode.get(i));
          }
        }
      }
    } catch (NoSuchElementException e) {
      // if the sign catalog has no entry for the given sign, the reference is not the URI but NA
      ref = "NA";
    }

    if (this.elementsOfTranscriptionCode.size() == 2) {
      // TODO
    }

    Glyph glyph = new Glyph(
        getGlyphBlockID() + "G" + getGlyphCounter(),
        this.elementsOfTranscriptionCode.get(i).replace("*", ""),
        ref,
        ParserUtilities.checkRendition(this.elementsOfTranscriptionCode, i),
        calculateCorresp(i));

    // A sign starting with an asterisk indicates a damage sign
    // and so the the glyph must be surrounded by the appropriate tags
    if (this.elementsOfTranscriptionCode.get(i).startsWith("*")) {
      damage = addDamage(i);
    }
    if (!damage.equals("")) {
      // TODO
    }

    return glyph.getTEICode();
  }

  public String closeSegment() {
    return "</seg>";
  }

  /**
   * @param i
   * @return
   */
  public String addGap(int i) {

    // a gap can contain one single block or more than one. In case of # it is just one block so the
    // unit is "g".
    // in case of "..." the gap contains more then one block so the unit is "ab"
    Gap gap;
    if (this.elementsOfTranscriptionCode.get(i).equals("#")) {
      gap = new Gap("", "unknown", "g");
    } else {
      gap = new Gap("", "unknown", "ab");
    }
    return gap.getGap();
  }

  /**
   * @param i
   * @return
   */
  public String addDamage(int i) {

    Damage damage;
    String ref = "";
    try {
      // the prefix * indicates just the damage to get URI from the sign catalog must be removed
      // i.e. *145zu -> 145zu
      String graphVariant = "";
      String signOrGraph =
          this.elementsOfTranscriptionCode.get(i).replace("*", "").replace("?", "");
      if (ParserUtilities.isAlphaNumeric(signOrGraph)) {
        if (signOrGraph.length() > 2) {
          graphVariant = signOrGraph.substring(signOrGraph.length() - 2, signOrGraph.length());
          // Graph variants consists strictly of alphabetic values no numbers or other sign.
          // if the garphVariant is alphabetic, the algorithm has to look for a graph in the sign
          // catalogue
        }
        if (ParserUtilities.isAlpha(graphVariant)) {

          ref = ParserUtilities.getTGURIforGraphNumber(signOrGraph);
        }
        // In other cases no graph type is attached: e.g. 134 -> looking for a sign in the sign
        // catalogue
        else {

          ref = ParserUtilities.getSignNumberFromURI(signOrGraph);
        }
      }

      // ref = ParserUtilities.getSignNumberFromURI(elementsOfTranscriptionCode.get(i).replace("*",
      // "").replace("?", ""));
    } catch (NoSuchElementException e) {
      ref = "NA";
    }

    // Different kinds of damage *, ?, ??, ...
    if (this.elementsOfTranscriptionCode.get(i).startsWith("*")) {
      damage = new Damage("", "1", "", "");
    } else {
      damage = new Damage("", "", "1", "g");
    }

    // if the damage sign is a single questionmark te glyp tag must be surrounded by
    // supplied and choice and at least two choices must be provided.
    // In case of more choices this must be added manually
    if (this.elementsOfTranscriptionCode.get(i).equals("?")) {
      this.glyphCounter++;
      damage
          .addSupplied(new Supplied("damage", "", ""))
          .addChoice(new Choice())
          .addSupplied(new Supplied(""))
          .addGlyph(new Glyph(getGlyphBlockID() + "G" + getGlyphCounter(),
              "",
              "",
              ParserUtilities.checkRendition(this.elementsOfTranscriptionCode, i),
              calculateCorresp(i)));
    }
    // in case of ?? no possibility for the sign is known. So no choice will be added
    else if (this.elementsOfTranscriptionCode.get(i).equals("??")) {
      this.glyphCounter++;
      if ((i + 1 < this.elementsOfTranscriptionCode.size()
          && this.elementsOfTranscriptionCode.get(i + 1).equals("-"))
          || i - 1 > 0 && this.elementsOfTranscriptionCode.get(i - 1).equals("-")) {
        damage.addSupplied(new Supplied("damage", "", "", "")).addGlyph(
            new Glyph(getGlyphBlockID() + "G" + getGlyphCounter(),
                "",
                "",
                ParserUtilities.checkRendition(this.elementsOfTranscriptionCode, i)));
      } else {
        damage.addGlyph(new Glyph(getGlyphBlockID() + "G" + getGlyphCounter(),
            ParserUtilities.checkRendition(this.elementsOfTranscriptionCode, i),
            calculateCorresp(i)));;
      }
    } else {
      this.glyphCounter++;
      damage.addSupplied(new Supplied("damage", "", "", "")).addGlyph(
          new Glyph(getGlyphBlockID() + "G" + getGlyphCounter(),
              this.elementsOfTranscriptionCode.get(i).replace("*", "").replace("?", ""),
              ref,
              ParserUtilities.checkRendition(this.elementsOfTranscriptionCode, i),
              calculateCorresp(i)));
    }

    return damage.getDamage();
  }

  /**
   * @param i
   * @return
   */
  public String calculateCorresp(int i) {

    String corresp = "#" + getGlyphBlockID();
    int signAmountInSegment =
        ParserUtilities
            .getQuantityOfGlyphsInSegment(ParserUtilities.unitDivider(this.TranscriptionCode))
            + this.getGlyphCounter() + 1;
    // System.out.println(elementsOfTranscriptionCode.get(i));

    if (i + 1 < this.elementsOfTranscriptionCode.size()) {

      if (this.elementsOfTranscriptionCode.get(i).equals("[")) {
        String correspGlyphOrSegment = "";
        // System.out.println(ParserUtilities.getCorrespondingForNestedSegments(elementsOfTranscriptionCode,
        // i, segmentCounter));

        if (i - 2 >= 0 && (ParserUtilities.isAlphaNumeric(
            this.elementsOfTranscriptionCode.get(i - 2).replace("*", "").replace("?", ""))
            || this.elementsOfTranscriptionCode.get(i - 2).equals("??"))) {

          correspGlyphOrSegment = "G" + this.getGlyphCounter();
        } else {
          correspGlyphOrSegment = ParserUtilities
              .getCorrespondingForNestedSegments(this.elementsOfTranscriptionCode, i,
                  this.segmentCounter);
          if (correspGlyphOrSegment.startsWith("G")) {

            return corresp + ParserUtilities
                .getCorrespondingForNestedSegments(this.elementsOfTranscriptionCode, i,
                    this.segmentCounter);
          }
        }

        return corresp + correspGlyphOrSegment;
      }
    }

    // Case that a segment is opened. The segment has the corresp the id of the first glyph outside
    // the segment.
    // Since this is not known at this moment the amount of the glyphs inside the segment will be
    // counted
    // incremented by one this is the id of the first glyph outside the segment
    // [2000st┌738st]:130bh -> the segment has two glyphs inside the segment and before the segment
    // is no glyph. The actual glyphCounter is 0 and the amount is two
    // so the next id is 3
    if (i + 1 < this.elementsOfTranscriptionCode.size()
        && this.elementsOfTranscriptionCode.get(i).equals("[")
        && !this.elementsOfTranscriptionCode.get(i + 1).equals("[")) {

      if (i - 2 > 0 && ((ParserUtilities
          .isAlphaNumeric(
              this.elementsOfTranscriptionCode.get(i + 1).replace("?", "").replace("*", ""))
          || this.elementsOfTranscriptionCode.get(i + 1).equals("["))
          && this.elementsOfTranscriptionCode.get(i + 3).equals("[")
          && !ParserUtilities.isAlphaNumeric(
              this.elementsOfTranscriptionCode.get(i - 2).replace("?", "").replace("*", "")))) {

        return corresp + "S"
            + Integer.toString(this.getSegmentCounter()
                - ParserUtilities
                    .amountOpenSegmentFromSpecificIndex(this.elementsOfTranscriptionCode, i)
                + 1);
      }

      if (i - 2 < 0 && this.elementsOfTranscriptionCode.get(i).equals("[")) {

        return corresp + "S" + Integer.toString(this.getSegmentCounter()
            + ParserUtilities
                .calculateRenditionOperatorForSegment(this.elementsOfTranscriptionCode));
      }

      // in the the case that the numerical transliteration contains subsegments the
      // first occurence of [ has not the same index then the last.
      // In this case the calculation of the correspendet is different
      if (this.TranscriptionCode.indexOf("[") != this.TranscriptionCode.lastIndexOf("[")) {

        signAmountInSegment = this.getGlyphCounter();
        return corresp + "G" + signAmountInSegment;
      }
      // case that a sign occures after a segment
      // [*1000st:713bb].181br
      if (this.elementsOfTranscriptionCode.indexOf("]") == this.elementsOfTranscriptionCode.size()
          - 2) {
        signAmountInSegment = this.getGlyphCounter();
        return corresp + "G" + signAmountInSegment;
      }

      // in this case the segment is surrounded by the glyphs
      // 5004st.[*1000st:713bb].181br
      if (i - 2 >= 0
          && i + 2 < this.elementsOfTranscriptionCode.size()
          && this.elementsOfTranscriptionCode.indexOf("]") + 2 < this.elementsOfTranscriptionCode
              .size()
          && (this.elementsOfTranscriptionCode
              .indexOf("]") == this.elementsOfTranscriptionCode.size() - 2
              || ParserUtilities.isAlphaNumeric(
                  this.elementsOfTranscriptionCode
                      .get(this.elementsOfTranscriptionCode.indexOf("]") + 2)
                      .replace("?", "").replace("*", "")))) {

        return corresp + "G" + this.getGlyphCounter();
      }

      return corresp + "G" + signAmountInSegment;
    }
    //
    if (i - 2 > 0
        && ParserUtilities
            .isAlphaNumeric(
                this.elementsOfTranscriptionCode.get(i).replace("*", "").replace("?", ""))
        && this.elementsOfTranscriptionCode.get(i - 2).equals("]")) {

      return corresp + "S"
          + Integer.toString(
              ParserUtilities
                  .amountClosedSegmentsTillSpecificIndex(this.elementsOfTranscriptionCode, i)
                  + 1);
    }
    // Case that a glyph has a follower divided by an operator
    // 524ex╝529st 529st is the follower of 524ex
    if (i + 2 < this.elementsOfTranscriptionCode.size()
        && !this.elementsOfTranscriptionCode.get(i + 2).equals("[")
        && ParserUtilities.isRenditionOperator((this.elementsOfTranscriptionCode.get(i + 1)))) {

      int signValueForCorresp = this.getGlyphCounter() + 1;
      return corresp + "G" + signValueForCorresp;
    }

    // Case that a glyph has a precursor divided by an operator
    // 524ex╝529st 524ex is the follower of 529st
    if (i - 2 >= 0
        // && (ParserUtilities.isAlphaNumeric(elementsOfTranscriptionCode.get(i-1)) ||
        // elementsOfTranscriptionCode.get(i-2).startsWith("*"))
        && !this.elementsOfTranscriptionCode.get(i - 2).equals("]")
        && ParserUtilities.isRenditionOperator((this.elementsOfTranscriptionCode.get(i - 1)))) {
      int signValueForCorresp = this.getGlyphCounter() - 1;
      return corresp + "G" + signValueForCorresp;
    }

    // Case the a glyph is followed by a segment
    // 59st.[169bt:188st:188st]
    if (i + 2 < this.elementsOfTranscriptionCode.size()
        && this.elementsOfTranscriptionCode.get(i + 2).equals("[")) {

      int signValueForCorresp = this.getSegmentCounter() + 1;

      return corresp + "S" + signValueForCorresp;
    }

    // In this case the next sign is not alphanumeric and idicates a damage with ?? so calculation
    // is different
    if (i + 2 < this.elementsOfTranscriptionCode.size()
        && this.elementsOfTranscriptionCode.get(i + 2).equals("??")) {
      int signValueForCorresp = this.getGlyphCounter() + 1;
      return corresp + "G" + signValueForCorresp;
    }
    // Same case as above but here from the other direction
    if (i - 2 > 0 && this.elementsOfTranscriptionCode.get(i - 2).equals("??")) {
      int signValueForCorresp = this.getGlyphCounter() - 1;
      return corresp + "G" + signValueForCorresp;
    }

    // Case that the glyph is last sign and the precursor is a segment
    // [2000st┌738st]:130bh
    if (i + 1 < this.elementsOfTranscriptionCode.size() && i - 2 > 0
        && this.elementsOfTranscriptionCode.get(i + 1).equals("+")
        && this.elementsOfTranscriptionCode.get(i - 2).equals("]")) {

      int signValueForCorresp = this.getSegmentCounter();
      return corresp + "S" + signValueForCorresp;
    }

    // Case that the sign is a damage sign is followed by a normal sign
    if (i + 2 < this.elementsOfTranscriptionCode.size()
        && (ParserUtilities.isAlphaNumeric(
            this.elementsOfTranscriptionCode.get(i + 2).replace("?", "").replace("*", ""))
            || ParserUtilities.isDamageSign(this.elementsOfTranscriptionCode.get(i)))) {
      int signValueForCorresp = this.getGlyphCounter() + 1;
      return corresp + "G" + signValueForCorresp;
    }

    // Case that the sign is a normal sign and is followed by a damage sign
    if (i - 2 >= 0
        && (ParserUtilities.isAlphaNumeric(
            this.elementsOfTranscriptionCode.get(i - 2).replace("?", "").replace("*", ""))
            || ParserUtilities.isDamageSign(this.elementsOfTranscriptionCode.get(i)))) {

      int signValueForCorresp = this.getGlyphCounter() - 1;
      return corresp + "G" + signValueForCorresp;
    }

    // Case that the sign is a damage sign identified by the question mark at the end
    if (this.elementsOfTranscriptionCode.get(i).endsWith("?")) {
      int signValueForCorresp = this.getGlyphCounter() - 1;
      return corresp + "G" + signValueForCorresp;
    }

    return corresp;
  }

  /**
   * Returns the numerical Transliteration
   * 
   * @return
   */
  public String getTranscriptionCode() {
    return this.TranscriptionCode;
  }

  /**
   * Set the numerical transliteration
   * 
   * @param TranscriptionCode
   * @return enable to call a function after another function in the class
   */
  public Transcription setTranscriptionCode(String TranscriptionCode) {
    this.TranscriptionCode = TranscriptionCode;
    return this;
  }

  /**
   * returns the ID of a GlyphBlock <ab> (NOT THE THE GLYPH!)
   * 
   * @return
   */
  public String getGlyphBlockID() {
    return this.glyphBlockID;
  }

  /**
   * set the the ID of a GlyphBlock <ab> (NOT THE THE GLYPH!)
   * 
   * @param glyphBlockID
   * @return enable to call a function after another function in the class
   */
  public Transcription setGlyphBlockID(String glyphBlockID) {
    this.glyphBlockID = glyphBlockID;
    return this;
  }

  /**
   * Returns the parsed numerical transliteration into TEI/XML
   * 
   * @return
   */
  public String getTeiCode() {
    return this.teiCode;
  }

  /**
   * 
   * @param teiCode
   * @return enable to call a function after another function in the class
   */
  public Transcription setTeiCode(String teiCode) {
    this.teiCode = teiCode;
    return this;
  }

  /**
   * Returns the single elements of the numerical transliteration as List<String>
   * 
   * @return
   */
  public List<String> getDividedTranscriptionCode() {
    return this.elementsOfTranscriptionCode;
  }

  /**
   * Returns the value of the glyphCounter to put it into the id of a glyph i.e. A1G3
   * 
   * @return
   */
  public int getGlyphCounter() {
    return this.glyphCounter;
  }

  /**
   * Set the initial value of the glyph counter. This function is not used
   * 
   * @param glyphCounter
   */
  public void setGlyphCounter(int glyphCounter) {
    this.glyphCounter = glyphCounter;
  }

  /**
   * Returns the value of the segment counter to put it into the id of a segment i.e. A1S2
   * 
   * @return
   */

  public int getSegmentCounter() {
    return this.segmentCounter;
  }

  /**
   * Set the initial value of the segment counter. This function is not used
   * 
   * @param segmentCounter
   */
  public void setSegmentCounter(int segmentCounter) {
    this.segmentCounter = segmentCounter;
  }

  /**
   * Return the value of a given rendition operator (divider of glyphs or segments) is an allowed
   * one
   * 
   * @return
   */
  public boolean isRenditionOperatorValid() {
    return this.isRenditionOperatorValid;
  }

  /**
   * Set the value if a rendition operator is valid or not
   * 
   * @param isRenditionOperatorValid
   */
  public void setRenditionOperatorValid(boolean isRenditionOperatorValid) {
    this.isRenditionOperatorValid = isRenditionOperatorValid;
  }

}
