package org.classicmayan.tools;

public class Note {

  private String id;
  private String noteContent;

  public Note() {}

  public String getId() {
    return this.id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getNoteContent() {
    return this.noteContent;
  }

  public void setNoteContent(String noteContent) {
    this.noteContent = noteContent;
  }

  public String getNote() {
    return "<note xml:id=\"" + getId() + "\">" + getNoteContent() + "</note>";
  }

}
