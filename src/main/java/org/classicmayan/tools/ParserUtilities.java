package org.classicmayan.tools;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.apache.cxf.helpers.IOUtils;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import info.textgrid.clients.CrudClient;
import info.textgrid.clients.tgcrud.CrudClientException;
import info.textgrid.clients.tgcrud.TextGridObject;

/**
 *
 */
public class ParserUtilities {

  /**
   * @param s
   * @return
   */
  public static boolean isAlphaNumeric(String s) {
    String pattern = "^[a-zA-Z0-9]*$";
    return s.matches(pattern);
  }

  /**
   * @param sign
   * @return
   */
  public static boolean isDamageSign(String sign) {
    String pattern = "^[??,?]*$";
    return sign.matches(pattern);
  }

  /**
   * @param sign
   * @return
   */
  public static boolean isRenditionOperator(String sign) {
    String allowedOperators = "^[.,:,+,°,^,┌,┐,└,┘,╝,╚,╗,╔,-]*$";
    return sign.matches(allowedOperators);
  }

  /**
   * @param transcriptionSegments
   * @param i
   * @return
   */
  public static String checkRendition(List<String> transcriptionSegments, int i) {

    if (i + 1 < transcriptionSegments.size() && transcriptionSegments.get(i + 1).equals("°")) {
      return "encloses";
    }
    if (i - 1 >= 0 && transcriptionSegments.get(i - 1).equals("°")) {
      return "infixed_in";
    }
    if (i + 1 < transcriptionSegments.size() && transcriptionSegments.get(i + 1).equals("+")) {
      return "conflated";
    }
    if (i - 1 >= 0 && transcriptionSegments.get(i - 1).equals("+")) {
      return "conflated";
    }
    if (i + 1 < transcriptionSegments.size() && transcriptionSegments.get(i + 1).equals(".")) {
      return "left_beside";
    }
    if (i - 1 >= 0 && transcriptionSegments.get(i - 1).equals(".")) {
      return "right_beside";
    }
    if (i + 1 < transcriptionSegments.size() && transcriptionSegments.get(i + 1).equals(":")) {
      return "above";
    }
    if (i - 1 >= 0 && transcriptionSegments.get(i - 1).equals(":")) {
      return "beneath";
    }
    if (i - 1 >= 0 && transcriptionSegments.get(i - 1).equals("~")) {
      return "amalganated";
    }
    if (i + 1 < transcriptionSegments.size() && transcriptionSegments.get(i + 1).equals("^")) {
      return "interferes_with";
    }
    if (i - 1 >= 0 && transcriptionSegments.get(i - 1).equals("^")) {
      return "is_interfered_by";
    }
    if (i + 1 < transcriptionSegments.size() && transcriptionSegments.get(i + 1).equals("┌")) {
      return "attached_at_top_left";
    }
    if (i - 1 >= 0 && transcriptionSegments.get(i - 1).equals("┌")) {
      return "is_adjoined_to";
    }
    if (i + 1 < transcriptionSegments.size() && transcriptionSegments.get(i + 1).equals("┐")) {
      return "attached_at_top_right";
    }
    if (i - 1 >= 0 && transcriptionSegments.get(i - 1).equals("┐")) {
      return "is_adjoined_to";
    }
    if (i + 1 < transcriptionSegments.size() && transcriptionSegments.get(i + 1).equals("└")) {
      return "attached_at_bottom_left";
    }
    if (i - 1 >= 0 && transcriptionSegments.get(i - 1).equals("└")) {
      return "is_adjoined_to";
    }
    if (i + 1 < transcriptionSegments.size() && transcriptionSegments.get(i + 1).equals("┘")) {
      return "attached_at_bottom_right";
    }
    if (i - 1 >= 0 && transcriptionSegments.get(i - 1).equals("┘")) {
      return "is_adjoined_to";
    }
    if (i + 1 < transcriptionSegments.size() && transcriptionSegments.get(i + 1).equals("╝")) {
      return "inserted_in_top_left_corner";
    }
    if (i - 1 >= 0 && transcriptionSegments.get(i - 1).equals("╝")) {
      return "is_bent_around";
    }
    if (i + 1 < transcriptionSegments.size() && transcriptionSegments.get(i + 1).equals("╚")) {
      return "inserted_in_top_right_corner";
    }
    if (i - 1 >= 0 && transcriptionSegments.get(i - 1).equals("╚")) {
      return "is_bent_around";
    }
    if (i + 1 < transcriptionSegments.size() && transcriptionSegments.get(i + 1).equals("╗")) {
      return "inserted_in_bottom_left_corner";
    }
    if (i - 1 >= 0 && transcriptionSegments.get(i - 1).equals("╗")) {
      return "is_bent_around";
    }
    if (i + 1 < transcriptionSegments.size() && transcriptionSegments.get(i + 1).equals("╔")) {
      return "inserted_in_bottom_right_corner";
    }
    if (i - 1 >= 0 && transcriptionSegments.get(i - 1).equals("╔")) {
      return "is_bent_around";
    }
    if (i + 1 < transcriptionSegments.size() && transcriptionSegments.get(i + 1).equals("-")) {
      return "unknown";
    }
    if (i - 1 >= 0 && transcriptionSegments.get(i - 1).equals("-")) {
      return "unknown";
    } else {
      return "N/A";
    }
  }

  /**
   * @param segment
   * @return
   */
  public static List<String> unitDivider(String segment) {

    segment = segment + "{";

    List<String> dividedTranscriptionCode = new ArrayList<String>();
    String graphNumber = "";
    dividedTranscriptionCode.clear();
    for (int i = 0; i < segment.length(); i++) {

      if (Character.isAlphabetic(segment.charAt(i)) ||
          Character.isDigit(segment.charAt(i)) ||
          String.valueOf(segment.charAt(i)).equals("*") ||
          String.valueOf(segment.charAt(i)).endsWith("?")) {
        graphNumber = graphNumber + segment.charAt(i);

      } else {
        if (graphNumber.length() > 0) {
          dividedTranscriptionCode.add(graphNumber);
          graphNumber = "";
        }
        if (String.valueOf(segment.charAt(i)).equals("?")
            && String.valueOf(segment.charAt(i + 1)).equals("?")) {
          dividedTranscriptionCode
              .add(String.valueOf(segment.charAt(i)) + String.valueOf(segment.charAt(i + 1)));
          i++;

        } else {
          dividedTranscriptionCode.add(new Character(segment.charAt(i)).toString());
        }
      }
    }

    return dividedTranscriptionCode;
  }

  /**
   * Querying the triplestore on the classicmayan Server for TextGrid.URI related to the graph
   * number inside the sign catalogue
   * 
   * @param graphNumber from the Transliteration Code
   * @return textGrid URI for the given graph object (by the graphNumber)
   */
  public static String getTGURIforGraphNumber(String graphNumber) {

    String query = "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
        "prefix idiomcat: <http://idiom-projekt.de/catalogue/>\n" +
        "\n" +
        "SELECT ?tgUri\n" +
        "  WHERE { GRAPH ?tgUri {\n" +
        "    ?tgObjects rdf:type idiomcat:Graph.\n" +
        "    ?tgObjects idiomcat:graphNumber \"" + graphNumber + "\";\n" +
        "  }\n" +
        "}";


    QueryExecution q = QueryExecutionFactory
        .sparqlService("https://www.classicmayan.org/trip/metadata/query", query);
    ResultSet results = q.execSelect();
    QuerySolution soln = results.next();
    String uriForGraphNumber = soln.get("tgUri").toString();

    return uriForGraphNumber;
  }

  /**
   * @param signNumber
   * @return
   */
  public static String getUriOfSignNumber(String signNumber) {

    String query =
        "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
            "prefix idiomcat: <http://idiom-projekt.de/catalogue/>\n" +
            "\n" +
            "SELECT ?tgUri\n" +
            "  WHERE { GRAPH ?tgUri {\n" +
            "    ?tgObjects rdf:type idiomcat:Sign.\n" +
            "    ?tgObjects idiomcat:signNumber \""
            + signNumber.substring(0, signNumber.length() - 2) + "\";\n" +
            "  }\n" +
            "}";

    QueryExecution q = QueryExecutionFactory
        .sparqlService("https://www.classicmayan.org/trip/metadata/query", query);
    ResultSet results = q.execSelect();
    QuerySolution soln = results.next();
    String uriForSignNumber = soln.get("tgUri").toString();

    return uriForSignNumber;
  }

  /**
   * @param name
   * @return
   */
  public static boolean isAlpha(String name) {
    return name.matches("[a-zA-Z]+");
  }

  /**
   * @param signNumber
   * @return
   */
  public static String getSignNumberFromURI(String signNumber) {

    String query = "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
        "prefix idiomcat: <http://idiom-projekt.de/catalogue/>\n" +
        "SELECT ?tgUri ?signNumber\n" +
        "            WHERE { GRAPH ?tgUri {\n" +
        "              ?sign rdf:type idiomcat:Sign .\n" +
        "    		   ?sign idiomcat:signNumber \"" + signNumber + "\" .\n" +
        "            }\n" +
        "    } ";

    QueryExecution q = QueryExecutionFactory
        .sparqlService("https://www.classicmayan.org/trip/metadata/query", query);
    ResultSet results = q.execSelect();
    QuerySolution soln = results.next();
    String tgURI = soln.get("tgUri").toString();

    return tgURI;
  }

  /**
   * @param transcriptionSegments
   * @return
   */
  /**
   * @param transcriptionSegments
   * @return
   */
  public static boolean validateClosedAndUnclosedSegments(List<String> transcriptionSegments) {

    int countBracketOpen = 0;
    int countBracketClosed = 0;

    for (String segment : transcriptionSegments) {
      if (segment.equals("[")) {
        countBracketOpen++;
      }
      if (segment.equals("]")) {
        countBracketClosed++;
      }
    }

    if (countBracketOpen > countBracketClosed) {
      return false;
    }
    if (countBracketOpen < countBracketClosed) {
      return false;
    } else {
      return true;
    }

  }

  /**
   * @param transcriptionSegments
   * @return
   */
  public static ParserError validateNoEnclosingBracketsForWholeTranscriptionCode(
      List<String> transcriptionSegments) {

    ParserError parserError = new ParserError();
    parserError.setNumericalTransliterationValid(true);

    int correspondingSegmentNumber =
        getIndexOfCorrespondingForNestedSegments(transcriptionSegments, 0, 1);
    // int amountOfOpenSegments = countInnerSegments(transcriptionSegments);

    if (correspondingSegmentNumber == transcriptionSegments.size() - 2) {
      parserError.setNumericalTransliterationValid(false);
      parserError
          .setReason("Enclosing segment brackets for whole transnumerical Literation not allowed");
    }

    return parserError;
  }

  /**
   * @param transcriptionSegments
   * @return
   */
  public static ParserError validateRenditionOperator(List<String> transcriptionSegments) {

    ParserError parserError = new ParserError();
    String allowedOperators = "^[.,:,+,°,^,┌,┐,└,┘,╝,╚,╗,╔,\\-,{]*$";
    parserError.setNumericalTransliterationValid(true);
    for (String segment : transcriptionSegments) {
      if (!isAlphaNumeric(segment)
          && !isDamageSign(segment)
          && !segment.matches(allowedOperators)
          && !segment.equals("[")
          && !segment.equals("]")
          && !segment.equals("…")
          && !segment.equals("#")
          && !segment.startsWith("*")
          && !segment.endsWith("?")) {
        parserError.setNumericalTransliterationValid(false);
        parserError.setReason(segment);

        return parserError;
      }
    }

    return parserError;
  }

  /**
   * @param transcriptionSegments
   * @return
   */
  public static ParserError validateMissingOperator(List<String> transcriptionSegments) {

    ParserError parserError = new ParserError();
    parserError.setNumericalTransliterationValid(true);
    for (int i = 0; i < transcriptionSegments.size(); i++) {
      if ((isAlphaNumeric(transcriptionSegments.get(i)) || transcriptionSegments.get(i).equals("?"))
          && transcriptionSegments.get(i + 1).equals("[")) {
        parserError.setNumericalTransliterationValid(false);
        parserError.setReason(" between " + transcriptionSegments.get(i) + " and "
            + transcriptionSegments.get(i + 1));
        return parserError;
      }
      if (isAlphaNumeric(transcriptionSegments.get(i))) {
        String segment = transcriptionSegments.get(i);
        for (int j = 0; j < segment.length(); j++) {
          if (j + 1 < segment.length() && Character.isAlphabetic(segment.charAt(j))
              && Character.isDigit(segment.charAt(j + 1))) {
            parserError.setNumericalTransliterationValid(false);
            parserError.setReason(" in " + segment);
          }
        }
      }
      if (transcriptionSegments.get(i).equals("]")
          && transcriptionSegments.get(i + 1).equals("?")) {
        parserError.setNumericalTransliterationValid(false);
        parserError.setReason(" between " + transcriptionSegments.get(i) + " and "
            + transcriptionSegments.get(i + 1));
      }
    }

    return parserError;
  }

  /**
   * @param transcriptionSegments
   * @return
   */
  public static boolean validateMissingSign(List<String> transcriptionSegments) {

    for (int i = 0; i < transcriptionSegments.size(); i++) {
      if (i + 1 < transcriptionSegments.size() && isRenditionOperator(transcriptionSegments.get(i))
          && transcriptionSegments.get(i + 1).equals("]")) {
        return false;
      }
    }

    return true;
  }

  /**
   * @param transcriptionSegments
   * @return
   */
  public static boolean validateSignIsEnteredInSignCatalogue(List<String> transcriptionSegments) {

    for (String segment : transcriptionSegments) {
      if (isAlphaNumeric(segment.replace("?", "")) && !segment.equals("??")) {
        try {
          getTGURIforGraphNumber(segment);
        } catch (NoSuchElementException e) {
          return false;
        }
      }
    }

    return true;
  }

  /**
   * @param transcriptionSegments
   * @return
   */
  public static ParserError validateGraphIsEnteredInSignCatalogue(
      List<String> transcriptionSegments) {

    ParserError parserError = new ParserError();
    parserError.setNumericalTransliterationValid(true);
    for (String segment : transcriptionSegments) {
      if (!segment.equals("?") && isAlphaNumeric(segment.replace("?", ""))
          && !segment.equals("??")) {
        try {
          getTGURIforGraphNumber(segment);
          getUriOfSignNumber(segment);
        } catch (NoSuchElementException e) {
          parserError.setNumericalTransliterationValid(false);
          parserError.setReason(segment);
          return parserError;
        }
      }
    }

    return parserError;
  }

  /**
   * @param transcriptionSegments
   * @return
   */
  public static boolean getQuantityOfSignsDividedByDifferentOperatorsOutsideSegments(
      List<String> transcriptionSegments) {

    for (int i = 0; i < transcriptionSegments.size(); i++) {
      if (i - 2 >= 0 && i + 2 < transcriptionSegments.size()) {
        if (isAlphaNumeric(transcriptionSegments.get(i))
            && isAlphaNumeric(transcriptionSegments.get(i - 2))
            && isAlphaNumeric(transcriptionSegments.get(i + 2))) {
          if (i - 3 >= 0) {
            if (!transcriptionSegments.get(i + 1).equals(transcriptionSegments.get(i - 1))
                && (!transcriptionSegments.get(i - 3).equals("["))) {
              return false;
            }
          } else if (i - 2 >= 0) {
            if (!transcriptionSegments.get(i + 1).equals(transcriptionSegments.get(i - 1))) {
              return false;
            }
          }
        }
      }
    }

    return true;
  }

  /**
   * @param transcriptionSegments
   */
  public static void getIndexOfClosingSignOfOpenSegment(List<String> transcriptionSegments) {

    int openSegments = 0;
    int closingSegments = 0;

    int i = 0;

    do {
      if (transcriptionSegments.get(i).equals("[")) {
        openSegments++;
      }
      if (transcriptionSegments.get(i).equals("]")) {
        closingSegments++;
      }
      i++;
      // indexOfClosignBracket++;
    } while (closingSegments == openSegments);
  }

  /**
   * @param transcriptionSegments
   * @return
   */
  public static int calculateRenditionOperatorForSegment(List<String> transcriptionSegments) {

    int indexOfFirstClosingSegment = 0;
    int openSegments = 0;

    for (indexOfFirstClosingSegment = 0; indexOfFirstClosingSegment <= transcriptionSegments
        .indexOf("]"); indexOfFirstClosingSegment++) {
      if (transcriptionSegments.get(indexOfFirstClosingSegment).equals("[")) {
        openSegments++;
      }
    }

    return openSegments;
  }

  /**
   * @param transcriptionSegments
   * @param i
   * @return
   */
  public static int amountOpenSegmentFromSpecificIndex(List<String> transcriptionSegments, int i) {

    int indexOfFirstOpenSegment = i;
    int openSegments = 0;

    for (; indexOfFirstOpenSegment >= transcriptionSegments
        .indexOf("["); indexOfFirstOpenSegment++) {
      if (indexOfFirstOpenSegment < transcriptionSegments.size()
          && transcriptionSegments.get(indexOfFirstOpenSegment).equals("[")) {
        openSegments++;
      }
    }

    return openSegments;
  }

  /**
   * @param transcriptionSegments
   * @param i
   * @return
   */
  public static int amountClosedSegmentsTillSpecificIndex(List<String> transcriptionSegments,
      int i) {

    int indexOfFirstOpenSegment = i;
    int openSegments = 0;

    int indexOfClosedSegment = transcriptionSegments.indexOf("]");
    for (; indexOfFirstOpenSegment >= indexOfClosedSegment; indexOfFirstOpenSegment++) {
      if (indexOfFirstOpenSegment < transcriptionSegments.size()
          && transcriptionSegments.get(indexOfFirstOpenSegment).equals("]")) {
        openSegments++;
      }
    }

    return openSegments;
  }


  /**
   * @param transcriptionSegments
   * @return
   */
  public static int countInnerSegments(List<String> transcriptionSegments) {

    int amountOfOpenSegments = 0;

    for (int i = 0; i < transcriptionSegments.size(); i++) {
      if (transcriptionSegments.get(i).equals("[")) {
        amountOfOpenSegments++;
      }
    }

    return amountOfOpenSegments;
  }

  /**
   * @param transcriptionSegments
   * @return
   */
  public static List<List<String>> cutSegments(List<String> transcriptionSegments) {

    List<List<String>> bla = new ArrayList<List<String>>();

    int amountOfSegments = amountOpenSegmentFromSpecificIndex(transcriptionSegments, 0);
    for (int j = 0; j < amountOfSegments; j++) {
      List<String> segment = new ArrayList<String>();
      for (int i = 0; i < transcriptionSegments.size(); i++) {
        if (!transcriptionSegments.get(i).equals("{")) {
          segment.add(transcriptionSegments.get(i));
        }
      }
      bla.add(segment);
    }

    return bla;

  }

  /**
   * @param transcriptionSegments
   * @return
   */
  public static int getQuantityOfGlyphsInSegment(List<String> transcriptionSegments) {

    int indexOfSegmentBegin = transcriptionSegments.indexOf("[");
    int indexOfSegmentEnd = transcriptionSegments.indexOf("]");
    int glyphCountInsideSegment = 0;

    for (int i = indexOfSegmentBegin; i < indexOfSegmentEnd; i++) {
      if (isAlphaNumeric(transcriptionSegments.get(i))) {
        glyphCountInsideSegment++;
      }
    }

    return glyphCountInsideSegment;
  }

  /**
   * @param tgUri
   * @param sid
   * @return
   * @throws CrudClientException
   * @throws IOException
   */
  public static TextGridObject getTextGridObject(String tgUri, String sid)
      throws CrudClientException, IOException {

    CrudClient crudclient = new CrudClient(CrudClient.DEFAULT_ENDPOINT)
        .enableGzipCompression();

    TextGridObject tgobj = crudclient.read().setSid(sid)
        .setTextgridUri(tgUri).execute();

    return tgobj;
  }

  /**
   * @param transcriptionCode
   * @return
   */
  public static ParserError validateNumericalTransliteration(String transcriptionCode) {

    ParserError parserError = new ParserError();
    List<String> segmentedTranscriptionCode = ParserUtilities.unitDivider(transcriptionCode);
    // ParserError parserErrorForGraphVariant =
    // validateGraphIsEnteredInSignCatalogue(segmentedTranscriptionCode);
    ParserError parserErrorForMissingOperator = validateMissingOperator(segmentedTranscriptionCode);
    ParserError parserErrorForInvalidRenditionOperator =
        validateRenditionOperator(segmentedTranscriptionCode);
    String errorCode = "";
    parserError.setNumericalTransliterationValid(true);

    if (unitDivider(transcriptionCode).get(0).equals("[")) {
      ParserError parserErrorForEnclosingSegmentBrackets =
          validateNoEnclosingBracketsForWholeTranscriptionCode(segmentedTranscriptionCode);
      if (!parserErrorForEnclosingSegmentBrackets.isNumericalTransliterationValid()) {
        errorCode =
            errorCode.concat("<error>BLOCK NOT PARSED: Segment brackets are not set correctly:  "
                + parserErrorForEnclosingSegmentBrackets.getReason() + " </error>");
        parserError.setNumericalTransliterationValid(false);
      }
    }
    if (!parserErrorForInvalidRenditionOperator.isNumericalTransliterationValid()) {
      errorCode = errorCode.concat("<error>BLOCK NOT PARSED: Invalid graphotactical operator set! "
          + parserErrorForInvalidRenditionOperator.getReason() + " </error>");
      parserError.setNumericalTransliterationValid(false);
    }
    if (!ParserUtilities
        .validateClosedAndUnclosedSegments(ParserUtilities.unitDivider(transcriptionCode))) {
      errorCode = errorCode
          .concat("<error>BLOCK NOT PARSED: Segment brackets are not set correctly!</error>");
      parserError.setNumericalTransliterationValid(false);
    }
    if (!parserErrorForMissingOperator.isNumericalTransliterationValid()) {
      errorCode =
          errorCode.concat("<error>BLOCK NOT PARSED: Operator between signs or segments missing!"
              + parserErrorForMissingOperator.getReason() + " </error>");
      parserError.setNumericalTransliterationValid(false);
    }
    if (!ParserUtilities.validateMissingSign(ParserUtilities.unitDivider(transcriptionCode))) {
      errorCode = errorCode
          .concat("<error>BLOCK NOT PARSED: Graph number is missing between operators!</error>");
      parserError.setNumericalTransliterationValid(false);
    }
    if (!ParserUtilities.getQuantityOfSignsDividedByDifferentOperatorsOutsideSegments(
        ParserUtilities.unitDivider(transcriptionCode))) {
      errorCode =
          errorCode.concat("<error>BLOCK NOT PARSED: Segment brackets are missing!</error>");
      parserError.setNumericalTransliterationValid(false);
    }
    parserError.setParserError(errorCode);

    return parserError;
  }

  /**
   * This function creates an array with all indices of the closing
   * 
   * @param transcriptionSegments
   * @return
   */
  public static List<Integer> getAllIndicesOfClosingSegmentSigns(
      List<String> transcriptionSegments) {

    List<Integer> segmentIndices = new ArrayList<Integer>();
    for (int i = 0; i < transcriptionSegments.size(); i++) {
      if (transcriptionSegments.get(i).equals("]")) {
        segmentIndices.add(i);
      }
    }

    return segmentIndices;
  }

  /**
   * This function creates an array with all indices of the opening segment sign
   * 
   * @param transcriptionSegments
   * @return
   */
  public static List<Integer> getAllIndicesOfOpeningSegmentSigns(
      List<String> transcriptionSegments) {

    List<Integer> segmentIndices = new ArrayList<Integer>();
    for (int i = 0; i < transcriptionSegments.size(); i++) {
      if (transcriptionSegments.get(i).equals("]")) {
        segmentIndices.add(i);
      }
    }

    return segmentIndices;
  }

  /**
   * @param transcriptionSegments
   * @param limit
   * @return
   */
  public static int countSegmentsTillSpecificIndex(List<String> transcriptionSegments, int limit) {

    int openSegmentsTillLimitInTranscriptionStringList = 0;

    for (int i = 0; i < limit; i++) {
      if (transcriptionSegments.get(i).equals("[")) {
        openSegmentsTillLimitInTranscriptionStringList++;
      }
    }

    return openSegmentsTillLimitInTranscriptionStringList;
  }

  /**
   * @param transcriptionSegments
   * @param begin
   * @param segmentNumber
   * @return
   */
  public static String getCorrespondingForNestedSegments(List<String> transcriptionSegments,
      int begin, int segmentNumber) {

    int amountClosedSegments = 0;
    int amountOpenSegments = 0;

    int i = begin;

    if (!(isRenditionOperator(transcriptionSegments.get(i))) &&
        !(isAlphaNumeric(transcriptionSegments.get(i).replace("*", ""))) &&
        !(transcriptionSegments.get(i).equals("{")) &&
        !(i - 2 > 0 && transcriptionSegments.get(i).equals("[")
            && transcriptionSegments.get(i - 2).equals("]"))) {
      do {
        if (transcriptionSegments.get(i).equals("[")) {
          amountOpenSegments++;
        }
        if (transcriptionSegments.get(i).equals("]")) {
          amountClosedSegments++;
        }
        if (amountOpenSegments == amountClosedSegments) {

          if (begin - 2 > 0 && isAlphaNumeric(transcriptionSegments.get(begin - 2)) ||
              (amountOpenSegments == 1 && transcriptionSegments.get(i + 1).equals("]"))
                  && (!transcriptionSegments.get(i + 2).equals("["))) {
            return "G";
          }

          if (begin - 1 >= 0 && transcriptionSegments.get(begin - 1).equals("[")
              && isAlphaNumeric(transcriptionSegments
                  .get(
                      getIndexOfCorrespondingForNestedSegments(transcriptionSegments, begin, 1) + 2)
                  .replace("*", ""))) {
            // System.out.println(getIndexOfCorrespondingForNestedSegments(transcriptionSegments,
            // begin, 1));
            // System.out.println(countGlyphTillIndex(transcriptionSegments,getIndexOfCorrespondingForNestedSegments(transcriptionSegments,
            // begin, 1)));
            return "G" + Integer.toString(countGlyphTillIndex(transcriptionSegments,
                getIndexOfCorrespondingForNestedSegments(transcriptionSegments, begin, 1)));
          }
          if (begin == 0 && transcriptionSegments.get(begin).equals("[")
              && isAlphaNumeric(transcriptionSegments
                  .get(
                      getIndexOfCorrespondingForNestedSegments(transcriptionSegments, begin, 1) + 2)
                  .replace("*", ""))) {
            // System.out.println(getIndexOfCorrespondingForNestedSegments(transcriptionSegments,
            // begin, 1));
            // System.out.println(countGlyphTillIndex(transcriptionSegments,getIndexOfCorrespondingForNestedSegments(transcriptionSegments,
            // begin, 1)));
            return "G" + Integer.toString(countGlyphTillIndex(transcriptionSegments,
                getIndexOfCorrespondingForNestedSegments(transcriptionSegments, begin, 1)));
          } else {

            return "S"
                + Integer.toString(countSegmentsTillSpecificIndex(transcriptionSegments, i) + 1);
          }
        }
        i++;
      } while (i < transcriptionSegments.size());
    } else {

      int openSegments = 0;
      int closedSegments = 0;

      for (int j = i - 1; j >= 0; j--) {
        if (transcriptionSegments.get(j).equals("[")) {
          openSegments++;
        }
        if (transcriptionSegments.get(j).equals("]")) {
          closedSegments++;
        }

        if (j != i - 1 && closedSegments == openSegments) {

          return "S"
              + Integer.toString(countSegmentsTillSpecificIndex(transcriptionSegments, j) + 1);
        }
      }
    }

    return "";
  }

  /**
   * @param transcriptionSegments
   * @param limit
   * @return
   */
  public static int countGlyphTillIndex(List<String> transcriptionSegments, int limit) {
    int glyphCount = 0;

    for (int i = 0; i < limit; i++) {
      if (isAlphaNumeric(transcriptionSegments.get(i).replace("*", ""))) {
        glyphCount++;
      }
    }

    if (isAlphaNumeric(transcriptionSegments.get(limit + 2).replace("*", ""))) {
      return ++glyphCount;
    }
    return glyphCount;
  }

  /**
   * @param transcriptionSegments
   * @param begin
   * @param segmentNumber
   * @return
   */
  public static int getIndexOfCorrespondingForNestedSegments(List<String> transcriptionSegments,
      int begin, int segmentNumber) {

    int amountClosedSegments = 0;
    int amountOpenSegments = 0;

    int i = begin;
    if (!(isRenditionOperator(transcriptionSegments.get(i))) &&
        !(isAlphaNumeric(transcriptionSegments.get(i).replace("*", ""))) &&
        !(transcriptionSegments.get(i).equals("{")) &&
        !(i - 2 > 0 && transcriptionSegments.get(i).equals("[")
            && transcriptionSegments.get(i - 2).equals("]"))) {
      do {
        if (transcriptionSegments.get(i).equals("[")) {
          amountOpenSegments++;

        }
        if (transcriptionSegments.get(i).equals("]")) {
          amountClosedSegments++;
        }
        if (amountOpenSegments == amountClosedSegments) {

          if (begin - 2 > 0 && isAlphaNumeric(transcriptionSegments.get(begin - 2).replace("*", ""))
              ||
              (amountOpenSegments == 1 && transcriptionSegments.get(i + 1).equals("]"))
                  && (!transcriptionSegments.get(i + 2).equals("["))) {

            return i;
          } else if (begin == 0 && isAlphaNumeric(transcriptionSegments.get(begin).replace("*", ""))
              ||
              (amountOpenSegments == 1 && transcriptionSegments.get(i).equals("]"))
                  && (i + 2 > transcriptionSegments.size()
                      && !transcriptionSegments.get(i + 2).equals("["))) {
            // System.out.println(transcriptionSegments.get(i));
            return i;
          } else {

            return i;
          }
        }
        i++;
      } while (i < transcriptionSegments.size());
    } else {
      int openSegments = 0;
      int closedSegments = 0;

      for (int j = i - 1; j >= 0; j--) {
        if (transcriptionSegments.get(j).equals("[")) {
          openSegments++;
        }
        if (transcriptionSegments.get(j).equals("]")) {
          closedSegments++;
        }

        if (j != i - 1 && closedSegments == openSegments) {

          // return "S" + Integer.toString(countSegmentsTillSpecificIndex(transcriptionSegments,
          // j)+1);
          return j;
        }
      }
    }

    return 666;
  }

  /**
   * @param transcriptionSegments
   * @param begin
   * @return
   */
  public static int getRenditionIndex(List<String> transcriptionSegments, int begin) {

    // System.out.println(begin);
    int amountClosedSegments = 0;
    int amountOpenSegments = 0;

    int i = begin;
    if (i - 2 >= 0) {
      if (ParserUtilities.isAlphaNumeric(transcriptionSegments.get(i - 2).replace("*", ""))
          || transcriptionSegments.get(i - 2).equals("??")) {

        return i;
      }
    }
    if (!(isRenditionOperator(transcriptionSegments.get(i))) &&
        !(isAlphaNumeric(transcriptionSegments.get(i))) &&
        !(transcriptionSegments.get(i).equals("{")) &&
        !(i - 2 > 0 && transcriptionSegments.get(i).equals("[")
            && transcriptionSegments.get(i - 2).equals("]"))) {
      do {

        if (transcriptionSegments.get(i).equals("[")) {
          amountOpenSegments = amountOpenSegments + 1;

        }
        if (transcriptionSegments.get(i).equals("]")) {
          amountClosedSegments = amountClosedSegments + 1;
        }
        // System.out.println("i: " + begin + " || " + amountOpenSegments + " || " +
        // amountClosedSegments);
        if (amountOpenSegments == amountClosedSegments) {

          if (begin - 2 > 0 && isAlphaNumeric(transcriptionSegments.get(begin - 2)) ||
              (amountOpenSegments == 1 && transcriptionSegments.get(i + 1).equals("]"))
                  && (!transcriptionSegments.get(i + 2).equals("["))) {
            if (transcriptionSegments.get(i + 1).equals("]")) {
              for (int j = i;; j--) {
                if (transcriptionSegments.get(j).equals("[")) {
                  // System.out.println("from rendition check j: " + transcriptionSegments.get(i));
                  return j;
                }
              }
            }
            // System.out.println("from rendition check i: " + transcriptionSegments.get(i));
            return i;
          } else {
            // System.out.println("from rendition check i: " + transcriptionSegments.get(i));
            return i;
          }
        }
        i++;
      } while (i < transcriptionSegments.size());

    } else {
      int openSegments = 0;
      int closedSegments = 0;
      // System.out.println("uncheck: " + transcriptionSegments.get(i+1));
      for (int j = i - 1; j >= 0; j--) {
        if (transcriptionSegments.get(j).equals("[")) {
          openSegments++;
        }
        if (transcriptionSegments.get(j).equals("]")) {
          closedSegments++;
        }

        if (j != i - 1 && closedSegments == openSegments) {
          // System.out.println("from rendition check i: " + transcriptionSegments.get(i));
          return i;
        }
      }
    }

    return 0;
  }

  /**
   * @param xmlData
   * @param indent
   * @return
   * @throws Exception
   */
  public static String getPrettyString(String xmlData, int indent) throws Exception {

    TransformerFactory transformerFactory = TransformerFactory.newInstance();
    transformerFactory.setAttribute("indent-number", indent);

    Transformer transformer = transformerFactory.newTransformer();
    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
    transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
    StringWriter stringWriter = new StringWriter();
    StreamResult xmlOutput = new StreamResult(stringWriter);

    Source xmlInput = new StreamSource(new StringReader(xmlData));
    transformer.transform(xmlInput, xmlOutput);

    return xmlOutput.getWriter().toString();
  }

  /**
   * @param tgUri
   * @param sid
   * @return
   * @throws TransformerFactoryConfigurationError
   * @throws TransformerException
   * @throws IOException
   * @throws CrudClientException
   * @throws SAXException
   */
  public static Document tgObjectToDomDoc(String tgUri, String sid)
      throws TransformerFactoryConfigurationError, TransformerException, IOException,
      CrudClientException, SAXException {

    final String XML_SCHEMA = "http://www.w3.org/2001/XMLSchema";
    final String SCHEMA_LANG = "http://java.sun.com/xml/jaxp/properties/schemaLanguage";
    final String SCHEMA_SOURCE = "http://java.sun.com/xml/jaxp/properties/schemaSource";
    TextGridObject tgObject = getTextGridObject(tgUri, sid);

    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder;
    Document doc = null;

    factory.setValidating(true);
    factory.setNamespaceAware(true);
    factory.setAttribute(SCHEMA_LANG, XML_SCHEMA);
    factory.setAttribute(SCHEMA_SOURCE, "");

    Transformer transformer = TransformerFactory.newInstance().newTransformer();
    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
    transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

    StreamResult result = new StreamResult(new StringWriter());

    DOMSource source = new DOMSource(doc);
    transformer.transform(source, result);

    String tgObjectAsXML = IOUtils.readStringFromStream(tgObject.getData());

    try {
      builder = factory.newDocumentBuilder();
      doc = builder.parse(new InputSource(new StringReader(tgObjectAsXML)));
    } catch (ParserConfigurationException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    System.out.println("TADA: ");
    System.out.println(doc.getElementById("A3-B3"));

    return doc;
  }

  /**
   * @param doc
   * @param id
   * @return
   */
  public static Element getGlyphBlock(Document doc, String id) {

    System.out.println(id);
    // System.out.println(doc.getElementById("A3-B3").getAttribute("n"));
    NodeList nodeList = doc.getElementsByTagName("ab");

    /*
     * for(int i=0; i<nodeList.getLength();i++) {
     * System.out.println(nodeList.item(i).getAttributes().getNamedItem("xml:id") + " " +
     * nodeList.item(i).getAttributes().getNamedItem("n")); }
     */

    System.out.println("buhu: " + doc.getElementById("A3-B3"));

    Element element = doc.getElementById("A3-B3");

    return element;
  }

}
