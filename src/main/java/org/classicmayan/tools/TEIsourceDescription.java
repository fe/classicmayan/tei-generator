package org.classicmayan.tools;

/**
 *
 */
public class TEIsourceDescription {

  private final String sourceDescBegin = "<sourceDesc>";
  private final String sourceDescEnd = "</sourceDesc>";

  private Paragraph p = new Paragraph();

  /**
   * @return
   */
  public Paragraph addPragraph() {
    return this.p;
  }

  public String getSourceDescription() {
    return this.sourceDescBegin + this.p.getParagraph() + this.sourceDescEnd;
  }

}
