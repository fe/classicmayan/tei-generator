package org.classicmayan.tools;

public class AltIdentifier {

  private final String altIdentifierBegin = "<altIdentifier>";
  private final String altIdentifierEnd = "</altIdentifier>";
  private AuthorityFile authorityFile = new AuthorityFile();

  public AuthorityFile addAuthorityFile() {
    return this.authorityFile;
  }

  public String getAltIdentifier() {
    return this.altIdentifierBegin + this.authorityFile.getAuthorityFile() + this.altIdentifierEnd;
  }

}
