package org.classicmayan.tools;

/**
 * @author Maximilian Brodhun
 * @version 0.1
 * @since 10.12.2017
 */
public class PersonName {

  private String persName;
  private String affiliation;
  public AuthorityFile authorityFile = new AuthorityFile();

  /**
   * @param persName
   * @param affiliation
   */
  public PersonName(String persName, String affiliation) {
    this.persName = persName;
    this.affiliation = affiliation;
  }

  /**
   * 
   */
  public PersonName() {

  }

  /**
   * @return
   */
  public AuthorityFile addAuthorityFile() {
    return this.authorityFile;
  }

  /**
   * @return
   */
  public String getAuthorityFile() {
    return "<idno type=\"" + this.authorityFile.getAuthorityFileType() + "\""
        + " xml:base=\"" + this.authorityFile.getAuthorityFilePrefix() + "\">"
        + this.authorityFile.getAuthorityFileID() + "</idno>";
  }

  /**
   * @return
   */
  public String getAffiliation() {
    return "<affiliation>" + this.affiliation + "</affiliation>";
  }

  /**
   * @param affiliation
   */
  public void setAffiliation(String affiliation) {
    this.affiliation = affiliation;
  }

  /**
   * @return
   */
  public String getPersName() {
    return "<persName>" + this.persName + "</persName>";
  }

  /**
   * @param persName
   */
  public void setPersName(String persName) {
    this.persName = persName;
  }

  /**
   * @return
   */
  public String getNameInXML() {
    return "<name>" + getPersName() + getAuthorityFile() + getAffiliation() + "</name>";
  }

}
