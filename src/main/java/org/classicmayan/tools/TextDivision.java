package org.classicmayan.tools;

/**
 *
 */
public class TextDivision {

  private String divId;
  private String typeOfTextfield;
  private GlyphBlock ab = new GlyphBlock();

  /**
   * @param ab
   * @return
   */
  public GlyphBlock addGlyphBlock(GlyphBlock ab) {
    this.ab = ab;
    return ab;
  }

  /**
   * @return
   */
  public String getDivId() {
    return this.divId;
  }

  /**
   * @param divId
   */
  public void setDivId(String divId) {
    this.divId = divId;
  }

  /**
   * @return
   */
  public String getTypeOfTextfield() {
    return this.typeOfTextfield;
  }

  /**
   * @param typeOfTextfield
   */
  public void setTypeOfTextfield(String typeOfTextfield) {
    this.typeOfTextfield = typeOfTextfield;
  }

  /**
   * @return
   */
  public String getTextDivision() {
    return "<div xml:id=\"" + getDivId() + "\""
        + " type=\"" + getTypeOfTextfield() + "\">"
        + this.ab.getGlyphblock() + "</div>";
  }

}
