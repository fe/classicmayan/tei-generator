package org.classicmayan.tools;

public class Glyph {

  private String signNumber;
  private String signCatalogReference;
  private String glyphID;
  private String glyphRendition;
  private String glyphStyle;
  private String gylphCorresponding;
  private int glyphCounter;

  public Glyph() {
    setGlyphCounter(getGlyphCounter() + 1);
  }

  public Glyph(String glyphID, String signNumber, String signCatalogReference,
      String glyphRendition) {
    setGlyphCounter(getGlyphCounter() + 1);
    this.signNumber = signNumber;
    this.signCatalogReference = signCatalogReference;
    this.glyphID = glyphID;
    this.glyphRendition = glyphRendition;
  }

  public Glyph(String glyphID, String signNumber, String signCatalogReference,
      String glyphRendition, String gylphCorresponding) {
    setGlyphCounter(getGlyphCounter() + 1);
    this.signNumber = signNumber;
    this.signCatalogReference = signCatalogReference;
    this.glyphID = glyphID;
    this.glyphRendition = glyphRendition;
    this.gylphCorresponding = gylphCorresponding;
  }

  public Glyph(String glyphID, String glyphRendition, String glyphCorresponding) {
    this.glyphID = glyphID;
    this.glyphRendition = glyphRendition;
    this.gylphCorresponding = glyphCorresponding;
  }

  public String getGlyphNumber() {
    return this.signNumber;
  }

  public void setGlyphNumber(String signNumber) {
    this.signNumber = signNumber;
  }

  public String getSignCatalogReference() {
    return this.signCatalogReference;
  }

  public void setSignCatalogReference(String signCatalogReference) {
    this.signCatalogReference = signCatalogReference;
  }

  public String getGlyphID() {
    return this.glyphID;
  }

  public void setGlyphID(String glpyhID) {
    this.glyphID = glpyhID;
  }

  public String getGlyphRendition() {
    return this.glyphRendition;
  }

  public void setGlyphRendition(String glyphRendition) {
    this.glyphRendition = glyphRendition;
  }

  public String getGlyphStyle() {
    return this.glyphStyle;
  }

  public void setGlyphStyle(String glyphStyle) {
    this.glyphStyle = glyphStyle;
  }

  public String getGylphCorresponding() {
    return this.gylphCorresponding;
  }

  public void setGylphCorresponding(String gylphCorresponding) {
    this.gylphCorresponding = gylphCorresponding;
  }

  public int getGlyphCounter() {
    return this.glyphCounter;
  }

  public void setGlyphCounter(int glyphCounter) {
    this.glyphCounter = glyphCounter;
  }

  public String getTEICode() {
    if (getGlyphNumber() == null && getSignCatalogReference() == null
        && this.getGylphCorresponding() == null) {
      return "<g " + "xml:id=\"" + getGlyphID() + "\""
          + " rend=\"" + getGlyphRendition() + "\"/>";
    }
    if (getGlyphNumber() == null && getSignCatalogReference() == null
        && this.getGylphCorresponding() != null) {
      return "<g " + "xml:id=\"" + getGlyphID() + "\""
          + " rend=\"" + getGlyphRendition() + "\""
          + " corresp=\"" + getGylphCorresponding() + "\"/>";
    }
    if (getGylphCorresponding() == null) {
      return "<g " + "xml:id=\"" + getGlyphID() + "\""
          + " n=\"" + getGlyphNumber() + "\""
          + " ref=\"" + getSignCatalogReference() + "\""
          + " rend=\"" + getGlyphRendition() + "\"/>";
    } else {
      return "<g " + "xml:id=\"" + getGlyphID() + "\""
          + " n=\"" + getGlyphNumber() + "\""
          + " ref=\"" + getSignCatalogReference() + "\""
          + " rend=\"" + getGlyphRendition() + "\""
          + " corresp=\"" + getGylphCorresponding() + "\"/>";
    }
  }

}

