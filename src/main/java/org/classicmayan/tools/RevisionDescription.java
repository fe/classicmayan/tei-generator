package org.classicmayan.tools;

public class RevisionDescription {

  private final String revsisionDesc = "<revisionDesc>\n" +
      "         <change when-iso=\"[yyyy-mm-dd]\" who=\"#[initials of editior]\">[description of the changes,\n"
      +
      "            e.g. correction of epigraphic analysis for texfield 2; supplied additional sign for ab\n"
      +
      "            xml:id=\"A1\"</change>\n" +
      "      </revisionDesc>";

  /**
   * @return
   */
  public String getRevsisionDesc() {
    return this.revsisionDesc;
  }

}
