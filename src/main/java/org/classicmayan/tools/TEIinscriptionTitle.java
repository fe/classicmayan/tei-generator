package org.classicmayan.tools;

/**
 *
 */
public class TEIinscriptionTitle {

  private String title;
  private String transcriberName;
  private final String titleStmtBegin = "<titleStmt>";
  private final String titleStmtEnd = "</titleStmt>";
  private RespStatement respStmt;

  /**
   * 
   */
  public TEIinscriptionTitle() {

  }

  /**
   * @param title
   * @param transcriberName
   */
  public TEIinscriptionTitle(String title, String transcriberName) {
    this.setInscriptionTitle(title);
    this.setTranscriberName(transcriberName);
    // TODO: Role for respStatement from GUI later
    this.addRespStatement(new RespStatement(this.getTranscriberName(), "Transcriber"));
  }

  /**
   * @return
   */
  public String getInscriptionTitle() {
    return "<title>" + this.title + "</title>";
  }

  /**
   * @param title
   */
  public void setInscriptionTitle(String title) {
    this.title = title;
  }

  /**
   * @param respStmt
   * @return
   */
  public RespStatement addRespStatement(RespStatement respStmt) {
    this.respStmt = respStmt;
    return respStmt;
  }

  /**
   * @return
   */
  public String getCompleteInscriptionTitleinXML() {
    return this.titleStmtBegin + getInscriptionTitle() + this.respStmt.getRespStatementInXML()
        + this.titleStmtEnd;
  }

  /**
   * @return
   */
  public String getRespStmtInXML() {
    return this.respStmt.getRespStatementInXML();
  }

  /**
   * @return
   */
  public String getTranscriberName() {
    return this.transcriberName;
  }

  /**
   * @param transcriberName
   */
  public void setTranscriberName(String transcriberName) {
    this.transcriberName = transcriberName;
  }

}
