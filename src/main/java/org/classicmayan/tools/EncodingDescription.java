package org.classicmayan.tools;

public class EncodingDescription {

  private final String encdodingDescription = "<encodingDesc>\n" +
      "         <projectDesc xml:lang=\"en\">\n" +
      "            <p> In 2014, the North Rhine-Westphalian Academy of Sciences, Humanities and the Arts\n"
      +
      "               established the \"Text Database Dictionary of Classic Mayan\" research center for the\n"
      +
      "               study of hieroglyphic writing and language of the ancient Maya at the University of\n"
      +
      "               Bonn’s Philosophy Faculty. The goal of this long term project is the analysis of all\n"
      +
      "               known hieroglyphic Mayan texts which will serve as the basis for the compilation and\n"
      +
      "               editing of a Classic Mayan language dictionary.</p>\n" +
      "         </projectDesc>\n" +
      "         <projectDesc xml:lang=\"de\">\n" +
      "            <p> Im Jahr 2014 nahm die Arbeitsstelle \"Textdatenbank und Wörterbuch des Klassischen\n"
      +
      "               Maya\" der Nordrhein-Westfälischen Akademie der Wissenschaften und der Künste an der\n"
      +
      "               Rheinischen Friedrich- Wilhelms-Universität Bonn ihre Arbeit auf. Ziel des\n"
      +
      "               Langzeitprojekts ist zum einen die Erforschung der Hieroglyphenschrift der\n"
      +
      "               vorspanischen Mayakultur (500 v. Chr. bis 1500 n. Chr.), die sich auf dem Gebiet der\n"
      +
      "               heutigen Staaten Mexiko, Guatemala, Belize und Honduras entwickelte. Zum anderen\n"
      +
      "               steht der Aufbau eines Wörterbuches sowie einer korpusbasierten Datenbank des\n"
      +
      "               Klassischen Maya im Mittelpunkt des Forschungsvorhabens. </p>\n" +
      "         </projectDesc>\n" +
      "         <projectDesc xml:lang=\"es\">\n" +
      "            <p> En 2014 fue fundado el centro de investigación \"Base de Datos de Texto y Diccionario\n"
      +
      "               del Maya Clásico\" (Textdatenbank und Wörterbuch des Klassischen Maya) de la Academia\n"
      +
      "               de Renania del Norte-Westfalia de las Ciencias y de las Artes (Nordrhein-Westfälische\n"
      +
      "               Akademie der Wissenschaften und der Künste) en la Universidad de Bonn. El primer\n"
      +
      "               objetivo del proyecto a largo plazo es el estudio de la escritura jeroglífica de la\n"
      +
      "               cultura maya prehispánica (500 a. C. hasta 1500 d. C.) que abarcó parte de los\n"
      +
      "               actuales territorios de México, Guatemala, Belice y Honduras. El segundo objetivo es\n"
      +
      "               compilar un diccionario y crear una base de datos del maya clásico.</p>\n" +
      "         </projectDesc>\n" +
      "         <editorialDecl xml:lang=\"en\">\n" +
      "            <p> Due to the circumstance that there are no Unicode characters for Classic Mayan and\n"
      +
      "               furthermore that the reading of some Mayan glyphs is still not known and/or is the\n"
      +
      "               subject of research discussions, the digital corpus coded in TEI/XML cannot be given\n"
      +
      "               in either original script nor in phonemic-transcribed values. To enable the creation\n"
      +
      "               of a flexible corpus, which is able to react to different possible readings, we use a\n"
      +
      "               sign catalogue as basis for generating the textcorpus. This sign catalog was\n"
      +
      "               developed in the scope of this research project and is realised in RDF. The coded\n"
      +
      "               text basically consists of glyphs coded with the element \"g\". By using the attributes\n"
      +
      "               @n (for the number of the glyph in the sign catalogue) and @ref (for URI reference),\n"
      +
      "               a reference to the sign catalogue is given. In a further step of processing the\n"
      +
      "               TEI-coded text the readings of signs given from the sign catalogue can be extracted\n"
      +
      "               and the text becomes \"readable\". </p>\n" +
      "         </editorialDecl>\n" +
      "         <editorialDecl xml:lang=\"de\">\n" +
      "            <p> Da es für die Hieroglyphenschrift des Klassischen Maya kein Unicode-Schriftsatz gibt\n"
      +
      "               und die Lesung vieler Zeichen noch nicht bekannt oder umstritten ist, kann das in\n"
      +
      "               TEI/XML kodierte Textkorpus weder aus Originalschreibung noch aus\n" +
      "               phonemisch-transkribierten Werten bestehen. Um ein flexibles Korpus zu erhalten, das\n"
      +
      "               auf neue Entzifferungen und auf verschiedenen Lesungshypothesen reagieren kann,\n"
      +
      "               benutzen wir den in RDF-vorliegenden Zeichenkatalog, der im Rahmen dieses\n" +
      "               Forschungsprojekts entstanden ist, als Grundlage und verknüpfen diesen mit dem\n"
      +
      "               Textkorpus. Innerhalb des kodierten Texts wird jede Glyphe mit dem Element \"g\"\n"
      +
      "               bezeichnet und mittels der Katalognummer (Attribut @n) und einer Referenz zu der\n"
      +
      "               entsprechenden URI im Zeichenkatalog (Attribut @ref) erfasst. Der Text wird erst\n"
      +
      "               durch die Abfrage der Daten aus dem Zeichenkatalog \"lesbar\". Dies geschieht in einem\n"
      +
      "               weiteren Prozessierungsschritt. </p>\n" +
      "         </editorialDecl>\n" +
      "         <editorialDecl xml:lang=\"es\">\n" +
      "            <p> El corpus de texto anotado en TEI/XML no puede consistir ni en la grafía original ni\n"
      +
      "               en valores transcritos fonémicamente, puesto que no existe un conjunto de caracteres\n"
      +
      "               Unicode para la escritura maya, y por el hecho que la lectura de muchos glifos\n"
      +
      "               todavía es desconocida o discutida. Para desarrollar un corpus flexible que se\n"
      +
      "               adapte a nuevos desciframientos y diferentes hipótesis de lectura utilisamos el\n"
      +
      "               catálogo de signos mayas escrito en RDF como base. Fue formado en el margen de este\n"
      +
      "               proyecto y será vinculado con el corpus de texto. Dentro del texto codificado cada\n"
      +
      "               glífo será designado con el elemento \"g\" y registrado a través de un número de\n"
      +
      "               catálogo (atributo @n) y una referencia a la URI en el catálogo de signos (atributo\n"
      +
      "               @ref). Sólo a través de la consulta de datos el catálogo será \"legible\". Esto succede\n"
      +
      "               en un siguiente paso de procesamiento.</p>\n" +
      "         </editorialDecl>\n" +
      "      </encodingDesc>";

  public String getEncdodingDescription() {
    return this.encdodingDescription;
  }

}
