package org.classicmayan.tools;

public class AuthorityFile {

  private String authorityFileID;
  private String authorityFileType;
  private String authorityFilePrefix;

  public AuthorityFile(String authorityFileID, String authorityFileType,
      String authorityFilePrefix) {
    this.authorityFileID = authorityFileID;
    this.authorityFileType = authorityFileType;
    this.authorityFilePrefix = authorityFilePrefix;
  }

  public AuthorityFile() {}

  public String getAuthorityFileID() {
    return this.authorityFileID;
  }

  public void setAuthorityFileID(String authorityFileID) {
    this.authorityFileID = authorityFileID;
  }

  public String getAuthorityFileType() {
    return this.authorityFileType;
  }

  public void setAuthorityFileType(String authorityFileType) {
    this.authorityFileType = authorityFileType;
  }

  public String getAuthorityFilePrefix() {
    return this.authorityFilePrefix;
  }

  public void setAuthorityFilePrefix(String authorityFilePrefix) {
    this.authorityFilePrefix = authorityFilePrefix;
  }

  public String getAuthorityFile() {
    return "<idno type=\"" + getAuthorityFileType() + "\""
        + " xml:base=\"" + getAuthorityFilePrefix() + "\">"
        + getAuthorityFileID() + "</idno>";
  }

}
