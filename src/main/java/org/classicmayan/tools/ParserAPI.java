package org.classicmayan.tools;

import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import info.textgrid.clients.tgcrud.CrudClientException;
import javax.ws.rs.core.Response;

/**
 *
 */
public interface ParserAPI {

  /**
   * @param textgridUri: identifier of the object containing the the to be parsed transcription code
   * @param sid: parameter to identify the access rights to the file
   * @return
   */
  @GET
  @Path("/")
  @Produces({"application/xml", "text/xml"})
  Response getTeiFile(@QueryParam("tgID") String textgridUri,
      @QueryParam("sid") String sessionid);

  /**
   * @param tei: identifier of the object containing the the to be parsed transcription code
   * @param artefact: identifier of the artifact. Necessary to get the link to the text carrier
   * @param sid: parameter to identify the access rights to the file
   * @return
   * @throws SAXException
   * @throws TransformerException
   * @throws TransformerFactoryConfigurationError
   * @throws IOException
   * @throws CrudClientException
   * @throws XPathExpressionException
   * @throws Exception
   */
  @GET
  @Path("/parse")
  @Produces(MediaType.TEXT_XML)
  Document parseToTEI(@QueryParam("tei") String uriToTEIFile,
      @QueryParam("artefact") String uriToArtefact,
      @QueryParam("title") String title,
      @QueryParam("sid") String sessionid)
      throws XPathExpressionException, CrudClientException, IOException,
      TransformerFactoryConfigurationError, TransformerException, SAXException, Exception;

  /**
   * @param textgridUri: identifier of the object containing the the to be parsed transcription code
   * @param transcriptionCode: new transcription code to be parsed
   * @param glyphBlockID: identify where the new parsed tei code will be exchanged
   * @param sid: parameter to identify the access rights to the file
   * @return
   * @throws IOException
   * @throws SAXException
   */
  @GET
  @Path("/correctGlyphBlock")
  @Produces(MediaType.TEXT_XML)
  Document correctGlyphBlock(@QueryParam("tgID") String textgridUri,
      @QueryParam("transcriptionCode") String transcriptionCode,
      @QueryParam("glyphBlock") String glyphBlockID,
      @QueryParam("sessionid") String sessionid) throws SAXException, IOException;

  /**
   * @return
   */
  @GET
  @Path("/version")
  @Produces(MediaType.TEXT_PLAIN)
  String getVersion();

}
