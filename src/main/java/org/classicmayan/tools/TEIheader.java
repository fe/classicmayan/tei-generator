package org.classicmayan.tools;

import java.io.IOException;

import info.textgrid.clients.tgcrud.CrudClientException;

/**
 *
 */
public class TEIheader {

  private TEIFileDescription fileDesc;
  private EncodingDescription encodeDesc = new EncodingDescription();
  private String textGridURIOfTranscriptionFile;
  private final String teiHeaderBegin = "<teiHeader>";
  private final String teiHeaderEnd = "</teiHeader>";
  private String nameOfTranscriptor;

  /**
   * 
   */
  public TEIheader() {

  }

  /**
   * @param textGridURIOfTranscriptionFile
   * @param textGridURIOfArtefact
   * @param sid
   * @throws CrudClientException
   * @throws IOException
   */
  public TEIheader(String textGridURIOfTranscriptionFile, String textGridURIOfArtefact, String sid)
      throws CrudClientException, IOException {
    this.setTextGridURIOfTranscriptionFile(textGridURIOfTranscriptionFile);
    this.nameOfTranscriptor = ParserUtilities.getTextGridObject(textGridURIOfTranscriptionFile, sid)
        .getMetadatada().getObject().getGeneric().getGenerated().getDataContributor()
        .replace(".", " ").replace("@textgrid de", "");
    this.addFileDescription(new TEIFileDescription(
        TripleStoreQuery.getArtefactNameByTextGridUri(textGridURIOfArtefact),
        this.nameOfTranscriptor));
  }

  /**
   * @param fileDesc
   * @return
   */
  public TEIFileDescription addFileDescription(TEIFileDescription fileDesc) {
    this.fileDesc = fileDesc;
    return fileDesc;
  }

  /**
   * @return
   */
  public String getTeiHeader() {
    return this.teiHeaderBegin + this.fileDesc.getTEIinscriptionsTitle()
        + this.encodeDesc.getEncdodingDescription() + this.teiHeaderEnd;
  }

  /**
   * @return
   */
  public String getTextGridURIOfTranscriptionFile() {
    return this.textGridURIOfTranscriptionFile;
  }

  /**
   * @param textGridURIOfTranscriptionFile
   */
  public void setTextGridURIOfTranscriptionFile(String textGridURIOfTranscriptionFile) {
    this.textGridURIOfTranscriptionFile = textGridURIOfTranscriptionFile;
  }

  /**
   * @return
   */
  public String getNameOfTranscriptor() {
    return this.nameOfTranscriptor;
  }

  /**
   * @param nameOfTranscriptor
   */
  public void setNameOfTranscriptor(String nameOfTranscriptor) {
    this.nameOfTranscriptor = nameOfTranscriptor;
  }

}
