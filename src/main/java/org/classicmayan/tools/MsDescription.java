package org.classicmayan.tools;

public class MsDescription {

  private final String msDescriptionBegin = "<msDesc>";
  private final String msDescriptionEnd = "</msDesc>";
  MsIdentifier msIdentifier = new MsIdentifier();

  public MsIdentifier addMsIdentifier() {
    return this.msIdentifier;
  }

  public String getMsDescription() {
    return this.msDescriptionBegin + this.msIdentifier.getMsIdentifier() + this.msDescriptionEnd;
  }

}
