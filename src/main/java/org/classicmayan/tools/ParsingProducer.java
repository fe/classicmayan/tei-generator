package org.classicmayan.tools;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import javax.activation.DataHandler;
import javax.mail.util.ByteArrayDataSource;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXB;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.ws.Holder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import info.textgrid.clients.CrudClient;
import info.textgrid.clients.tgcrud.CrudClientException;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ProtocolNotImplementedFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudService;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.tgcrudclient.TGCrudClientUtilities;

/**
 *
 */
@Path("/")
public class ParsingProducer implements ParserAPI {

  private Log log = LogFactory.getLog(ParsingProducer.class);

  /**
   *
   */
  @Override
  public Response getTeiFile(String textgridUri, String sessionid) {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   *
   */
  @Override
  public Document parseToTEI(String uriToTEIFile, String uriToArtefact, String title,
      String sessionid) throws TransformerFactoryConfigurationError, Exception {

    this.log.info("uriToTeiFile: " + uriToTEIFile);
    this.log.info("uriToArtefact: " + uriToArtefact);
    this.log.info("sessionid: " + sessionid);
    this.log.info("title: " + title);

    String tei = new TEIFile(uriToTEIFile, uriToArtefact, sessionid).getTeiFile();

    this.log.info("Result of the parsing process is: " + tei);

    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder;
    Document doc = null;

    IdiomTextGridObject idiomTGObject = new IdiomTextGridObject();
    idiomTGObject.setSessionID(sessionid);
    // FIXME Set classicmayan project ID via configuration file here! (see Queries Collection
    // configuration!)
    idiomTGObject.setProjectID("TGPR-0e926f53-1aba-d415-ecf6-539edcd8a318");

    try {
      builder = factory.newDocumentBuilder();
      doc = builder.parse(new InputSource(new StringReader(tei)));
      try {
        saveToTextGrid(idiomTGObject, tei, title, "textgrid:3rq31");
      } catch (ObjectNotFoundFault e) {
        this.log.error("Object not found");
        e.printStackTrace();
      } catch (AuthFault e) {
        this.log.error("Object is not accessible");
        e.printStackTrace();
      } catch (IoFault e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (MetadataParseFault e) {
        this.log.error("Could not parse Metadata");
        e.printStackTrace();
      } catch (ProtocolNotImplementedFault e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    } catch (ParserConfigurationException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    // return Response.ok().build();
    return doc;
  }

  /**
   *
   */
  @Override
  public Document correctGlyphBlock(String textgridUri, String transcriptionCode,
      String glyphBlockID, String sessionid) throws SAXException, IOException {

    Transcription transcription = new Transcription(transcriptionCode);
    transcription.setGlyphBlockID(glyphBlockID);
    transcription.parseTranscriptionCode();

    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder;
    Document doc = null;

    try {
      builder = factory.newDocumentBuilder();
      doc = builder.parse(new InputSource(new StringReader(transcription.getTeiCode())));
    } catch (ParserConfigurationException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    return doc;
  }

  /**
   *
   */
  @Override
  public String getVersion() {
    // TODO Return real version here!
    return "Wuhu";
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * @param tgObject
   * @param jsonFile
   * @param title
   * @param aggregationURI
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws ProtocolNotImplementedFault
   * @throws IOException
   * @throws CrudClientException
   */
  public void saveToTextGrid(IdiomTextGridObject tgObject, String jsonFile, String title,
      String aggregationURI) throws ObjectNotFoundFault, AuthFault, IoFault, MetadataParseFault,
      ProtocolNotImplementedFault, IOException, CrudClientException {

    // TODO Use textgrid-java-clients here?!
    TGCrudService crudClient = TGCrudClientUtilities.getTgcrud(CrudClient.DEFAULT_ENDPOINT);

    // Create metadata from template string, add title.
    String template = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
        "<ns3:tgObjectMetadata\n" +
        "xmlns:ns3=\"http://textgrid.info/namespaces/metadata/core/2010\"\n" +
        "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
        "xsi:schemaLocation=\"http://textgrid.info/namespaces/metadata/core/2010 http://textgridlab.org/schema/textgrid-metadata_2010.xsd\">\n"
        +
        "<ns3:object>\n" +
        "<ns3:generic>\n" +
        "<ns3:provided>\n" +
        "<ns3:title>" + title + "</ns3:title>\n" +
        "<ns3:format>text/xml</ns3:format>\n" +
        "</ns3:provided>\n" +
        "</ns3:generic>\n" +
        "<ns3:item />\n" +
        "</ns3:object>\n" +
        "</ns3:tgObjectMetadata>";
    ByteArrayInputStream theMetadataStream = new ByteArrayInputStream(template.getBytes());
    MetadataContainerType metadata = JAXB.unmarshal(theMetadataStream, MetadataContainerType.class);

    // JAXB.marshal(metadata, System.out);

    Holder<MetadataContainerType> metadataHolder = new Holder<MetadataContainerType>(metadata);
    DataHandler objectData =
        new DataHandler(new ByteArrayDataSource(jsonFile.getBytes(), "application/octet-stream"));

    // Set PARSER FILE type here in notes of metadata.
    metadataHolder.value.getObject().getGeneric().getProvided().setNotes("PARSER_FILE");

    // Create new TEI file.
    crudClient.create(tgObject.getSessionID(), "", "", false, tgObject.getProjectID(),
        metadataHolder, objectData);

    // Get TextGrid URI from result metadata.
    MetadataContainerType resultMetadata = metadataHolder.value;
    List<String> textgridURI = new ArrayList<String>();
    textgridURI
        .add(resultMetadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue());

    // Add new object to existing aggregation.
    tgObject.addUriListToAggregation(textgridURI, aggregationURI);
  }

}
