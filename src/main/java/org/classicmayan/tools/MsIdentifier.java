package org.classicmayan.tools;

public class MsIdentifier {

  private String msIdentifierBegin = "<msIdentifier>";
  private String msIdentifierEnd = "</msIdentifier>";

  AltIdentifier altIdentifier = new AltIdentifier();
  MsName msName = new MsName();

  public AltIdentifier addAltIdentifier() {
    return this.altIdentifier;
  }

  public MsName addMasName() {
    return this.msName;
  }

  public String getMsIdentifier() {
    return this.msIdentifierBegin + this.msName.getMsName() + this.altIdentifier.getAltIdentifier()
        + this.msIdentifierEnd;
  }

}
