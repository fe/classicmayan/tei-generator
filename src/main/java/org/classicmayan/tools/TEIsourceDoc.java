package org.classicmayan.tools;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 */
public class TEIsourceDoc {

  private String tgObjectAsXML;

  /**
   * @param tgObjectAsXML
   */
  public TEIsourceDoc(String tgObjectAsXML) {
    this.tgObjectAsXML = tgObjectAsXML;
  }

  /**
   * @return
   */
  public String getTEISourceDoc() {

    Pattern pattern = Pattern.compile("<sourceDoc>.*</sourceDoc>", Pattern.DOTALL);
    Matcher matcher = pattern.matcher(this.tgObjectAsXML);
    String result = matcher.replaceAll(this.tgObjectAsXML);

    return result;
  }

}
