package org.classicmayan.tools;

public class Affiliation {

  public String affiliationName;
  public String tada;

  public String getAffiliationName() {
    return this.affiliationName;
  }

  public void setAffiliationName(String affiliationName) {
    this.affiliationName = affiliationName;
  }

}
