package org.classicmayan.tools;

/**
 *
 */
public class Unclear {

  private String unclear;

  /**
   * @return
   */
  public String getUnclear() {
    return this.unclear;
  }

  /**
   * @param unclear
   */
  public void setUnclear(String unclear) {
    this.unclear = unclear;
  }

}
