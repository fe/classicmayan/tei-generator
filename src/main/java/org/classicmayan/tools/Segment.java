package org.classicmayan.tools;

/**
 *
 */
public class Segment {

  private String segmentID;
  private String segmentType;
  private String segmentRendition;
  private String segmentCorresp;
  private int segmentCount;

  /**
   * 
   */
  public Segment() {
    setSegmentCount(getSegmentCount() + 1);
  }

  /**
   * @param segmentID
   * @param segmentType
   * @param segmentRendition
   * @param segmentCorresp
   */
  public Segment(String segmentID, String segmentType, String segmentRendition,
      String segmentCorresp) {

    setSegmentCount(getSegmentCount() + 1);
    this.segmentID = segmentID;
    this.segmentType = segmentType;
    this.segmentRendition = segmentRendition;
    this.segmentCorresp = segmentCorresp;
  }

  /**
   * @return
   */
  public String getSegmentID() {
    return this.segmentID;
  }

  /**
   * @param segmentID
   */
  public void setSegmentID(String segmentID) {
    this.segmentID = segmentID;
  }

  /**
   * @return
   */
  public String getSegmentType() {
    return this.segmentType;
  }

  /**
   * @param segmentType
   */
  public void setSegmentType(String segmentType) {
    this.segmentType = segmentType;
  }

  /**
   * @return
   */
  public String getSegmentRendition() {
    return this.segmentRendition;
  }

  /**
   * @param segmentRendition
   */
  public void setSegmentRendition(String segmentRendition) {
    this.segmentRendition = segmentRendition;
  }

  /**
   * @return
   */
  public String getSegmentCorresp() {
    return this.segmentCorresp;
  }

  /**
   * @param segmentCorresp
   */
  public void setSegmentCorresp(String segmentCorresp) {
    this.segmentCorresp = segmentCorresp;
  }

  /**
   * @return
   */
  public int getSegmentCount() {
    return this.segmentCount;
  }

  /**
   * @param segmentCount
   */
  public void setSegmentCount(int segmentCount) {
    this.segmentCount = segmentCount;
  }

  /**
   * @return
   */
  public String getTeiCode() {
    return "<seg " + "xml:id=\"" + getSegmentID() + "\""
        + " type=\"" + getSegmentType() + "\""
        + " rend=\"" + getSegmentRendition() + "\""
        + " corresp=\"" + getSegmentCorresp() + "\"" + ">";
  }

}
