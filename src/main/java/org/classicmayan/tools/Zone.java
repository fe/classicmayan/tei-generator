package org.classicmayan.tools;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class Zone {

  private String corresp;
  private List<Zone> zonesForSigns = new ArrayList<Zone>();

  /**
   * 
   */
  public Zone() {

  }

  /**
   * @param corresp
   */
  public Zone(String corresp) {
    this.setCorresp(corresp);
  }

  /**
   * @param zone
   * @return
   */
  public List<Zone> addZone(Zone zone) {
    this.zonesForSigns.add(zone);
    return this.zonesForSigns;
  }

  /**
   * @return
   */
  public String getCorresp() {
    return this.corresp;
  }

  /**
   * @param corresp
   */
  public void setCorresp(String corresp) {
    this.corresp = corresp;
  }

  /**
   * @return
   */
  public String getZoneXML() {

    String zonesForSignsString = "";
    if (this.zonesForSigns.size() > 0) {
      for (Zone zone : this.zonesForSigns) {
        zonesForSignsString =
            zonesForSignsString.concat("<zone corresp=\"" + zone.getCorresp() + "\"/>");
      }
      return "<zone corresp=\"" + getCorresp() + "\">" + zonesForSignsString + "</zone>";
    } else {
      return "<zone corresp=\"" + getCorresp() + "\">";
    }
  }

}
