package org.classicmayan.tools;

/**
 *
 */
public class Supplied {

  private String reason;
  private String evidence;
  private String precision;
  private String ana;
  private Note note;
  private Glyph glyph;
  private Choice choice;

  /**
   * @param precision
   */
  public Supplied(String precision) {
    this.precision = precision;
  }

  /**
   * @param reason
   * @param evidence
   * @param precision
   * @param ana
   */
  public Supplied(String reason, String evidence, String precision, String ana) {
    this.reason = reason;
    this.evidence = evidence;
    this.precision = precision;
    this.ana = ana;
  }

  /**
   * @param reason
   * @param evidence
   * @param ana
   */
  public Supplied(String reason, String evidence, String ana) {
    this.reason = reason;
    this.evidence = evidence;
    this.ana = ana;
  }

  /**
   * @param choice
   * @return
   */
  public Choice addChoice(Choice choice) {
    this.choice = choice;
    return choice;
  }

  /**
   * @param glyph
   * @return
   */
  public Glyph addGlyph(Glyph glyph) {
    this.glyph = glyph;
    return glyph;
  }

  /**
   * @return
   */
  public String getReason() {
    return this.reason;
  }

  /**
   * @param reson
   */
  public void setReason(String reson) {
    this.reason = reson;
  }

  /**
   * @return
   */
  public String getEvidence() {
    return this.evidence;
  }

  /**
   * @param evidence
   */
  public void setEvidence(String evidence) {
    this.evidence = evidence;
  }

  /**
   * @return
   */
  public String getAna() {
    return this.ana;
  }

  /**
   * @param ana
   */
  public void setAna(String ana) {
    this.ana = ana;
  }

  /**
   * @return
   */
  public String getPrecision() {
    return this.precision;
  }

  /**
   * @param precision
   */
  public void setPrecision(String precision) {
    this.precision = precision;
  }

  /**
   * @return
   */
  public String getSupplied() {

    if ((getEvidence() != null && (getEvidence().equals("conjecture")
        || (getEvidence().equals("low") && getPrecision() != null)))) {
      return "<supplied" +
          " reason=\"" + this.reason + "\"" +
          " evidence=\"" + this.evidence + "\"" +
          " precision=\"" + this.precision + "\"" +
          " ana=\"" + this.ana + "\">" + this.note.getNote() +
          "</supplied>";
    }

    if (getReason() == null && getEvidence() == null && getAna() == null) {
      if (!this.glyph.getGlyphID().endsWith("a") && !this.glyph.getGlyphID().endsWith("b")) {
        this.glyph.setGlyphID(this.glyph.getGlyphID() + "a");
      } else {
        this.glyph.setGlyphID(this.glyph.getGlyphID().replaceAll("a", "b"));
      }
      return "<supplied precision=\"" + this.precision + "\">" + this.glyph.getTEICode()
          + "</supplied>";
    }

    if (getPrecision() == null) {
      return "<supplied" +
          " reason=\"" + this.reason + "\"" +
          " evidence=\"" + this.evidence + "\"" +
          " ana=\"" + this.ana + "\">" + this.choice.getChoice() +
          "</supplied>";
    }

    if (this.choice != null) {
      return "<supplied" +
          " reason=\"" + this.reason + "\"" +
          " evidence=\"" + this.evidence + "\"" +
          " precision=\"" + this.precision + "\"" +
          " ana=\"" + this.ana + "\">" + this.choice.getChoice() +
          "</supplied>";
    } else {
      return "<supplied" +
          " reason=\"" + this.reason + "\"" +
          " evidence=\"" + this.evidence + "\"" +
          " precision=\"" + this.precision + "\"" +
          " ana=\"" + this.ana + "\">" + this.glyph.getTEICode() +
          "</supplied>";
    }
  }

}
