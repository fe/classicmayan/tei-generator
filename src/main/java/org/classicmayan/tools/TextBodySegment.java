package org.classicmayan.tools;

/**
 *
 */
public class TextBodySegment {

  private final String textBodyBegin = "<text><body>";
  private final String textBodyEnd = "</body></text>";
  private TextDivision div = new TextDivision();;

  /**
   * @return
   */
  public TextDivision addTextDivision() {
    return this.div;
  }

  /**
   * @return
   */
  public String getTextBody() {
    return this.textBodyBegin + this.div.getTextDivision() + this.textBodyEnd;
  }

}
