package org.classicmayan.tools;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPathExpressionException;

import org.apache.cxf.helpers.IOUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import info.textgrid.clients.tgcrud.CrudClientException;
import info.textgrid.clients.tgcrud.TextGridObject;

/**
 *
 */
public class TEIFile {

  private TEIheader teiheader;
  private TextBodySegment textbody = new TextBodySegment();
  private String textGridUri;
  private SourceDoc sourceDoc = new SourceDoc();

  private String teiFile;

  /**
   * 
   */
  public TEIFile() {

  }

  /**
   * @param textGridUri
   * @param textGridURIOfArtefact
   * @param sid
   * @throws CrudClientException
   * @throws IOException
   * @throws TransformerFactoryConfigurationError
   * @throws TransformerException
   * @throws SAXException
   * @throws XPathExpressionException
   */
  public TEIFile(String textGridUri, String textGridURIOfArtefact, String sid)
      throws CrudClientException, IOException, TransformerFactoryConfigurationError,
      TransformerException, SAXException, XPathExpressionException {

    this.setTextGridUri(textGridUri);
    this.addTEIheader(new TEIheader(this.getTextGridUri(), textGridURIOfArtefact, sid));

    TextGridObject tgObject = ParserUtilities.getTextGridObject(this.getTextGridUri(), sid);

    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder;
    Document doc = null;

    Transformer transformer = TransformerFactory.newInstance().newTransformer();
    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
    transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

    StreamResult result = new StreamResult(new StringWriter());

    DOMSource source = new DOMSource(doc);
    transformer.transform(source, result);

    String tgObjectAsXML = IOUtils.readStringFromStream(tgObject.getData());

    try {
      builder = factory.newDocumentBuilder();
      doc = builder.parse(new InputSource(new StringReader(tgObjectAsXML)));
    } catch (ParserConfigurationException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    NodeList nodeList = doc.getElementsByTagName("ab");

    /*
     * Contaiing all divs with the attribute data-tag=surface. For each surface a group of text
     * fields is present in the xml file
     */
    List<Node> surfaceDivs = new ArrayList<Node>();

    for (int i = 0; i < doc.getElementsByTagName("div").getLength(); i++) {
      if (doc.getElementsByTagName("div").item(i).getAttributes().getNamedItem("xml:id") != null) {
        // textfieldDivs.add(doc.getElementsByTagName("div").item(i));
      } else {
        surfaceDivs.add(doc.getElementsByTagName("div").item(i));
      }
    }

    for (Node node : surfaceDivs) {
      Surface surface = new Surface(node.getAttributes().getNamedItem("data-type").toString()
          .replace("data-type=", "").replace("\"", ""));
      NodeList textfieldDivs = node.getChildNodes();

      for (int y = 0; y < textfieldDivs.getLength(); y++) {
        if (!textfieldDivs.item(y).getNodeName().equals("#text")) {
          Zone zoneForTextField = new Zone(textfieldDivs.item(y).getAttributes()
              .getNamedItem("xml:id").toString().replace("xml:id=\"", "#").replace("\"", ""));

          NodeList furtherDivs = textfieldDivs.item(y).getChildNodes();

          for (int z = 0; z < furtherDivs.getLength(); z++) {
            if (furtherDivs.item(z).getNodeName().equals("ab")) {
              zoneForTextField.addZone(
                  new Zone(
                      furtherDivs.item(z).getAttributes().getNamedItem("xml:id").toString()
                          .replace("xml:id=\"", "#").replaceAll("\"", "")));
            }
          }
          surface.addZone(zoneForTextField);
        }
      }
      this.sourceDoc.addSurface(surface);
    }

    NodeList body = doc.getElementsByTagName("body");
    body.item(0).getAttributes().removeNamedItem("type");

    NodeList bodyChildren = body.item(0).getChildNodes();
    List<Node> bodyChildrenNodeList = new ArrayList<Node>();

    for (int i = 0; i < bodyChildren.getLength(); i++) {
      if (bodyChildren.item(i).getAttributes() != null) {
        bodyChildrenNodeList.add(bodyChildren.item(i));
      }
    }

    for (Node nodes2 : bodyChildrenNodeList) {
      List<Node> textfieldDivs2 = new ArrayList<Node>();

      for (int i = 0; i < nodes2.getChildNodes().getLength(); i++) {
        if (nodes2.getChildNodes().item(i).getNodeName().equals("div")) {
          textfieldDivs2.add(nodes2.getChildNodes().item(i));
        }
      }
      Element element = (Element) nodes2;
      element.getParentNode().removeChild(element);

      for (Node nodes3 : textfieldDivs2) {

        List<Node> nodes = new ArrayList<Node>();
        for (int p = 0; p < element.getChildNodes().getLength(); p++) {
          if (element.getChildNodes().item(p).getAttributes() != null) {
            nodes.add(element.getChildNodes().item(p));
          }
        }

        for (int p = 0; p < nodes.size(); p++) {
          body.item(0).appendChild(nodes3);
        }

        doc.normalize();
      }
    }

    NodeList glyphBlocks = doc.getElementsByTagName("ab");

    tgObjectAsXML = transformXMLObjectToString(doc);

    for (int i = 0; i < doc.getElementsByTagName("ab").getLength(); i++) {
      String transcriptionCode = nodeList.item(i).getTextContent();
      String glyphBlockID = glyphBlocks.item(i).getAttributes().getNamedItem("xml:id").toString()
          .replace("xml:id=\"", "").replace("\"", "");
      Transcription testIt = new Transcription(transcriptionCode).setGlyphBlockID(glyphBlockID)
          .parseTranscriptionCode();

      GlyphBlock ab = new GlyphBlock(glyphBlockID, "", "", "", transcriptionCode);
      ab.addTranscriptionWithCode(testIt);
      tgObjectAsXML = tgObjectAsXML.replace(
          "<ab xml:id=\"" + glyphBlockID + "\">" + transcriptionCode + "</ab>",
          ab.getGlyphblock());
    }

    this.setTeiFile(tgObjectAsXML
        .replace("<TEI xmlns=\"http://www.tei-c.org/ns/1.0\">",
            "<TEI xmlns=\"http://www.tei-c.org/ns/1.0\">" + this.teiheader.getTeiHeader())
        .replace("<text>", this.sourceDoc.getSourceDocInXML() + "<text>"));
  }

  /**
   * @param teiHeader
   * @return
   */
  public TEIheader addTEIheader(TEIheader teiHeader) {
    this.teiheader = teiHeader;
    return this.teiheader;
  }

  /**
   * @param textBodySegment
   * @return
   */
  public TextBodySegment addTextBodySegment(TextBodySegment textBodySegment) {
    this.textbody = textBodySegment;
    return this.textbody;
  }

  /**
   * 
   */
  public void setTEIHeader() {
    //
  }

  /**
   * @return
   */
  public String getTextGridUri() {
    return this.textGridUri;
  }

  /**
   * @param textGridUri
   */
  public void setTextGridUri(String textGridUri) {
    this.textGridUri = textGridUri;
  }

  /**
   * @return
   * @throws Exception
   */
  public String getTeiFile() throws Exception {
    return ParserUtilities.getPrettyString(this.teiFile.replaceAll(">\\s*<", "><"), 2);
  }

  /**
   * @param teiFile
   */
  public void setTeiFile(String teiFile) {
    this.teiFile = teiFile;
  }

  /**
   * @param doc
   * @return
   * @throws TransformerException
   */
  private static String transformXMLObjectToString(Document doc) throws TransformerException {

    StringWriter sw = new StringWriter();
    TransformerFactory tf = TransformerFactory.newInstance();
    Transformer transformer2 = tf.newTransformer();
    transformer2.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
    transformer2.setOutputProperty(OutputKeys.METHOD, "xml");
    transformer2.setOutputProperty(OutputKeys.INDENT, "yes");
    transformer2.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
    transformer2.transform(new DOMSource(doc), new StreamResult(sw));

    return sw.toString();
  }

  /**
   * @param xmlData
   * @param indent
   * @return
   * @throws Exception
   */
  public static String getPrettyString(String xmlData, int indent) throws Exception {

    TransformerFactory transformerFactory = TransformerFactory.newInstance();
    transformerFactory.setAttribute("indent-number", indent);

    Transformer transformer = transformerFactory.newTransformer();
    transformer.setOutputProperty(OutputKeys.INDENT, "yes");

    StringWriter stringWriter = new StringWriter();
    StreamResult xmlOutput = new StreamResult(stringWriter);

    Source xmlInput = new StreamSource(new StringReader(xmlData));
    transformer.transform(xmlInput, xmlOutput);

    return xmlOutput.getWriter().toString();
  }

}
