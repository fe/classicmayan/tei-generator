package org.classicmayan.tools;

public class ParserError {

  private boolean isNumericalTransliterationValid;
  private String parserError;
  private String reason;

  public boolean isNumericalTransliterationValid() {
    return this.isNumericalTransliterationValid;
  }

  public void setNumericalTransliterationValid(boolean isNumericalTransliterationValid) {
    this.isNumericalTransliterationValid = isNumericalTransliterationValid;
  }

  public String getParserError() {
    return this.parserError;
  }

  public void setParserError(String parserError) {
    this.parserError = parserError;
  }

  public String getReason() {
    return this.reason;
  }

  public void setReason(String reason) {
    this.reason = reason;
  }

}
