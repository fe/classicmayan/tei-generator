package org.classicmayan.tools;

public class GlyphBlock {

  private String glyphBlockID;
  private String glyphBlockType;
  private String glyphBlockRendition;
  private String glyphBlockStyle;
  private String numericalTransliteration;
  private int glyphBlockCount = 0;
  private String glyphBlockEnd = "</ab>";
  private Transcription trans;

  public GlyphBlock() {
    setGlyphBlockCount(getGlyphBlockCount() + 1);
  }

  public GlyphBlock(String glyphBlockID, String glyphBlockType, String glyphBlockRendition,
      String glyphBlockStyle, String numericalTransliteration) {
    this.glyphBlockID = glyphBlockID;
    this.glyphBlockType = glyphBlockType;
    this.glyphBlockRendition = glyphBlockRendition;
    this.glyphBlockStyle = glyphBlockStyle;
    this.setNumericalTransliteration(numericalTransliteration);

    setGlyphBlockCount(getGlyphBlockCount() + 1);
  }

  public Transcription addTranscription(Transcription trans) {
    this.trans = trans;
    return trans;
  }

  public Transcription addTranscriptionWithCode(Transcription transcription) {
    this.trans = transcription;
    return this.trans;
  }

  public String getGlyphBlockID() {
    return this.glyphBlockID;
  }

  public void setGlyphBlockID(String glyphBlockID) {
    this.glyphBlockID = glyphBlockID;
  }

  public String getGlyphBlockType() {
    return this.glyphBlockType;
  }

  public void setGlyphBlockType(String glyphBlockType) {
    this.glyphBlockType = glyphBlockType;
  }

  public String getGlyphBlockRendition() {
    return this.glyphBlockRendition;
  }

  public void setGlyphBlockRendition(String glyphBlockRendition) {
    this.glyphBlockRendition = glyphBlockRendition;
  }

  public String getGlyphBlockStyle() {
    return this.glyphBlockStyle;
  }

  public void setGlyphBlockStyle(String glyphBlockStyle) {
    this.glyphBlockStyle = glyphBlockStyle;
  }

  public int getGlyphBlockCount() {
    return this.glyphBlockCount;
  }

  public void setGlyphBlockCount(int glyphBlockCount) {
    this.glyphBlockCount = glyphBlockCount;
  }

  public String getNumericalTransliteration() {
    return this.numericalTransliteration;
  }

  public void setNumericalTransliteration(String numericalTransliteration) {
    this.numericalTransliteration = numericalTransliteration;
  }

  public String getGlyphblock() {
    return "<ab " + "xml:id=\"" + getGlyphBlockID() + "\"" + " n=\"" + getNumericalTransliteration()
        + "\"" + " type=\"glyph-block\">" + this.trans.getTeiCode() + this.glyphBlockEnd;
  }

}
