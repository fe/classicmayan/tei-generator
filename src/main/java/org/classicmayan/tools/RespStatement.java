package org.classicmayan.tools;

/**
 *
 */
public class RespStatement {

  public PersonName persName = new PersonName();

  private boolean nonProjectMember;
  private String respKey;
  private String respKind;
  private String transcriberName;
  private String transcriberKey;
  private final String respStmtBegin = "<respStmt>";
  private final String respStmtEnd = "</respStmt>";
  private String role;

  /**
   * 
   */
  public RespStatement() {

  }

  /**
   * @param transcriberName
   * @param role
   */
  public RespStatement(String transcriberName, String role) {

    this.transcriberName = transcriberName;
    this.checkProjectMembershipOfTranscriberOrMarkupEditor(this.transcriberName);
    this.role = role;
    if (this.role.equals("Transcriber")) {
      this.addTranscriber();
    } else if (this.role.equals("MarkupEditor")) {
      this.addMarkUpEditor();
    }
  }

  /**
   * 
   */
  public void addTranscriber() {
    setRespKey("trc");
    setRespKind("Transcriber");
  }

  /**
   * 
   */
  public void addMarkUpEditor() {
    setRespKey("mrk");
    setRespKind("Markup Editor");
  }

  /**
   * @return
   */
  public String getRespKey() {
    return this.respKey;
  }

  /**
   * @param respKey
   */
  private void setRespKey(String respKey) {
    this.respKey = respKey;
  }

  /**
   * @return
   */
  public String getRespKind() {
    return this.respKind;
  }

  /**
   * @param respKind
   */
  private void setRespKind(String respKind) {
    this.respKind = respKind;
  }

  /**
   * @param transcriberName
   * @return
   */
  public String getTranscriberKey(String transcriberName) {

    if (this.nonProjectMember) {
      if (transcriberName.equals("Sven Gronemeyer")) {
        this.transcriberKey = "#SG";
      } else if (transcriberName.equals("Christian Prager")) {
        this.transcriberKey = "#CP";
      } else if (transcriberName.equals("Elisabeth Wagner")) {
        this.transcriberKey = "#EW";
      } else if (transcriberName.equals("Katja Diederichs")) {
        this.transcriberKey = "#KD";
      } else if (transcriberName.equals("Maximilian Brodhun")) {
        this.transcriberKey = "#MB";
      }
    } else {
      this.setNonProjectStatement(true);
    }

    return this.transcriberKey;
  }

  /**
   * @param transcriberName
   */
  public void setTranscriberName(String transcriberName) {
    this.transcriberName = transcriberName;
  }

  /**
   * @return
   */
  public String getRespInXML() {
    return "<resp key=\"" + getRespKey() + "\">" + getRespKind() + "</resp>";
  }

  /**
   * @return
   */
  public String getNameInXML() {

    if (!this.nonProjectMember) {
      return this.persName.getNameInXML();
    } else {
      return "<name sameAs=\"" + getTranscriberKey(this.transcriberName) + "\"></name>";
    }
  }

  /**
   * @return
   */
  public String getRespStatementInXML() {
    return this.respStmtBegin + getRespInXML() + getNameInXML() + this.respStmtEnd;
  }

  /**
   * @return
   */
  public boolean isNonProjectStatement() {
    return this.nonProjectMember;
  }

  /**
   * @param nonProjectStatement
   */
  public void setNonProjectStatement(boolean nonProjectStatement) {
    this.nonProjectMember = nonProjectStatement;
  }

  /**
   * @return
   */
  public PersonName addPersName() {
    return this.persName;
  }

  /**
   * @return
   */
  public String getRole() {
    return this.role;
  }

  /**
   * @param role
   */
  public void setRole(String role) {
    this.role = role;
  }

  /**
   * @param transcriberName
   */
  public void checkProjectMembershipOfTranscriberOrMarkupEditor(String transcriberName) {

    if (transcriberName.equals("Sven Gronemeyer") ||
        transcriberName.equals("Christian Prager") ||
        transcriberName.equals("Elisabeth Wagner") ||
        transcriberName.equals("Katja Diederichs") ||
        transcriberName.equals("Maximilian Brodhun")) {
      this.setNonProjectStatement(true);
    } else {
      this.setNonProjectStatement(false);
    }
  }

}
