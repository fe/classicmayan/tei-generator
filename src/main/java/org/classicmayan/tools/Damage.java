package org.classicmayan.tools;

public class Damage {

  private Supplied supplied;
  private Unclear unclear;
  private Gap gap;
  private Glyph glyph;

  private String agent;
  private String degree;
  private String quantity;
  private String unit;

  public Damage(String agent, String degree, String quantity, String unit) {
    this.agent = agent;
    this.degree = degree;
    this.quantity = quantity;
    this.unit = unit;
  }

  public Damage() {}

  public Supplied addSupplied(Supplied supplied) {
    this.supplied = supplied;
    return supplied;
  }

  public Glyph addGlyph(Glyph glyph) {
    this.glyph = glyph;
    return glyph;
  }

  public Unclear addUnclear() {
    return this.unclear;
  }

  public Gap addGap() {
    return this.gap;
  }

  public String getDamage() {
    if (this.glyph != null && this.glyph.getSignCatalogReference() == null
        && this.glyph.getGlyphNumber() == null) {
      return "<damage" +
          " agent=\"" + this.agent + "\"" +
          " degree=\"" + this.degree + "\"" +
          " quantity=\"" + this.quantity + "\"" +
          " unit=\"" + this.unit + "\">" + this.glyph.getTEICode() + "</damage>";
    }

    return "<damage" +
        " agent=\"" + this.agent + "\"" +
        " degree=\"" + this.degree + "\"" +
        " quantity=\"" + this.quantity + "\"" +
        " unit=\"" + this.unit + "\">" + this.supplied.getSupplied() + "</damage>";
  }

  public String getAgent() {
    return this.agent;
  }

  public void setAgent(String agent) {
    // TODO: get from sessionID
    this.agent = agent;
  }

  public String getDegree() {
    return this.degree;
  }

  public void setDegree(String degree) {

    this.degree = degree;
  }

  public String getQuantity() {
    return this.quantity;
  }

  public void setQuantity(String quantity) {
    this.quantity = quantity;
  }

  public String getUnit() {
    return this.unit;
  }

  public void setUnit(String unit) {
    this.unit = unit;
  }

}
