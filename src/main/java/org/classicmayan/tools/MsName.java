package org.classicmayan.tools;

public class MsName {

  private String msName;
  private final String msNameBegin = "<msName>";
  private final String msNameEnd = "</msName>";

  public String getMsName() {
    return this.msNameBegin + this.msName + this.msNameEnd;
  }

  public void setMsName(String msName) {
    this.msName = msName;
  }
}
