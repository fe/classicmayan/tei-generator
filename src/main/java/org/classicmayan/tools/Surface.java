package org.classicmayan.tools;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class Surface {

  private String type;
  List<Zone> zones = new ArrayList<Zone>();

  /**
   * @param type
   */
  public Surface(String type) {
    this.type = type;
  }

  /**
   * @param zone
   * @return
   */
  public List<Zone> addZone(Zone zone) {
    this.zones.add(zone);
    return this.zones;
  }

  /**
   * @return
   */
  public String getType() {
    return this.type;
  }

  /**
   * @param type
   */
  public void setType(String type) {
    this.type = type;
  }

  /**
   * @return
   */
  public String getSurfaceXML() {

    String zonesString = "";
    for (Zone zone : this.zones) {
      zonesString = zonesString.concat(zone.getZoneXML());
    }

    return "<surface type=\"" + getType() + "\">" + zonesString + "</surface>";
  }

}
