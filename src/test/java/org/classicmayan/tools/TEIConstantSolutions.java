package org.classicmayan.tools;

/**
 *
 */
public class TEIConstantSolutions {

  public static final String subSegments =
      "<ab xml:id=\"A1\" n=\"59st.[684st^[168bt:188st]]\" type=\"glyph-block\"><g xml:id=\"A1G1\" n=\"59st\" ref=\"textgrid:30ng4\" rend=\"left_beside\" corresp=\"#A1S1\"/><seg xml:id=\"A1S1\" type=\"glyph-group\" rend=\"right_beside\" corresp=\"#A1G1\"><g xml:id=\"A1G2\" n=\"684st\" ref=\"NA\" rend=\"interferes_with\" corresp=\"#A1S2\"/><seg xml:id=\"A1S2\" type=\"glyph-group\" rend=\"is_interfered_by\" corresp=\"#A1G2\"><g xml:id=\"A1G3\" n=\"168bt\" ref=\"textgrid:34p4g\" rend=\"above\" corresp=\"#A1G4\"/><g xml:id=\"A1G4\" n=\"188st\" ref=\"textgrid:2smqc\" rend=\"beneath\" corresp=\"#A1G3\"/></seg></seg></ab>";
  public static final String asteriskDamage =
      "<ab xml:id=\"A1\" n=\"5004st.[*1000st:713bb].181br\" type=\"glyph-block\"><g xml:id=\"A1G1\" n=\"5004st\" ref=\"NA\" rend=\"left_beside\" corresp=\"#A1S1\"/><seg xml:id=\"A1S1\" type=\"glyph-group\" rend=\"right_beside\" corresp=\"#A1G1\"><damage agent=\"\" degree=\"1\" quantity=\"\" unit=\"\"><supplied reason=\"damage\" evidence=\"\" precision=\"\" ana=\"\"><g xml:id=\"A1G2\" n=\"1000st\" ref=\"NA\" rend=\"above\" corresp=\"#A1G3\"/></supplied></damage><g xml:id=\"A1G3\" n=\"713bb\" ref=\"NA\" rend=\"beneath\" corresp=\"#A1G2\"/></seg><g xml:id=\"A1G4\" n=\"181br\" ref=\"textgrid:36q3h\" rend=\"right_beside\" corresp=\"#A1S1\"/></ab>";
  public static final String hyphenWithDamage =
      "<ab xml:id=\"A1\" n=\"??-??\" type=\"glyph-block\"><damage agent=\"\" degree=\"\" quantity=\"1\" unit=\"g\"><supplied reason=\"damage\" evidence=\"\" precision=\"\" ana=\"\"><g xml:id=\"A1G1\" n=\"\" ref=\"\" rend=\"unknown\"/></supplied></damage><damage agent=\"\" degree=\"\" quantity=\"1\" unit=\"g\"><supplied reason=\"damage\" evidence=\"\" precision=\"\" ana=\"\"><g xml:id=\"A1G2\" n=\"\" ref=\"\" rend=\"unknown\"/></supplied></damage></ab>";
  public static final String choiceInDamage =
      "<ab xml:id=\"A1\" n=\"?.[28bv:142th]\" type=\"glyph-block\"><damage agent=\"\" degree=\"\" quantity=\"1\" unit=\"g\"><supplied reason=\"damage\" evidence=\"\" ana=\"\"><choice><supplied precision=\"\"><g xml:id=\"A1G1a\" n=\"\" ref=\"\" rend=\"left_beside\" corresp=\"#A1S1\"/></supplied><supplied precision=\"\"><g xml:id=\"A1G1b\" n=\"\" ref=\"\" rend=\"left_beside\" corresp=\"#A1S1\"/></supplied></choice></supplied></damage><seg xml:id=\"A1S1\" type=\"glyph-group\" rend=\"right_beside\" corresp=\"#A1G1\"><g xml:id=\"A1G2\" n=\"28bv\" ref=\"textgrid:327v8\" rend=\"above\" corresp=\"#A1G3\"/><g xml:id=\"A1G3\" n=\"142th\" ref=\"NA\" rend=\"beneath\" corresp=\"#A1G2\"/></seg></ab>";
  public static final String doubleQuestionMark =
      "<ab xml:id=\"A1\" n=\"45st.??:583st\" type=\"glyph-block\"><g xml:id=\"A1G1\" n=\"45st\" ref=\"textgrid:3r5kf\" rend=\"left_beside\" corresp=\"#A1G2\"/><damage agent=\"\" degree=\"\" quantity=\"1\" unit=\"g\"><g xml:id=\"A1G2\" rend=\"right_beside\"/></damage><g xml:id=\"A1G3\" n=\"583st\" ref=\"NA\" rend=\"beneath\" corresp=\"#A1G2\"/></ab>";
  public static final String justDoubleQuestionmark =
      "<ab xml:id=\"A1\" n=\"??.??\" type=\"glyph-block\"><damage agent=\"\" degree=\"\" quantity=\"1\" unit=\"g\"><g xml:id=\"A1G1\" rend=\"left_beside\"/></damage><damage agent=\"\" degree=\"\" quantity=\"1\" unit=\"g\"><g xml:id=\"A1G2\" rend=\"right_beside\"/></damage></ab>";
  public static final String signAttachedQuestionmark =
      "<ab xml:id=\"A1\" n=\"?.5009st?\" type=\"glyph-block\"><damage agent=\"\" degree=\"\" quantity=\"1\" unit=\"g\"><supplied reason=\"damage\" evidence=\"\" ana=\"\"><choice><supplied precision=\"\"><g xml:id=\"A1G1a\" n=\"\" ref=\"\" rend=\"left_beside\" corresp=\"#A1G2\"/></supplied><supplied precision=\"\"><g xml:id=\"A1G1b\" n=\"\" ref=\"\" rend=\"left_beside\" corresp=\"#A1G2\"/></supplied></choice></supplied></damage><damage agent=\"\" degree=\"\" quantity=\"1\" unit=\"g\"><supplied reason=\"damage\" evidence=\"\" precision=\"\" ana=\"\"><g xml:id=\"A1G2\" n=\"5009st\" ref=\"NA\" rend=\"right_beside\" corresp=\"#A1G1\"/></supplied></damage></ab>";
  public static final String cornerRenditionWithTwoSign =
      "<ab xml:id=\"A1\" n=\"524ey╝529st\" type=\"glyph-block\"><g xml:id=\"A1G1\" n=\"524ey\" ref=\"NA\" rend=\"inserted_in_top_left_corner\" corresp=\"#A1G2\"/><g xml:id=\"A1G2\" n=\"529st\" ref=\"NA\" rend=\"is_bent_around\" corresp=\"#A1G1\"/></ab>";
  public static final String unclosedSegments =
      "<ab xml:id=\"A1\" n=\"59st*[684st[168bt:188st]\" type=\"glyph-block\"><error>BLOCK NOT PARSED: Invalid graphotactical operator set!</error><error>BLOCK NOT PARSED: Segment brackets are not set correctly!</error><error>BLOCK NOT PARSED: Operator between signs or segments missing!</error></ab>";
  public static final String hashTag =
      "<ab xml:id=\"A1\" n=\"#\" type=\"glyph-block\"><gap reason=\"\" extent=\"unknown\" unit=\"g\"/></ab>";
  public static final String gapWithThreePoints =
      "<gap reason=\"\" extent=\"unknown\" unit=\"ab\"/>";
  public static final String invalidRenditionOperator =
      "<ab xml:id=\"A1\" n=\"59st%[684st[168bt:188st]\" type=\"glyph-block\"><error>BLOCK NOT PARSED: Invalid graphotactical operator set!</error><error>BLOCK NOT PARSED: Segment brackets are not set correctly!</error><error>BLOCK NOT PARSED: Operator between signs or segments missing!</error></ab>";
  public static final String missingOperatorBetweenSegments =
      "<ab xml:id=\"A1\" n=\"59st.[684st[168bt:188st]]\" type=\"glyph-block\"><error>BLOCK NOT PARSED: Operator between signs or segments missing!</error></ab>";
  public static final String missingOperatorBetweenTwoSigns =
      "<ab xml:id=\"A1\" n=\"59st.[684st:[168bt188st]]\" type=\"glyph-block\"><error>BLOCK NOT PARSED: Operator between signs or segments missing!</error><error>BLOCK NOT PARSED: Sign is entered in catalogue but not with this graph!</error></ab>";
  public static final String missingSignBetweenOperator =
      "<ab xml:id=\"A1\" n=\"59st.[684st.[168bt:188st.]]\" type=\"glyph-block\"><error>BLOCK NOT PARSED: Graph number is missing between operators!</error></ab>";
  public static final String moreThenTwoSignsDividedBySameOperator =
      "<ab xml:id=\"A1\" n=\"59st.684st.168bt\" type=\"glyph-block\"><error>BLOCK NOT PARSED: Segment brackets are missing!</error></ab>";
}
