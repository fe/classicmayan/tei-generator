package org.classicmayan.tools;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;
import org.apache.cxf.helpers.IOUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;
import info.textgrid.clients.tgcrud.CrudClientException;
import info.textgrid.clients.tgcrud.TextGridObject;

/**
 * TODO Check test class!
 */
@Ignore
public class TestParseTranscriptionCodeToTEI {

  private static final String SID = "***";

  /**
   * Create the test case
   *
   * @param testName name of the test case
   * @throws CrudClientException
   * @throws TransformerException
   * @throws TransformerFactoryConfigurationError
   * @throws IOException
   * @throws SAXException
   */
  @Test
  public void testTGSearchQuery() throws IOException {
    TGSearchQueries t = new TGSearchQueries();
    HashMap<String, String> test = t.getTitleWithURI();

    for (String name : test.keySet()) {
      String key = name.toString();
      String value = test.get(name).toString();
      System.out.println(key + " || " + value);
    }
  }

  /**
   * @throws TransformerFactoryConfigurationError
   * @throws TransformerException
   * @throws IOException
   * @throws CrudClientException
   * @throws SAXException
   */
  @Test
  public void testGetTextObjectAsDocument() throws TransformerFactoryConfigurationError,
      TransformerException, IOException, CrudClientException, SAXException {
    Document doc = ParserUtilities.tgObjectToDomDoc("textgrid:3rx6v.0", SID);
    System.out.println(doc.getElementsByTagName("ab").getLength());
  }

  /**
   * @throws TransformerFactoryConfigurationError
   * @throws TransformerException
   * @throws IOException
   * @throws CrudClientException
   * @throws SAXException
   */
  @Test
  public void testGetGlyphBlockById() throws TransformerFactoryConfigurationError,
      TransformerException, IOException, CrudClientException, SAXException {
    Document doc = ParserUtilities.tgObjectToDomDoc("textgrid:3rx6v.0", SID);
    Element element = ParserUtilities.getGlyphBlock(doc, "A3-B3");
    System.out.println(element);
  }

  /**
   * 
   */
  @Test
  public void testEndigSegmentIndex() {
    ParserUtilities.getIndexOfClosingSignOfOpenSegment(
        ParserUtilities.unitDivider("[1st:[5004st.[534st:102st]]:[679st.[513st:59st]]].45gz"));
  }

  /**
   * 
   */
  @Test
  public void testSegmentRenditionCalculation() {
    ParserUtilities.calculateRenditionOperatorForSegment(
        ParserUtilities.unitDivider("[1st:[5004st.[534st:102st]]]:[[679st.[513st:59st]].45gz]"));
  }


  /**
   * 
   */
  @Test
  public void testAmountOpenSegmentFromSpecificIndex() {
    System.out.println(ParserUtilities.amountOpenSegmentFromSpecificIndex(
        ParserUtilities.unitDivider("[[1st:[5004st.[534st:102st]]]:[[679st.[513st:59st]].45gz]]"),
        0));
  }

  /**
   * @throws CrudClientException
   * @throws IOException
   */
  @Test
  public void testTeiHeaderNew() throws CrudClientException, IOException {
    TEIheader teiHeader = new TEIheader("textgrid:3r4ft.0", "textgrid:3r111", SID);
    System.out.println(teiHeader.getTeiHeader());
  }

  /**
   * 
   */
  @Test
  public void testValidateEnclosingBrackets() {
    System.out.println(ParserUtilities.getIndexOfCorrespondingForNestedSegments(
        ParserUtilities.unitDivider("[[1st:[5004st.[534st:102st]]]:[[679st.[513st:59st]].45gz]]"),
        4, 1));
    System.out.println(ParserUtilities
        .unitDivider("[[1st:[5004st.[534st:102st]]]:[[679st.[513st:59st]].45gz]]").get(12));
    System.out.println(ParserUtilities
        .unitDivider("[[1st:[5004st.[534st:102st]]]:[[679st.[513st:59st]].45gz]]").get(10));
    // System.out.println(ParserUtilities.unitDivider("[[1st:[5004st.[534st:102st]]]:[[679st.[513st:59st]].45gz]]").size());
    ParserError parserError = ParserUtilities.validateNoEnclosingBracketsForWholeTranscriptionCode(
        ParserUtilities.unitDivider("[[501st:501st:501st].[501st:[501st°501st]]]"));
    System.out
        .println(parserError.isNumericalTransliterationValid() + " : " + parserError.getReason());
  }

  /**
   * @throws CrudClientException
   * @throws IOException
   */
  @Test
  public void testTeiHeaderWithNoMemberOfProject() throws CrudClientException, IOException {
    RespStatement respStmt = new RespStatement("Paul Bearer", "Transcriber");
    respStmt.addPersName();
    System.out.println(respStmt.getRespStatementInXML());
  }

  /**
   * @throws CrudClientException
   * @throws IOException
   */
  @Test
  public void testGenerateSourceDoc() throws CrudClientException, IOException {
    TextGridObject tgObject = ParserUtilities.getTextGridObject("textgrid:3r4fp.0", SID);
    String tgObjectAsXML = IOUtils.readStringFromStream(tgObject.getData());
    TEIsourceDoc sourceDoc = new TEIsourceDoc(tgObjectAsXML);
    System.out.println(sourceDoc.getTEISourceDoc());
  }

  /**
   * @throws TransformerFactoryConfigurationError
   * @throws Exception
   */
  @Test
  public void testGetCompleteTeiFile() throws TransformerFactoryConfigurationError, Exception {
    System.out.println(new TEIFile("textgrid:3vpxh.0", "textgrid:3r111", SID).getTeiFile());
  }

  /**
   * 
   */
  @Test
  public void parseBlock() {
    String transcriptionCode = "[[17st.819br]:229bl].136mp";
    // String transcriptionCode = "679.[513bv:59st]";
    Transcription testIt =
        new Transcription(transcriptionCode).setGlyphBlockID("B7").parseTranscriptionCode();
    GlyphBlock ab = new GlyphBlock("B7", "", "", "", transcriptionCode);
    ab.addTranscriptionWithCode(testIt);
    System.out.println(ab.getGlyphblock());
  }

  /**
   * 
   */
  @Test
  public void testCountSegmentsTillSpecificLimit() {
    String transcriptionCode =
        "[1br:[5004st.[178bl:103ex]]:[679.[513bv:59st]]].[[5009st.[1500bv°524ex]]:[5002st.[?:130bh]]]";
    System.out.println(ParserUtilities
        .countSegmentsTillSpecificIndex(ParserUtilities.unitDivider(transcriptionCode), 36));
  }

  /**
   * 
   */
  @Test
  public void testAsteriskDamage() {
    String transcriptionCode = "5004st.[*1000st:713bb].181br";
    Transcription testIt =
        new Transcription(transcriptionCode).setGlyphBlockID("A1").parseTranscriptionCode();
    GlyphBlock ab = new GlyphBlock("A1", "", "", "", transcriptionCode);
    ab.addTranscriptionWithCode(testIt);
    String teiCode = ab.getGlyphblock();

    if (teiCode.equals(TEIConstantSolutions.asteriskDamage)) {
      System.out.println(teiCode);
    } else {
      System.out.println("Error in testAsteriskDamage");
      System.out.println("TEI code is: ");
      System.out.println(teiCode);
      System.out.println("But should be: ");
      System.out.println(TEIConstantSolutions.asteriskDamage);
    }
  }

  /**
   * 
   */
  @Test
  public void testUnitDivider() {
    String transcriptionCode = "??-??";
    for (String element : ParserUtilities.unitDivider(transcriptionCode)) {
      System.out.println(element);
    }
  }

  /**
   * 
   */
  @Test
  public void testChoiceInDamage() {
    String transcriptionCode = "?.[28bv:142th]";
    Transcription testIt =
        new Transcription(transcriptionCode).setGlyphBlockID("A1").parseTranscriptionCode();
    GlyphBlock ab = new GlyphBlock("A1", "", "", "", transcriptionCode);
    ab.addTranscriptionWithCode(testIt);
    String teiCode = ab.getGlyphblock();

    if (teiCode.equals(TEIConstantSolutions.choiceInDamage)) {
      System.out.println(ab.getGlyphblock());
    } else {
      System.out.println("Error in testChoiceInDamage()");
      System.out.println("TEI code is: ");
      System.out.println(teiCode);
      System.out.println("But should be: ");
      System.out.println(TEIConstantSolutions.choiceInDamage);
    }
  }

  /**
   * 
   */
  @Test
  public void testDoubleQuestionMark() {
    String transcriptionCode = "45st.??:583st";
    Transcription testIt =
        new Transcription(transcriptionCode).setGlyphBlockID("A1").parseTranscriptionCode();
    GlyphBlock ab = new GlyphBlock("A1", "", "", "", transcriptionCode);
    ab.addTranscriptionWithCode(testIt);
    String teiCode = ab.getGlyphblock();

    if (teiCode.equals(TEIConstantSolutions.doubleQuestionMark)) {
      System.out.println(ab.getGlyphblock());
    } else {
      System.out.println("Error in testDoubleQuestionMark()");
      System.out.println("TEI code is: ");
      System.out.println(teiCode);
      System.out.println("But should be: ");
      System.out.println(TEIConstantSolutions.doubleQuestionMark);
    }
  }

  /**
   * 
   */
  @Test
  public void testJustDoubleQuestionmarks() {
    String transcriptionCode = "??.??";
    Transcription testIt =
        new Transcription(transcriptionCode).setGlyphBlockID("A1").parseTranscriptionCode();
    GlyphBlock ab = new GlyphBlock("A1", "", "", "", transcriptionCode);
    ab.addTranscriptionWithCode(testIt);
    String teiCode = ab.getGlyphblock();

    if (teiCode.equals(TEIConstantSolutions.justDoubleQuestionmark)) {
      System.out.println(teiCode);
    } else {
      System.out.println("Error in testJustDoubleQuestionmarks");
      System.out.println("TEI code is: ");
      System.out.println(teiCode);
      System.out.println("But should be: ");
      System.out.println(TEIConstantSolutions.justDoubleQuestionmark);
    }
  }

  /**
   * 
   */
  @Test
  public void testAttachedQuestionMark() {
    String transcriptionCode = "?.5009st?";
    Transcription testIt =
        new Transcription(transcriptionCode).setGlyphBlockID("A1").parseTranscriptionCode();
    GlyphBlock ab = new GlyphBlock("A1", "", "", "", transcriptionCode);
    ab.addTranscriptionWithCode(testIt);
    String teiCode = ab.getGlyphblock();
    if (teiCode.equals(TEIConstantSolutions.signAttachedQuestionmark)) {
      System.out.println(teiCode);
    } else {
      System.out.println("Error in testAttachedQuestionMark");
      System.out.println("TEI code is: ");
      System.out.println(teiCode);
      System.out.println("But should be: ");
      System.out.println(TEIConstantSolutions.signAttachedQuestionmark);
    }
  }

  /**
   * 
   */
  @Test
  public void testSignsInCornerWithDoubleSign() {

    String transcriptionCode = "524ex╝529st";
    Transcription testIt =
        new Transcription(transcriptionCode).setGlyphBlockID("A1").parseTranscriptionCode();
    GlyphBlock ab = new GlyphBlock("A1", "", "", "", transcriptionCode);
    ab.addTranscriptionWithCode(testIt);
    System.out.println(ab.getGlyphblock());
  }

  /**
   * 
   */
  @Test
  public void testSignsInCornerWithSignSign() {
    String transcriptionCode = "[14st┌111st]:4bh";
    Transcription testIt =
        new Transcription(transcriptionCode).setGlyphBlockID("A1").parseTranscriptionCode();
    GlyphBlock ab = new GlyphBlock("A1", "", "", "", transcriptionCode);
    ab.addTranscriptionWithCode(testIt);
    System.out.println(ab.getGlyphblock());
  }

  /**
   * 
   */
  @Test
  public void testSignsInCornerThreeStackedSigns() {
    String transcriptionCode = "59st.[169bt:188st:188st]";
    Transcription testIt =
        new Transcription(transcriptionCode).setGlyphBlockID("A1").parseTranscriptionCode();
    GlyphBlock ab = new GlyphBlock("A1", "", "", "", transcriptionCode);
    ab.addTranscriptionWithCode(testIt);
    System.out.println(ab.getGlyphblock());
  }

  /**
   * 
   */
  @Test
  public void testSubSegments() {
    String transcriptionCode = "59st.[684st^[168bt:188st]]";
    Transcription testIt =
        new Transcription(transcriptionCode).setGlyphBlockID("A1").parseTranscriptionCode();
    GlyphBlock ab = new GlyphBlock("A1", "", "", "", transcriptionCode);
    ab.addTranscriptionWithCode(testIt);
    String teiCode = ab.getGlyphblock();
    if (teiCode.equals(TEIConstantSolutions.subSegments)) {
      System.out.println(teiCode);
    } else {
      System.out.println("Error in testSubSegments()");
      System.out.println("TEI code is: ");
      System.out.println(teiCode);
      System.out.println("But should be: ");
      System.out.println(TEIConstantSolutions.subSegments);
    }
  }

  /**
   * 
   */
  @Test
  public void testHyphenWithDamagedSigns() {
    String transcriptionCode = "??-??";
    Transcription testIt =
        new Transcription(transcriptionCode).setGlyphBlockID("A1").parseTranscriptionCode();
    GlyphBlock ab = new GlyphBlock("A1", "", "", "", transcriptionCode);
    ab.addTranscriptionWithCode(testIt);
    String teiCode = ab.getGlyphblock();

    if (teiCode.equals(TEIConstantSolutions.hyphenWithDamage)) {
      System.out.println(ab.getGlyphblock());
    } else {
      System.out.println("Error in testHyphenWithDamagedSigns()");
      System.out.println("TEI code is: ");
      System.out.println(teiCode);
      System.out.println("But should be: ");
      System.out.println(TEIConstantSolutions.hyphenWithDamage);
    }
  }

  /**
   * 
   */
  @Test
  public void testCornerWithTwoSign() {
    String transcriptionCode = "524ey╝529st";
    Transcription testIt =
        new Transcription(transcriptionCode).setGlyphBlockID("A1").parseTranscriptionCode();
    GlyphBlock ab = new GlyphBlock("A1", "", "", "", transcriptionCode);
    ab.addTranscriptionWithCode(testIt);
    String teiCode = ab.getGlyphblock();
    if (teiCode.equals(TEIConstantSolutions.cornerRenditionWithTwoSign)) {
      System.out.println(teiCode);
    } else {
      System.out.println("Error in testCornerWithTwoSign()");
      System.out.println("TEI code is: ");
      System.out.println(teiCode);
      System.out.println("But should be: ");
      System.out.println(TEIConstantSolutions.cornerRenditionWithTwoSign);
    }
  }

  /**
   * 
   */
  @Test
  public void testUnclosedSegment() {
    String transcriptionCode = "59st*[684st[168bt:188st]";
    Transcription testIt =
        new Transcription(transcriptionCode).setGlyphBlockID("A1").parseTranscriptionCode();
    GlyphBlock ab = new GlyphBlock("A1", "", "", "", transcriptionCode);
    ab.addTranscriptionWithCode(testIt);
    String teiCode = ab.getGlyphblock();

    if (teiCode.equals(TEIConstantSolutions.unclosedSegments)) {
      System.out.println(teiCode);
    } else {
      System.out.println("Error in testUnclosedSegment()");
    }
  }

  /**
   * 
   */
  @Test
  public void testValidateSegments() {
    String transcriptionCode = "59st*[684st[168bt:188st]";
    ParserUtilities
        .validateClosedAndUnclosedSegments(ParserUtilities.unitDivider(transcriptionCode));
  }

  /**
   * 
   */
  @Test
  public void testHashTag() {
    String transcriptionCode = "#";
    Transcription testIt =
        new Transcription(transcriptionCode).setGlyphBlockID("A1").parseTranscriptionCode();
    GlyphBlock ab = new GlyphBlock("A1", "", "", "", transcriptionCode);
    ab.addTranscriptionWithCode(testIt);
    String teiCode = ab.getGlyphblock();
    if (teiCode.equals(TEIConstantSolutions.hashTag)) {
      System.out.println(teiCode);
    } else {
      System.out.println("Error in testHashTag()");
      System.out.println("TEI code is: ");
      System.out.println(teiCode);
      System.out.println("But should be: ");
      System.out.println(TEIConstantSolutions.hashTag);
    }
  }

  /**
   * 
   */
  @Test
  public void testGapWithThreePoints() {
    String transcriptionCode = "…";
    Transcription testIt =
        new Transcription(transcriptionCode).setGlyphBlockID("A1").parseTranscriptionCode();
    String teiCode = testIt.getTeiCode();

    if (teiCode.equals(TEIConstantSolutions.gapWithThreePoints)) {
      System.out.println(teiCode);
    } else {
      System.out.println("Error in testGapWithThreePoints()");
      System.out.println("TEI code is: ");
      System.out.println(teiCode);
      System.out.println("But should be: ");
      System.out.println(TEIConstantSolutions.gapWithThreePoints);
    }
  }

  /**
   * 
   */
  @Test
  public void testCountGlyphsInSegment() {
    String transcriptionCode = "59st.[169bt:188st:188st]";
    System.out.println(
        "The numerical transliteration " + transcriptionCode + "has an amount of glyphs of: ");
    System.out.println(ParserUtilities
        .getQuantityOfGlyphsInSegment(ParserUtilities.unitDivider(transcriptionCode)));
  }

  /**
   * 
   */
  @Test
  public void testIsDamageSign() {
    System.out.println(ParserUtilities.isDamageSign("?"));
  }


  /**
   * 
   */
  @Test
  public void testValidateRenditionOperators() {
    String transcriptionCode = "59st%[684st[168bt:188st]";
    Transcription testIt =
        new Transcription(transcriptionCode).setGlyphBlockID("A1").parseTranscriptionCode();
    GlyphBlock ab = new GlyphBlock("A1", "", "", "", transcriptionCode);
    ab.addTranscriptionWithCode(testIt);
    String teiCode = ab.getGlyphblock();

    if (teiCode.equals(TEIConstantSolutions.invalidRenditionOperator)) {
      System.out.println(teiCode);
    } else {
      System.out.println("Error in testValidateRenditionOperators()");
      System.out.println("TEI code is: ");
      System.out.println(teiCode);
      System.out.println("But should be: ");
      System.out.println(TEIConstantSolutions.invalidRenditionOperator);
    }
  }

  /**
   * 
   */
  @Test
  public void testMissingOperatorBetweenSignAndSegment() {
    String transcriptionCode = "59st.[684st[168bt:188st]]";
    Transcription testIt =
        new Transcription(transcriptionCode).setGlyphBlockID("A1").parseTranscriptionCode();
    GlyphBlock ab = new GlyphBlock("A1", "", "", "", transcriptionCode);
    ab.addTranscriptionWithCode(testIt);
    String teiCode = ab.getGlyphblock();

    if (teiCode.equals(TEIConstantSolutions.missingOperatorBetweenSegments)) {
      System.out.println(teiCode);
    } else {
      System.out.println("Error in testMissingOperatorBetweenSignAndSegment()");
      System.out.println("TEI code is: ");
      System.out.println(teiCode);
      System.out.println("But should be: ");
      System.out.println(TEIConstantSolutions.missingOperatorBetweenSegments);
    }
  }

  /**
   * 
   */
  @Test
  public void testMissingOperatorBetweenTwoSigns() {
    String transcriptionCode = "59st.[684st:[168bt188st]]";
    Transcription testIt =
        new Transcription(transcriptionCode).setGlyphBlockID("A1").parseTranscriptionCode();
    GlyphBlock ab = new GlyphBlock("A1", "", "", "", transcriptionCode);
    ab.addTranscriptionWithCode(testIt);
    String teiCode = ab.getGlyphblock();

    if (teiCode.equals(TEIConstantSolutions.missingOperatorBetweenTwoSigns)) {
      System.out.println(teiCode);
    } else {
      System.out.println("Error in testMissingOperatorBetweenTwoSigns()");
      System.out.println("TEI code is: ");
      System.out.println(teiCode);
      System.out.println("But should be: ");
      System.out.println(TEIConstantSolutions.missingOperatorBetweenTwoSigns);
    }
  }

  /**
   * 
   */
  @Test
  public void testMissingSign() {
    String transcriptionCode = "59st.[684st.[168bt:188st.]]";
    Transcription testIt =
        new Transcription(transcriptionCode).setGlyphBlockID("A1").parseTranscriptionCode();
    GlyphBlock ab = new GlyphBlock("A1", "", "", "", transcriptionCode);
    ab.addTranscriptionWithCode(testIt);
    String teiCode = ab.getGlyphblock();

    if (teiCode.equals(TEIConstantSolutions.missingSignBetweenOperator)) {
      System.out.println(teiCode);
    } else {
      System.out.println("Error in testMissingSign()");
      System.out.println("TEI code is: ");
      System.out.println(teiCode);
      System.out.println("But should be: ");
      System.out.println(TEIConstantSolutions.missingSignBetweenOperator);
    }
  }

  /**
   * 
   */
  @Test
  public void testIsGraphNumberInsideSignCatalogue() {
    String transcriptionCode = "?.5009st?";
    Transcription testIt =
        new Transcription(transcriptionCode).setGlyphBlockID("A1").parseTranscriptionCode();
    GlyphBlock ab = new GlyphBlock("A1", "", "", "", transcriptionCode);
    ab.addTranscriptionWithCode(testIt);
    System.out.println(ab.getGlyphblock());
  }

  /**
   * 
   */
  @Test
  public void testMoreThanTwoSignsDividedByDifferentOperators() {
    String transcriptionCode = "59st.684st.168bt";
    Transcription testIt =
        new Transcription(transcriptionCode).setGlyphBlockID("A1").parseTranscriptionCode();
    GlyphBlock ab = new GlyphBlock("A1", "", "", "", transcriptionCode);
    ab.addTranscriptionWithCode(testIt);
    String teiCode = ab.getGlyphblock();
    if (teiCode.equals(TEIConstantSolutions.moreThenTwoSignsDividedBySameOperator)) {
      System.out.println(teiCode);
    } else {
      System.out.println("Error in testMoreThanTwoSignsDividedByDifferentOperators()");
    }
  }

  /**
   * 
   */
  @Test
  public void signNumberIsEnteredInSignCatalog() {
    System.out.println(ParserUtilities.getUriOfSignNumber("5009st"));
  }

  /**
   * 
   */
  @Test
  public void graphVariationIsEnteredInSignCatalog() {
    System.out.println(ParserUtilities
        .validateGraphIsEnteredInSignCatalogue(ParserUtilities.unitDivider("5009s")));
  }

  /**
   * 
   */
  @Test
  public void testCutSegments() {

    List<List<String>> bla = new ArrayList<List<String>>();

    bla = ParserUtilities
        .cutSegments(ParserUtilities.unitDivider("[182bt:32vb].1000st:[182bt:32vb]"));
    System.out.println(bla.size());
    for (int i = 0; i < bla.size(); i++) {
      int j = 0;
      for (String blubb : bla.get(j)) {
        System.out.print(blubb);
      }
      j++;
      System.out.println("\n");
    }
  }

  /**
   * 
   */
  @Test
  public void getAllIndices() {

    List<Integer> segments = new ArrayList<Integer>();

    segments = ParserUtilities.getAllIndicesOfClosingSegmentSigns(
        ParserUtilities.unitDivider("[182bt:32vb].1000st:[182bt:32vb].[121po°34zu]"));

    for (Integer segment : segments) {
      System.out.println(segment);
    }
  }

}
