# [1.2.0](https://gitlab.gwdg.de/fe/classicmayan/tei-generator/compare/v1.1.0...v1.2.0) (2022-11-28)


### Bug Fixes

* increase version ([336eb29](https://gitlab.gwdg.de/fe/classicmayan/tei-generator/commit/336eb29aa8ccd76f9c8db07453c723abe30972c9))


### Features

* add new gitlab CI workflow, and more ([0e4ac30](https://gitlab.gwdg.de/fe/classicmayan/tei-generator/commit/0e4ac302a9ccfe6ada13c646c85a7bc3f4d454a2))
