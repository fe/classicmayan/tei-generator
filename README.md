# ClassicMayan Tools – Queries Collection

## General

A webservice for creating the TEI-Files for the IDIOM-Project.

    mvn -U clean install

    mvn eclipse:clean eclipse:eclipse

    http://localhost:3131


Takes a XML-Template-File containing several string for the lingusitic annotation.


```
<?xml version="1.0" encoding="UTF-8"?> <?xml-model href="textgrid:3qtrg" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?> <?xml-model href="textgrid:3qtrg" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
    <text>
        <body type="parser-data">
            <div data-tag="surface" data-type="front">
                <div xml:id="front" type="textfield">
                    <ab xml:id="A1-B2">507vb:[1600bh+204bv]:[1604bh?+548bv]</ab>
                    <ab xml:id="A3">5009st:[177bb:186st]</ab>
                    <ab xml:id="B3">5012st.28tv</ab>
                    <ab xml:id="A4">5010st:548bt</ab>
                </div>
            </div>
        </body>
    </text>
</TEI>
```

This input will be processed to the output:

```
<?xml version="1.0" encoding="UTF-8"?><?xml-model href="textgrid:3qtrg" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?><?xml-model href="textgrid:3qtrg" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?><TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>Uxul, Altar 2</title>
        <respStmt>
          <resp key="trc">Transcriber</resp>
          <name sameAs="#CP"/>
        </respStmt>
      </titleStmt>
      <publicationStmt>
        <p>
          <listBibl>
            <bibl xml:lang="en">Digital Corpus of Classic Mayan. Edited by "Textdatabase and
                     Dictionary of Classic Mayan", Research Center of the North Rhine-Westphalian
                     Academy of Sciences, Humanities and the Arts. http://www.classicmayan.org,
                     [year], accessed online: [date of access]</bibl>
            <bibl xml:lang="de">Digitales Korpus des Klassischen Maya. Herausgegeben von
                     "Textdatenbank und Wörterbuch des Klassischen Maya", Arbeitsstelle der
                     Nordrhein-Westfälischen Akademie der Wissenschaft und Künste.
                     http://www.classicmayan.org, [Jahr], abgerufen am: [Datum des Abrufs]</bibl>
            <bibl xml:lang="es">Corpus digital del Maya Clásico. Editado por el centro de
                     investigación "Base de Datos de Texto y Diccionario del Maya Clásico" de la
                     Academia de Renania del Norte-Westfalia de las Ciencias y de las Artes.
                     http://www.classicmayan.org, [año], último acceso: [fecha del acceso]</bibl>
            <bibl>
              <availability>
                <licence target="http://creativecommons.org/licenses/by/4.0/">The corpus is
                           available under licence "CC BY 4.0". You are free to share (copy and
                           redistribute the material in any medium or format) and adapt (remix,
                           transform, and build upon the material for any purpose, even
                           commercially) under the following terms: Attribution: You must give
                           appropriate credit, provide a link to the license, and indicate if
                           changes were made. You may do so in any reasonable manner, but not in any
                           way that suggests the licensor endorses you or your use. No additional
                           restrictions: You may not apply legal terms or technological measures
                           that legally restrict others from doing anything the license permits.
                           Note that the images are licenced differently. </licence>
              </availability>
            </bibl>
          </listBibl>
        </p>
      </publicationStmt>
      <seriesStmt>
        <title xml:lang="en">Digital Corpus of Classic Mayan</title>
        <title xml:lang="de">Digitales Korpus des Klassischen Maya</title>
        <title xml:lang="es">Corpus digital del maya clásico</title>
        <respStmt>
          <resp key="edc">Editor of Digital Corpus</resp>
          <name>
            <orgName xml:lang="en">Text Database and Dictionary of Classic Mayan, Research
                     Center of the North Rhine-Westphalian Academy of Sciences, Humanities and the
                     Arts at the University of Bonn, Faculty of Philosophy</orgName>
            <orgName xml:lang="de">Textdatenbank und Wörterbuch des Klassischen Maya,
                     Arbeitsstelle der NRW Akademie der Wissenschaften und der Künste. Rheinische
                     Friedrich-Wilhelms-Universität Bonn, Abteilung für Altamerikanistik</orgName>
            <orgName xml:lang="es">Base de Datos de Texto y Diccionario del Maya Clásico,
                     Centro de Investigación de la Academia de Renania del Norte-Westfalia de las
                     Ciencias y de las Artes en la Universidad de Bonn</orgName>
            <idno type="GND" xml:base="http://d-nb.info/gnd/">1103701312</idno>
          </name>
        </respStmt>
        <respStmt>
          <resp key="pdr">Project Director</resp>
          <name xml:id="NG">
            <persName>Nikolai Grube</persName>
            <affiliation>Rheinische Friedrich-Wilhelms-Universität, Bonn</affiliation>
            <idno type="GND" xml:base="http://d-nb.info/gnd/">110726553</idno>
          </name>
        </respStmt>
        <respStmt>
          <resp key="oth">Project Coordinator</resp>
          <name xml:id="CP">
            <persName>Christian Prager</persName>
            <affiliation>Rheinische Friedrich-Wilhelms-Universität, Bonn</affiliation>
            <idno type="GND" xml:base="http://d-nb.info/gnd/">139962859</idno>
          </name>
        </respStmt>
        <respStmt>
          <resp key="oth">Research Assistants</resp>
          <name xml:id="SG">
            <persName>Sven Gronemeyer</persName>
            <affiliation>Rheinische Friedrich-Wilhelms-Universität, Bonn</affiliation>
            <affiliation>La Trobe University, Melbourne</affiliation>
            <idno type="GND" xml:base="http://d-nb.info/gnd/">138228450</idno>
          </name>
          <name xml:id="EW">
            <persName>Elisabeth Wagner</persName>
            <affiliation>Rheinische Friedrich-Wilhelms-Universität, Bonn</affiliation>
          </name>
          <name xml:id="KD">
            <persName>Katja Diederichs</persName>
            <affiliation>Rheinische Friedrich-Wilhelms-Universität, Bonn</affiliation>
          </name>
        </respStmt>
        <respStmt>
          <resp key="oth">Cooperation Partner</resp>
          <name>
            <orgName xml:lang="en">Göttingen State and University Library</orgName>
            <orgName xml:lang="de">Niedersächsische Staats- und Universitätsbibliothek
                     Göttingen</orgName>
            <affiliation>Niedersächsische Staats- und Universitätsbibliothek,
                     Göttingen</affiliation>
            <idno type="GND" xml:base="http://d-nb.info/gnd/">2020450-4</idno>
            <idno type="ISIL" xml:base="http://ld.zdb-services.de/resource/organisations/">DE-7</idno>
          </name>
        </respStmt>
        <respStmt>
          <resp key="oth">IT, Programming and Visualisation</resp>
          <name xml:id="MB">
            <persName>Maximilian Brodhun</persName>
            <affiliation>Niedersächsische Staats- und Universitätsbibliothek,
                     Göttingen</affiliation>
          </name>
        </respStmt>
        <respStmt>
          <resp key="oth">Metadata</resp>
          <name xml:id="FD">
            <persName>Franziska Diehr</persName>
            <affiliation>&gt;Niedersächsische Staats- und Universitätsbibliothek,
                     Göttingen</affiliation>
          </name>
        </respStmt>
        <respStmt>
          <resp key="oth">Sponsor</resp>
          <name>
            <orgName xml:lang="en">North Rhine-Westphalian Academy of Sciences, Humanities and
                     the Arts.</orgName>
            <orgName xml:lang="de">Nordrhein-Westfälische Akademie der Wissenschaften und der
                     Künste</orgName>
            <orgName xml:lang="es">Academia de Renania del Norte-Westfalia de las Ciencias y
                     de las Artes</orgName>
            <idno type="GND" xml:base="http://d-nb.info/gnd/">10370564-8</idno>
          </name>
        </respStmt>
        <respStmt>
          <resp key="oth">Sponsor</resp>
          <name>
            <orgName xml:lang="en">Union of the German Academies of Sciences and
                     Humanities</orgName>
            <orgName xml:lang="de">Union der deutschen Akademien der Wissenschaften</orgName>
            <orgName xml:lang="es"/>
            <idno type="GND" xml:base="http://d-nb.info/gnd/">5350721-6</idno>
          </name>
        </respStmt>
      </seriesStmt>
      <sourceDesc>
        <p>
          <msDesc>
            <msIdentifier>
              <msName>null</msName>
              <altIdentifier>
                <idno type="null" xml:base="null">null</idno>
              </altIdentifier>
            </msIdentifier>
          </msDesc>
        </p>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc xml:lang="en">
        <p> In 2014, the North Rhine-Westphalian Academy of Sciences, Humanities and the Arts
               established the "Text Database Dictionary of Classic Mayan" research center for the
               study of hieroglyphic writing and language of the ancient Maya at the University of
               Bonn’s Philosophy Faculty. The goal of this long term project is the analysis of all
               known hieroglyphic Mayan texts which will serve as the basis for the compilation and
               editing of a Classic Mayan language dictionary.</p>
      </projectDesc>
      <projectDesc xml:lang="de">
        <p> Im Jahr 2014 nahm die Arbeitsstelle "Textdatenbank und Wörterbuch des Klassischen
               Maya" der Nordrhein-Westfälischen Akademie der Wissenschaften und der Künste an der
               Rheinischen Friedrich- Wilhelms-Universität Bonn ihre Arbeit auf. Ziel des
               Langzeitprojekts ist zum einen die Erforschung der Hieroglyphenschrift der
               vorspanischen Mayakultur (500 v. Chr. bis 1500 n. Chr.), die sich auf dem Gebiet der
               heutigen Staaten Mexiko, Guatemala, Belize und Honduras entwickelte. Zum anderen
               steht der Aufbau eines Wörterbuches sowie einer korpusbasierten Datenbank des
               Klassischen Maya im Mittelpunkt des Forschungsvorhabens. </p>
      </projectDesc>
      <projectDesc xml:lang="es">
        <p> En 2014 fue fundado el centro de investigación "Base de Datos de Texto y Diccionario
               del Maya Clásico" (Textdatenbank und Wörterbuch des Klassischen Maya) de la Academia
               de Renania del Norte-Westfalia de las Ciencias y de las Artes (Nordrhein-Westfälische
               Akademie der Wissenschaften und der Künste) en la Universidad de Bonn. El primer
               objetivo del proyecto a largo plazo es el estudio de la escritura jeroglífica de la
               cultura maya prehispánica (500 a. C. hasta 1500 d. C.) que abarcó parte de los
               actuales territorios de México, Guatemala, Belice y Honduras. El segundo objetivo es
               compilar un diccionario y crear una base de datos del maya clásico.</p>
      </projectDesc>
      <editorialDecl xml:lang="en">
        <p> Due to the circumstance that there are no Unicode characters for Classic Mayan and
               furthermore that the reading of some Mayan glyphs is still not known and/or is the
               subject of research discussions, the digital corpus coded in TEI/XML cannot be given
               in either original script nor in phonemic-transcribed values. To enable the creation
               of a flexible corpus, which is able to react to different possible readings, we use a
               sign catalogue as basis for generating the textcorpus. This sign catalog was
               developed in the scope of this research project and is realised in RDF. The coded
               text basically consists of glyphs coded with the element "g". By using the attributes
               @n (for the number of the glyph in the sign catalogue) and @ref (for URI reference),
               a reference to the sign catalogue is given. In a further step of processing the
               TEI-coded text the readings of signs given from the sign catalogue can be extracted
               and the text becomes "readable". </p>
      </editorialDecl>
      <editorialDecl xml:lang="de">
        <p> Da es für die Hieroglyphenschrift des Klassischen Maya kein Unicode-Schriftsatz gibt
               und die Lesung vieler Zeichen noch nicht bekannt oder umstritten ist, kann das in
               TEI/XML kodierte Textkorpus weder aus Originalschreibung noch aus
               phonemisch-transkribierten Werten bestehen. Um ein flexibles Korpus zu erhalten, das
               auf neue Entzifferungen und auf verschiedenen Lesungshypothesen reagieren kann,
               benutzen wir den in RDF-vorliegenden Zeichenkatalog, der im Rahmen dieses
               Forschungsprojekts entstanden ist, als Grundlage und verknüpfen diesen mit dem
               Textkorpus. Innerhalb des kodierten Texts wird jede Glyphe mit dem Element "g"
               bezeichnet und mittels der Katalognummer (Attribut @n) und einer Referenz zu der
               entsprechenden URI im Zeichenkatalog (Attribut @ref) erfasst. Der Text wird erst
               durch die Abfrage der Daten aus dem Zeichenkatalog "lesbar". Dies geschieht in einem
               weiteren Prozessierungsschritt. </p>
      </editorialDecl>
      <editorialDecl xml:lang="es">
        <p> El corpus de texto anotado en TEI/XML no puede consistir ni en la grafía original ni
               en valores transcritos fonémicamente, puesto que no existe un conjunto de caracteres
               Unicode para la escritura maya, y por el hecho que la lectura de muchos glifos
               todavía es desconocida o discutida. Para desarrollar un corpus flexible que se
               adapte a nuevos desciframientos y diferentes hipótesis de lectura utilisamos el
               catálogo de signos mayas escrito en RDF como base. Fue formado en el margen de este
               proyecto y será vinculado con el corpus de texto. Dentro del texto codificado cada
               glífo será designado con el elemento "g" y registrado a través de un número de
               catálogo (atributo @n) y una referencia a la URI en el catálogo de signos (atributo
               @ref). Sólo a través de la consulta de datos el catálogo será "legible". Esto succede
               en un siguiente paso de procesamiento.</p>
      </editorialDecl>
    </encodingDesc>
  </teiHeader>
  <sourceDoc>
    <surface type="top_side" xml:id="a1">
      <zone corresp="#top_side" xml:id="a13">
        <zone corresp="#A1-B2" xml:id="a2"/>
        <zone corresp="#A3-B3" xml:id="a3"/>
        <zone corresp="#A4-B4" xml:id="a4"/>
        <zone corresp="#A5-B5" xml:id="a5"/>
        <zone corresp="#A6-B6" xml:id="a6"/>
        <zone corresp="#A7-B7" xml:id="a7"/>
        <zone corresp="#A8-B8" xml:id="a8"/>
        <zone corresp="#A9" xml:id="a9"/>
        <zone corresp="#B9" xml:id="a10"/>
        <zone corresp="#A10" xml:id="a11"/>
        <zone corresp="#B10" xml:id="a12"/>
        <zone corresp="#C1" xml:id="a14"/>
        <zone corresp="#D1" xml:id="a15"/>
        <zone corresp="#C2" xml:id="a16"/>
        <zone corresp="#D2" xml:id="a17"/>
        <zone corresp="#C3" xml:id="a18"/>
        <zone corresp="#D3" xml:id="a19"/>
        <zone corresp="#C4" xml:id="a20"/>
        <zone corresp="#D4" xml:id="a21"/>
        <zone corresp="#C5" xml:id="a22"/>
        <zone corresp="#D5" xml:id="a23"/>
        <zone corresp="#C6" xml:id="a24"/>
        <zone corresp="#D6" xml:id="a25"/>
        <zone corresp="#C7" xml:id="a26"/>
        <zone corresp="#D7" xml:id="a27"/>
        <zone corresp="#C8" xml:id="a28"/>
        <zone corresp="#D8" xml:id="a29"/>
        <zone corresp="#C9" xml:id="a30"/>
        <zone corresp="#D9" xml:id="a31"/>
        <zone corresp="#C10" xml:id="a32"/>
        <zone corresp="#D10" xml:id="a33"/>
        <zone corresp="#E1" xml:id="a34"/>
        <zone corresp="#F1" xml:id="a35"/>
        <zone corresp="#E2" xml:id="a36"/>
        <zone corresp="#F2" xml:id="a37"/>
        <zone corresp="#E3" xml:id="a38"/>
        <zone corresp="#F3" xml:id="a39"/>
        <zone corresp="#E4" xml:id="a40"/>
        <zone corresp="#F4" xml:id="a41"/>
        <zone corresp="#E5" xml:id="a42"/>
        <zone corresp="#F5" xml:id="a43"/>
        <zone corresp="#E6" xml:id="a44"/>
        <zone corresp="#F6" xml:id="a45"/>
        <zone corresp="#E7" xml:id="a46"/>
        <zone corresp="#F7" xml:id="a47"/>
        <zone corresp="#E8" xml:id="a48"/>
        <zone corresp="#F8" xml:id="a49"/>
        <zone corresp="#E9" xml:id="a50"/>
        <zone corresp="#F9" xml:id="a51"/>
        <zone corresp="#E10" xml:id="a52"/>
        <zone corresp="#F10" xml:id="a53"/>
        <zone corresp="#G1" xml:id="a54"/>
        <zone corresp="#H1" xml:id="a55"/>
        <zone corresp="#G2" xml:id="a56"/>
        <zone corresp="#H2" xml:id="a57"/>
        <zone corresp="#G3" xml:id="a58"/>
        <zone corresp="#H3" xml:id="a59"/>
        <zone corresp="#G4" xml:id="a60"/>
        <zone corresp="#H4" xml:id="a61"/>
        <zone corresp="#G5" xml:id="a62"/>
        <zone corresp="#H5" xml:id="a63"/>
        <zone corresp="#G6" xml:id="a64"/>
        <zone corresp="#H6" xml:id="a65"/>
        <zone corresp="#G7" xml:id="a66"/>
        <zone corresp="#H7" xml:id="a67"/>
        <zone corresp="#G8" xml:id="a68"/>
        <zone corresp="#H8" xml:id="a69"/>
        <zone corresp="#G9" xml:id="a70"/>
        <zone corresp="#H9" xml:id="a71"/>
        <zone corresp="#G10" xml:id="a72"/>
        <zone corresp="#H10" xml:id="a73"/>
        <zone corresp="#I1" xml:id="a74"/>
        <zone corresp="#J1" xml:id="a75"/>
        <zone corresp="#I2" xml:id="a76"/>
        <zone corresp="#J2" xml:id="a77"/>
        <zone corresp="#I3" xml:id="a78"/>
        <zone corresp="#J3" xml:id="a79"/>
        <zone corresp="#I4" xml:id="a80"/>
        <zone corresp="#J4" xml:id="a81"/>
        <zone corresp="#I5" xml:id="a82"/>
        <zone corresp="#J5" xml:id="a83"/>
        <zone corresp="#I6" xml:id="a84"/>
        <zone corresp="#J6" xml:id="a85"/>
        <zone corresp="#I7" xml:id="a86"/>
        <zone corresp="#J7" xml:id="a92"/>
        <zone corresp="#I8" xml:id="a87"/>
        <zone corresp="#J8" xml:id="a88"/>
        <zone corresp="#I9" xml:id="a89"/>
        <zone corresp="#J9" xml:id="a90"/>
        <zone corresp="#I10" xml:id="a91"/>
        <zone corresp="#J10" xml:id="a93"/>
        <zone corresp="#K1" xml:id="a94"/>
        <zone corresp="#L1" xml:id="a95"/>
        <zone corresp="#K2" xml:id="a96"/>
        <zone corresp="#L2" xml:id="a97"/>
        <zone corresp="#K3" xml:id="a98"/>
        <zone corresp="#L3" xml:id="a99"/>
        <zone corresp="#K4" xml:id="a100"/>
        <zone corresp="#L4" xml:id="a101"/>
        <zone corresp="#K5" xml:id="a102"/>
        <zone corresp="#L5" xml:id="a103"/>
        <zone corresp="#K6" xml:id="a104"/>
        <zone corresp="#L6" xml:id="a105"/>
        <zone corresp="#K7" xml:id="a106"/>
        <zone corresp="#L7" xml:id="a107"/>
        <zone corresp="#K8" xml:id="a108"/>
        <zone corresp="#L8" xml:id="a109"/>
        <zone corresp="#K9" xml:id="a110"/>
        <zone corresp="#L9" xml:id="a111"/>
        <zone corresp="#K10" xml:id="a112"/>
        <zone corresp="#L10" xml:id="a114"/>
        <zone corresp="#M1" xml:id="a113"/>
        <zone corresp="#N1" xml:id="a115"/>
        <zone corresp="#M2" xml:id="a116"/>
        <zone corresp="#N2" xml:id="a117"/>
        <zone corresp="#M3" xml:id="a118"/>
        <zone corresp="#N3" xml:id="a119"/>
        <zone corresp="#M4" xml:id="a120"/>
        <zone corresp="#N4" xml:id="a121"/>
        <zone corresp="#M5" xml:id="a122"/>
        <zone corresp="#N5" xml:id="a123"/>
        <zone corresp="#M6" xml:id="a124"/>
        <zone corresp="#N6" xml:id="a125"/>
        <zone corresp="#M7" xml:id="a126"/>
        <zone corresp="#N7" xml:id="a127"/>
        <zone corresp="#M8" xml:id="a128"/>
        <zone corresp="#N8" xml:id="a129"/>
        <zone corresp="#M9" xml:id="a130"/>
        <zone corresp="#N9" xml:id="a131"/>
        <zone corresp="#M10" xml:id="a132"/>
        <zone corresp="#N10" xml:id="a133"/>
        <zone corresp="#O1" xml:id="a134"/>
        <zone corresp="#P1" xml:id="a135"/>
        <zone corresp="#O2" xml:id="a136"/>
        <zone corresp="#P2" xml:id="a137"/>
        <zone corresp="#O3" xml:id="a138"/>
        <zone corresp="#P3" xml:id="a139"/>
        <zone corresp="#O4" xml:id="a140"/>
        <zone corresp="#P4" xml:id="a141"/>
        <zone corresp="#O5" xml:id="a142"/>
        <zone corresp="#P5" xml:id="a143"/>
        <zone corresp="#O6" xml:id="a144"/>
        <zone corresp="#P6" xml:id="a145"/>
        <zone corresp="#O7" xml:id="a146"/>
        <zone corresp="#P7" xml:id="a147"/>
        <zone corresp="#O8" xml:id="a148"/>
        <zone corresp="#P8" xml:id="a149"/>
        <zone corresp="#O9" xml:id="a150"/>
        <zone corresp="#P9" xml:id="a151"/>
        <zone corresp="#O10" xml:id="a152"/>
        <zone corresp="#P10" xml:id="a153"/>
      </zone>
    </surface>
    <surface type="front" xml:id="a154">
      <zone corresp="#front" xml:id="a155">
        <zone corresp="#Q1" xml:id="a159"/>
        <zone corresp="#Q2" xml:id="a158"/>
        <zone corresp="#Q3" xml:id="a157"/>
        <zone corresp="#Q4" xml:id="a156"/>
      </zone>
    </surface>
  </sourceDoc>
  <text>
    <body>
      <div type="textfield" xml:id="top_side">
        <ab xml:id="A1-B2" n="124st:[1600st°741st]:548bv" type="glyph-block">
          <g xml:id="A1-B2G1" n="124st" ref="textgrid:3r4t3" rend="above" corresp="#A1-B2S1"/>
          <seg xml:id="A1-B2S1" type="glyph-group" rend="beneath" corresp="#A1-B2G1">
            <g xml:id="A1-B2G2" n="1600st" ref="textgrid:3rcft" rend="encloses" corresp="#A1-B2G3"/>
            <g xml:id="A1-B2G3" n="741st" ref="textgrid:3rpvp" rend="infixed_in" corresp="#A1-B2G2"/>
          </seg>
          <g xml:id="A1-B2G4" n="548bv" ref="textgrid:3r8gk" rend="beneath" corresp="#A1-B2S1"/>
        </ab>
        <ab xml:id="A3-B3" n="5009st.1033st" type="glyph-block">
          <g xml:id="A3-B3G1" n="5009st" ref="textgrid:30gnx" rend="left_beside" corresp="#A3-B3G2"/>
          <g xml:id="A3-B3G2" n="1033st" ref="textgrid:3rqd4" rend="right_beside" corresp="#A3-B3G1"/>
        </ab>
        <ab xml:id="A4-B4" n="5009st.746st" type="glyph-block">
          <g xml:id="A4-B4G1" n="5009st" ref="textgrid:30gnx" rend="left_beside" corresp="#A4-B4G2"/>
          <g xml:id="A4-B4G2" n="746st" ref="textgrid:3rpvw" rend="right_beside" corresp="#A4-B4G1"/>
        </ab>
        <ab xml:id="A5-B5" n="5007st.1034st" type="glyph-block">
          <g xml:id="A5-B5G1" n="5007st" ref="textgrid:3rb7p" rend="left_beside" corresp="#A5-B5G2"/>
          <g xml:id="A5-B5G2" n="1034st" ref="textgrid:3rqdc" rend="right_beside" corresp="#A5-B5G1"/>
        </ab>
        <ab xml:id="A6-B6" n="5009st.1519st" type="glyph-block">
          <g xml:id="A6-B6G1" n="5009st" ref="textgrid:30gnx" rend="left_beside" corresp="#A6-B6G2"/>
          <g xml:id="A6-B6G2" n="1519st" ref="textgrid:3rqdf" rend="right_beside" corresp="#A6-B6G1"/>
        </ab>
        <ab xml:id="A7-B7" n="5010st.544st" type="glyph-block">
          <g xml:id="A7-B7G1" n="5010st" ref="textgrid:3rb7t" rend="left_beside" corresp="#A7-B7G2"/>
          <g xml:id="A7-B7G2" n="544st" ref="textgrid:3r8g7" rend="right_beside" corresp="#A7-B7G1"/>
        </ab>
        <ab xml:id="A8-B8" n="5010st.[1500bt°765st]" type="glyph-block">
          <g xml:id="A8-B8G1" n="5010st" ref="textgrid:3rb7t" rend="left_beside" corresp="#A8-B8S1"/>
          <seg xml:id="A8-B8S1" type="glyph-group" rend="right_beside" corresp="#A8-B8G1">
            <g xml:id="A8-B8G2" n="1500bt" ref="textgrid:3rcg2" rend="encloses" corresp="#A8-B8G3"/>
            <g xml:id="A8-B8G3" n="765st" ref="textgrid:3rrtw" rend="infixed_in" corresp="#A8-B8G2"/>
          </seg>
        </ab>
        <ab xml:id="A9" n="5009st.[32vt:670st]" type="glyph-block">
          <g xml:id="A9G1" n="5009st" ref="textgrid:30gnx" rend="left_beside" corresp="#A9S1"/>
          <seg xml:id="A9S1" type="glyph-group" rend="right_beside" corresp="#A9G1">
            <g xml:id="A9G2" n="32vt" ref="textgrid:3r3r7" rend="above" corresp="#A9G3"/>
            <g xml:id="A9G3" n="670st" ref="textgrid:3rdbj" rend="beneath" corresp="#A9G2"/>
          </seg>
        </ab>
        <ab xml:id="B9" n="128st:60st:23st" type="glyph-block">
          <g xml:id="B9G1" n="128st" ref="textgrid:3r4tg" rend="above" corresp="#B9G2"/>
          <g xml:id="B9G2" n="60st" ref="textgrid:30p06" rend="above" corresp="#B9G3"/>
          <g xml:id="B9G3" n="23st" ref="textgrid:2stz0" rend="beneath" corresp="#B9G2"/>
        </ab>
        <ab xml:id="A10" n="5007st.45st:??" type="glyph-block">
          <g xml:id="A10G1" n="5007st" ref="textgrid:3rb7p" rend="left_beside" corresp="#A10G2"/>
          <g xml:id="A10G2" n="45st" ref="textgrid:2ss1z" rend="right_beside" corresp="#A10G3"/>
          <damage agent="environment" degree="1" quantity="1" unit="g">
            <g xml:id="A10G3" rend="beneath" corresp="#A10G2"/>
          </damage>
        </ab>
        <ab xml:id="B10" n="5003st.[[*717hh.181br]:713bb]" type="glyph-block">
          <g xml:id="B10G1" n="5003st" ref="textgrid:3rb7c" rend="left_beside" corresp="#B10S1"/>
          <seg xml:id="B10S1" type="glyph-group" rend="right_beside" corresp="#B10G1">
            <seg xml:id="B10S2" type="glyph-group" rend="above" corresp="#B10G4">
              <damage agent="environment" degree="1" quantity="1" unit="g">
                <supplied reason="damage" evidence="external" precision="high" ana="#CP">
                  <g xml:id="B10G2" n="717hh" ref="textgrid:3rj61" rend="left_beside" corresp="#B10G3"/>
                </supplied>
              </damage>
              <g xml:id="B10G3" n="181br" ref="textgrid:3r4xm" rend="right_beside" corresp="#B10G2"/>
            </seg>
            <g xml:id="B10G4" n="713bb" ref="textgrid:3rhb6" rend="beneath" corresp="#B10S2"/>
          </seg>
        </ab>
        <ab xml:id="C1" n="1568ex.681st" type="glyph-block">
          <g xml:id="C1G1" n="1568ex" ref="textgrid:3rrv0" rend="left_beside" corresp="#C1G2"/>
          <g xml:id="C1G2" n="681st" ref="textgrid:3rgsz" rend="right_beside" corresp="#C1G1"/>
        </ab>
        <ab xml:id="D1" n="204bv" type="glyph-block">
          <g xml:id="D1G1" n="204bv" ref="textgrid:3r52q"/>
        </ab>
        <ab xml:id="C2" n="187:[758°110]" type="glyph-block">
          <g xml:id="C2G1" n="187" ref="textgrid:36zh3" rend="above" corresp="#C2S1"/>
          <seg xml:id="C2S1" type="glyph-group" rend="beneath" corresp="#C2G1">
            <g xml:id="C2G2" n="758" ref="textgrid:3rcgq" rend="encloses" corresp="#C2G3"/>
            <g xml:id="C2G3" n="110" ref="textgrid:3r4h5" rend="infixed_in" corresp="#C2G2"/>
          </seg>
        </ab>
        <ab xml:id="D2" n="1578st.1579st" type="glyph-block">
          <g xml:id="D2G1" n="1578st" ref="textgrid:3rq92" rend="left_beside" corresp="#D2G2"/>
          <g xml:id="D2G2" n="1579st" ref="textgrid:3rrv4" rend="right_beside" corresp="#D2G1"/>
        </ab>
        <ab xml:id="C3" n="5018st.[58st:186bv]" type="glyph-block">
          <g xml:id="C3G1" n="5018st" ref="textgrid:3rb90" rend="left_beside" corresp="#C3S1"/>
          <seg xml:id="C3S1" type="glyph-group" rend="right_beside" corresp="#C3G1">
            <g xml:id="C3G2" n="58st" ref="textgrid:30n1x" rend="above" corresp="#C3G3"/>
            <g xml:id="C3G3" n="186bv" ref="textgrid:3r512" rend="beneath" corresp="#C3G2"/>
          </seg>
        </ab>
        <ab xml:id="D3" n="??:[??.??]" type="glyph-block">
          <damage agent="environment" degree="1" quantity="1" unit="g">
            <g xml:id="D3G1" rend="above" corresp="#D3S1"/>
          </damage>
          <seg xml:id="D3S1" type="glyph-group" rend="beneath" corresp="#D3G1">
            <damage agent="environment" degree="1" quantity="1" unit="g">
              <g xml:id="D3G2" rend="left_beside" corresp="#D3G3"/>
            </damage>
            <damage agent="environment" degree="1" quantity="1" unit="g">
              <g xml:id="D3G3" rend="right_beside" corresp="#D3G2"/>
            </damage>
          </seg>
        </ab>
        <ab xml:id="C4" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="D4" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="C5" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="D5" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="C6" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="D6" n="5003st.??" type="glyph-block">
          <g xml:id="D6G1" n="5003st" ref="textgrid:3rb7c" rend="left_beside" corresp="#D6G2"/>
          <damage agent="environment" degree="1" quantity="1" unit="g">
            <g xml:id="D6G2" rend="right_beside" corresp="#D6G1"/>
          </damage>
        </ab>
        <ab xml:id="C7" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="D7" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="C8" n="5009st.[574st:23st]" type="glyph-block">
          <g xml:id="C8G1" n="5009st" ref="textgrid:30gnx" rend="left_beside" corresp="#C8S1"/>
          <seg xml:id="C8S1" type="glyph-group" rend="right_beside" corresp="#C8G1">
            <g xml:id="C8G2" n="574st" ref="textgrid:3r9g0" rend="above" corresp="#C8G3"/>
            <g xml:id="C8G3" n="23st" ref="textgrid:2stz0" rend="beneath" corresp="#C8G2"/>
          </seg>
        </ab>
        <ab xml:id="D8" n="5002st.[521st:??]" type="glyph-block">
          <g xml:id="D8G1" n="5002st" ref="textgrid:3rb79" rend="left_beside" corresp="#D8S1"/>
          <seg xml:id="D8S1" type="glyph-group" rend="right_beside" corresp="#D8G1">
            <g xml:id="D8G2" n="521st" ref="textgrid:3r8dj" rend="above" corresp="#D8G3"/>
            <damage agent="environment" degree="1" quantity="1" unit="g">
              <g xml:id="D8G3" rend="beneath" corresp="#D8G2"/>
            </damage>
          </seg>
        </ab>
        <ab xml:id="C9" n="5004st.[548bv:??]" type="glyph-block">
          <g xml:id="C9G1" n="5004st" ref="textgrid:3rb7f" rend="left_beside" corresp="#C9S1"/>
          <seg xml:id="C9S1" type="glyph-group" rend="right_beside" corresp="#C9G1">
            <g xml:id="C9G2" n="548bv" ref="textgrid:3r8gk" rend="above" corresp="#C9G3"/>
            <damage agent="environment" degree="1" quantity="1" unit="g">
              <g xml:id="C9G3" rend="beneath" corresp="#C9G2"/>
            </damage>
          </seg>
        </ab>
        <ab xml:id="D9" n="5001st.28ta" type="glyph-block">
          <g xml:id="D9G1" n="5001st" ref="textgrid:3rb77" rend="left_beside" corresp="#D9G2"/>
          <g xml:id="D9G2" n="28ta" ref="textgrid:3r3qn" rend="right_beside" corresp="#D9G1"/>
        </ab>
        <ab xml:id="C10" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="D10" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="E1" n="679st.[204bt:59st]" type="glyph-block">
          <g xml:id="E1G1" n="679st" ref="textgrid:3rcgk" rend="left_beside" corresp="#E1S1"/>
          <seg xml:id="E1S1" type="glyph-group" rend="right_beside" corresp="#E1G1">
            <g xml:id="E1G2" n="204bt" ref="textgrid:3r52p" rend="above" corresp="#E1G3"/>
            <g xml:id="E1G3" n="59st" ref="textgrid:30ng5" rend="beneath" corresp="#E1G2"/>
          </seg>
        </ab>
        <ab xml:id="F1" n="5002st.[1500bt°??]" type="glyph-block">
          <g xml:id="F1G1" n="5002st" ref="textgrid:3rb79" rend="left_beside" corresp="#F1S1"/>
          <seg xml:id="F1S1" type="glyph-group" rend="right_beside" corresp="#F1G1">
            <g xml:id="F1G2" n="1500bt" ref="textgrid:3rcg2" rend="encloses" corresp="#F1G3"/>
            <damage agent="environment" degree="1" quantity="1" unit="g">
              <g xml:id="F1G3" rend="infixed_in" corresp="#F1G2"/>
            </damage>
          </seg>
        </ab>
        <ab xml:id="E2" n="5017st.748st?" type="glyph-block">
          <g xml:id="E2G1" n="5017st" ref="textgrid:3rb8x" rend="left_beside" corresp="#E2G2"/>
          <damage agent="environment" degree="1" quantity="1" unit="g">
            <supplied reason="damage" evidence="external" precision="high" ana="#CP">
              <g xml:id="E2G2" n="748st" ref="textgrid:3rpw5" rend="right_beside" corresp="#E2G1"/>
            </supplied>
          </damage>
        </ab>
        <ab xml:id="F2" n="[*177bl:*507vc].*181br" type="glyph-block">
          <seg xml:id="F2S1" type="glyph-group" rend="left_beside" corresp="#F2G3">
            <damage agent="environment" degree="1" quantity="1" unit="g">
              <supplied reason="damage" evidence="external" precision="high" ana="#CP">
                <g xml:id="F2G1" n="177bl" ref="textgrid:3r4x1" rend="above" corresp="#F2G2"/>
              </supplied>
            </damage>
            <damage agent="environment" degree="1" quantity="1" unit="g">
              <supplied reason="damage" evidence="external" precision="high" ana="#CP">
                <g xml:id="F2G2" n="507vc" ref="textgrid:3r5zx" rend="beneath" corresp="#F2G1"/>
              </supplied>
            </damage>
          </seg>
          <damage agent="environment" degree="1" quantity="1" unit="g">
            <supplied reason="damage" evidence="external" precision="high" ana="#CP">
              <g xml:id="F2G3" n="181br" ref="textgrid:3r4xm" rend="right_beside" corresp="#F2S1"/>
            </supplied>
          </damage>
        </ab>
        <ab xml:id="E3" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="F3" n="4vl.[1540st:229bl]" type="glyph-block">
          <g xml:id="F3G1" n="4vl" ref="textgrid:2skk5" rend="left_beside" corresp="#F3S1"/>
          <seg xml:id="F3S1" type="glyph-group" rend="right_beside" corresp="#F3G1">
            <g xml:id="F3G2" n="1540st" ref="textgrid:3rq8h" rend="above" corresp="#F3G3"/>
            <g xml:id="F3G3" n="229bl" ref="textgrid:3r56h" rend="beneath" corresp="#F3G2"/>
          </seg>
        </ab>
        <ab xml:id="E4" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="F4" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="E5" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="F5" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="E6" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="F6" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="E7" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="F7" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="E8" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="F8" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="E9" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="F9" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="E10" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="F10" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="G1" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="H1" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="G2" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="H2" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="G3" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="H3" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="G4" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="H4" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="G5" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="H5" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="G6" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="H6" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="G7" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="H7" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="G8" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="H8" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="G9" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="H9" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="G10" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="H10" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="I1" n="124st:[1600st°*503vc]:548bv" type="glyph-block">
          <g xml:id="I1G1" n="124st" ref="textgrid:3r4t3" rend="above" corresp="#I1S1"/>
          <seg xml:id="I1S1" type="glyph-group" rend="beneath" corresp="#I1G1">
            <g xml:id="I1G2" n="1600st" ref="textgrid:3rcft" rend="encloses" corresp="#I1G3"/>
            <damage agent="environment" degree="1" quantity="1" unit="g">
              <supplied reason="damage" evidence="external" precision="high" ana="#CP">
                <g xml:id="I1G3" n="503vc" ref="textgrid:3r5zj" rend="infixed_in" corresp="#I1G2"/>
              </supplied>
            </damage>
          </seg>
          <g xml:id="I1G4" n="548bv" ref="textgrid:3r8gk" rend="beneath" corresp="#I1S1"/>
        </ab>
        <ab xml:id="J1" n="5009st.1033st" type="glyph-block">
          <g xml:id="J1G1" n="5009st" ref="textgrid:30gnx" rend="left_beside" corresp="#J1G2"/>
          <g xml:id="J1G2" n="1033st" ref="textgrid:3rqd4" rend="right_beside" corresp="#J1G1"/>
        </ab>
        <ab xml:id="I2" n="5010st.746st" type="glyph-block">
          <g xml:id="I2G1" n="5010st" ref="textgrid:3rb7t" rend="left_beside" corresp="#I2G2"/>
          <g xml:id="I2G2" n="746st" ref="textgrid:3rpvw" rend="right_beside" corresp="#I2G1"/>
        </ab>
        <ab xml:id="J2" n="5009st.1034st" type="glyph-block">
          <g xml:id="J2G1" n="5009st" ref="textgrid:30gnx" rend="left_beside" corresp="#J2G2"/>
          <g xml:id="J2G2" n="1034st" ref="textgrid:3rqdc" rend="right_beside" corresp="#J2G1"/>
        </ab>
        <ab xml:id="I3" n="5017st.1519st" type="glyph-block">
          <g xml:id="I3G1" n="5017st" ref="textgrid:3rb8x" rend="left_beside" corresp="#I3G2"/>
          <g xml:id="I3G2" n="1519st" ref="textgrid:3rqdf" rend="right_beside" corresp="#I3G1"/>
        </ab>
        <ab xml:id="J3" n="173st.[544st:116st]" type="glyph-block">
          <g xml:id="J3G1" n="173st" ref="textgrid:3r4wp" rend="left_beside" corresp="#J3S1"/>
          <seg xml:id="J3S1" type="glyph-group" rend="right_beside" corresp="#J3G1">
            <g xml:id="J3G2" n="544st" ref="textgrid:3r8g7" rend="above" corresp="#J3G3"/>
            <g xml:id="J3G3" n="116st" ref="textgrid:3r4sf" rend="beneath" corresp="#J3G2"/>
          </seg>
        </ab>
        <ab xml:id="I4" n="5006st.[1500bv°533st]" type="glyph-block">
          <g xml:id="I4G1" n="5006st" ref="textgrid:3rb7m" rend="left_beside" corresp="#I4S1"/>
          <seg xml:id="I4S1" type="glyph-group" rend="right_beside" corresp="#I4G1">
            <g xml:id="I4G2" n="1500bv" ref="textgrid:3rcg1" rend="encloses" corresp="#I4G3"/>
            <g xml:id="I4G3" n="533st" ref="textgrid:3r8ff" rend="infixed_in" corresp="#I4G2"/>
          </seg>
        </ab>
        <ab xml:id="J4" n="4vl.[128st:??]" type="glyph-block">
          <g xml:id="J4G1" n="4vl" ref="textgrid:2skk5" rend="left_beside" corresp="#J4S1"/>
          <seg xml:id="J4S1" type="glyph-group" rend="right_beside" corresp="#J4G1">
            <g xml:id="J4G2" n="128st" ref="textgrid:3r4tg" rend="above" corresp="#J4G3"/>
            <damage agent="environment" degree="1" quantity="1" unit="g">
              <g xml:id="J4G3" rend="beneath" corresp="#J4G2"/>
            </damage>
          </seg>
        </ab>
        <ab xml:id="I5" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="J5" n="1537bh:[24st.126do]" type="glyph-block">
          <g xml:id="J5G1" n="1537bh" ref="textgrid:3rrvd" rend="above" corresp="#J5S1"/>
          <seg xml:id="J5S1" type="glyph-group" rend="beneath" corresp="#J5G1">
            <g xml:id="J5G2" n="24st" ref="textgrid:2sm22" rend="left_beside" corresp="#J5G3"/>
            <g xml:id="J5G3" n="126do" ref="textgrid:3r4tc" rend="right_beside" corresp="#J5G2"/>
          </seg>
        </ab>
        <ab xml:id="I6" n="*5006st.[[??.181br]:*713bb]" type="glyph-block">
          <damage agent="environment" degree="1" quantity="1" unit="g">
            <supplied reason="damage" evidence="external" precision="high" ana="#CP">
              <g xml:id="I6G1" n="5006st" ref="textgrid:3rb7m" rend="left_beside" corresp="#I6S1"/>
            </supplied>
          </damage>
          <seg xml:id="I6S1" type="glyph-group" rend="right_beside" corresp="#I6G1">
            <seg xml:id="I6S2" type="glyph-group" rend="above" corresp="#I6G3">
              <damage agent="environment" degree="1" quantity="1" unit="g">
                <g xml:id="I6G2" rend="left_beside" corresp="#I6G3"/>
              </damage>
              <g xml:id="I6G3" n="181br" ref="textgrid:3r4xm" rend="right_beside" corresp="#I6G2"/>
            </seg>
            <damage agent="environment" degree="1" quantity="1" unit="g">
              <supplied reason="damage" evidence="external" precision="high" ana="#CP">
                <g xml:id="I6G4" n="713bb" ref="textgrid:3rhb6" rend="beneath" corresp="#I6S2"/>
              </supplied>
            </damage>
          </seg>
        </ab>
        <ab xml:id="J6" n="1568ex.681st" type="glyph-block">
          <g xml:id="J6G1" n="1568ex" ref="textgrid:3rrv0" rend="left_beside" corresp="#J6G2"/>
          <g xml:id="J6G2" n="681st" ref="textgrid:3rgsz" rend="right_beside" corresp="#J6G1"/>
        </ab>
        <ab xml:id="I7" n="[??.*187]:[*758°*110]" type="glyph-block">
          <seg xml:id="I7S1" type="glyph-group" rend="above" corresp="#I7S2">
            <damage agent="environment" degree="1" quantity="1" unit="g">
              <g xml:id="I7G1" rend="left_beside" corresp="#I7G2"/>
            </damage>
            <damage agent="environment" degree="1" quantity="1" unit="g">
              <supplied reason="damage" evidence="external" precision="high" ana="#CP">
                <g xml:id="I7G2" n="187" ref="textgrid:36zh3" rend="right_beside" corresp="#I7G1"/>
              </supplied>
            </damage>
          </seg>
          <seg xml:id="I7S2" type="glyph-group" rend="beneath" corresp="#I7S1">
            <damage agent="environment" degree="1" quantity="1" unit="g">
              <supplied reason="damage" evidence="external" precision="high" ana="#CP">
                <g xml:id="I7G3" n="758" ref="textgrid:3rcgq" rend="encloses" corresp="#I7G4"/>
              </supplied>
            </damage>
            <damage agent="environment" degree="1" quantity="1" unit="g">
              <supplied reason="damage" evidence="external" precision="high" ana="#CP">
                <g xml:id="I7G4" n="110" ref="textgrid:3r4h5" rend="infixed_in" corresp="#I7G3"/>
              </supplied>
            </damage>
          </seg>
        </ab>
        <ab xml:id="J7" n="1578st.5010st" type="glyph-block">
          <g xml:id="J7G1" n="1578st" ref="textgrid:3rq92" rend="left_beside" corresp="#J7G2"/>
          <g xml:id="J7G2" n="5010st" ref="textgrid:3rb7t" rend="right_beside" corresp="#J7G1"/>
        </ab>
        <ab xml:id="I8" n="*5018st.[*502:*25]" type="glyph-block">
          <damage agent="environment" degree="1" quantity="1" unit="g">
            <supplied reason="damage" evidence="external" precision="high" ana="#CP">
              <g xml:id="I8G1" n="5018st" ref="textgrid:3rb90" rend="left_beside" corresp="#I8S1"/>
            </supplied>
          </damage>
          <seg xml:id="I8S1" type="glyph-group" rend="right_beside" corresp="#I8G1">
            <damage agent="environment" degree="1" quantity="1" unit="g">
              <supplied reason="damage" evidence="external" precision="high" ana="#CP">
                <g xml:id="I8G2" n="502" ref="textgrid:2skz3" rend="above" corresp="#I8G3"/>
              </supplied>
            </damage>
            <damage agent="environment" degree="1" quantity="1" unit="g">
              <supplied reason="damage" evidence="external" precision="high" ana="#CP">
                <g xml:id="I8G3" n="25" ref="textgrid:327ts" rend="beneath" corresp="#I8G2"/>
              </supplied>
            </damage>
          </seg>
        </ab>
        <ab xml:id="J8" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="I9" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="J9" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="I10" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="J10" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="K1" n="5012st.[??:??]" type="glyph-block">
          <g xml:id="K1G1" n="5012st" ref="textgrid:3rb8k" rend="left_beside" corresp="#K1S1"/>
          <seg xml:id="K1S1" type="glyph-group" rend="right_beside" corresp="#K1G1">
            <damage agent="environment" degree="1" quantity="1" unit="g">
              <g xml:id="K1G2" rend="above" corresp="#K1G3"/>
            </damage>
            <damage agent="environment" degree="1" quantity="1" unit="g">
              <g xml:id="K1G3" rend="beneath" corresp="#K1G2"/>
            </damage>
          </seg>
        </ab>
        <ab xml:id="L1" n="5001st.[??:126dt]" type="glyph-block">
          <g xml:id="L1G1" n="5001st" ref="textgrid:3rb77" rend="left_beside" corresp="#L1S1"/>
          <seg xml:id="L1S1" type="glyph-group" rend="right_beside" corresp="#L1G1">
            <damage agent="environment" degree="1" quantity="1" unit="g">
              <g xml:id="L1G2" rend="above" corresp="#L1G3"/>
            </damage>
            <g xml:id="L1G3" n="126dt" ref="textgrid:3r4tb" rend="beneath" corresp="#L1G2"/>
          </seg>
        </ab>
        <ab xml:id="K2" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="L2" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="K3" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="L3" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="K4" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="L4" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="K5" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="L5" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="K6" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="L6" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="K7" n="4vl.[1540st:229bl]" type="glyph-block">
          <g xml:id="K7G1" n="4vl" ref="textgrid:2skk5" rend="left_beside" corresp="#K7S1"/>
          <seg xml:id="K7S1" type="glyph-group" rend="right_beside" corresp="#K7G1">
            <g xml:id="K7G2" n="1540st" ref="textgrid:3rq8h" rend="above" corresp="#K7G3"/>
            <g xml:id="K7G3" n="229bl" ref="textgrid:3r56h" rend="beneath" corresp="#K7G2"/>
          </seg>
        </ab>
        <ab xml:id="L7" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="K8" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="L8" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="K9" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="L9" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="K10" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="L10" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="M1" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="N1" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="M2" n="[1600st°*1536st]:548bt" type="glyph-block">
          <seg xml:id="M2S1" type="glyph-group" rend="above" corresp="#M2G3">
            <g xml:id="M2G1" n="1600st" ref="textgrid:3rcft" rend="encloses" corresp="#M2G2"/>
            <damage agent="environment" degree="1" quantity="1" unit="g">
              <supplied reason="damage" evidence="external" precision="high" ana="#CP">
                <g xml:id="M2G2" n="1536st" ref="textgrid:3rcfx" rend="infixed_in" corresp="#M2G1"/>
              </supplied>
            </damage>
          </seg>
          <g xml:id="M2G3" n="548bt" ref="textgrid:3r8gj" rend="beneath" corresp="#M2S1"/>
        </ab>
        <ab xml:id="N2" n="5009st.1033st" type="glyph-block">
          <g xml:id="N2G1" n="5009st" ref="textgrid:30gnx" rend="left_beside" corresp="#N2G2"/>
          <g xml:id="N2G2" n="1033st" ref="textgrid:3rqd4" rend="right_beside" corresp="#N2G1"/>
        </ab>
        <ab xml:id="M3" n="5010st.746st" type="glyph-block">
          <g xml:id="M3G1" n="5010st" ref="textgrid:3rb7t" rend="left_beside" corresp="#M3G2"/>
          <g xml:id="M3G2" n="746st" ref="textgrid:3rpvw" rend="right_beside" corresp="#M3G1"/>
        </ab>
        <ab xml:id="N3" n="5010st.1034st" type="glyph-block">
          <g xml:id="N3G1" n="5010st" ref="textgrid:3rb7t" rend="left_beside" corresp="#N3G2"/>
          <g xml:id="N3G2" n="1034st" ref="textgrid:3rqdc" rend="right_beside" corresp="#N3G1"/>
        </ab>
        <ab xml:id="M4" n="173st.1519st" type="glyph-block">
          <g xml:id="M4G1" n="173st" ref="textgrid:3r4wp" rend="left_beside" corresp="#M4G2"/>
          <g xml:id="M4G2" n="1519st" ref="textgrid:3rqdf" rend="right_beside" corresp="#M4G1"/>
        </ab>
        <ab xml:id="N4" n="173st.[544st:116st]" type="glyph-block">
          <g xml:id="N4G1" n="173st" ref="textgrid:3r4wp" rend="left_beside" corresp="#N4S1"/>
          <seg xml:id="N4S1" type="glyph-group" rend="right_beside" corresp="#N4G1">
            <g xml:id="N4G2" n="544st" ref="textgrid:3r8g7" rend="above" corresp="#N4G3"/>
            <g xml:id="N4G3" n="116st" ref="textgrid:3r4sf" rend="beneath" corresp="#N4G2"/>
          </seg>
        </ab>
        <ab xml:id="M5" n="5013st.[1500bv°533st]" type="glyph-block">
          <g xml:id="M5G1" n="5013st" ref="textgrid:3rb8n" rend="left_beside" corresp="#M5S1"/>
          <seg xml:id="M5S1" type="glyph-group" rend="right_beside" corresp="#M5G1">
            <g xml:id="M5G2" n="1500bv" ref="textgrid:3rcg1" rend="encloses" corresp="#M5G3"/>
            <g xml:id="M5G3" n="533st" ref="textgrid:3r8ff" rend="infixed_in" corresp="#M5G2"/>
          </seg>
        </ab>
        <ab xml:id="N5" n="128st:??" type="glyph-block">
          <g xml:id="N5G1" n="128st" ref="textgrid:3r4tg" rend="above" corresp="#N5G2"/>
          <damage agent="environment" degree="1" quantity="1" unit="g">
            <g xml:id="N5G2" rend="beneath" corresp="#N5G1"/>
          </damage>
        </ab>
        <ab xml:id="M6" n="*5006st.[1537bh:[*24st.*126do]]" type="glyph-block">
          <damage agent="environment" degree="1" quantity="1" unit="g">
            <supplied reason="damage" evidence="external" precision="high" ana="#CP">
              <g xml:id="M6G1" n="5006st" ref="textgrid:3rb7m" rend="left_beside" corresp="#M6S1"/>
            </supplied>
          </damage>
          <seg xml:id="M6S1" type="glyph-group" rend="right_beside" corresp="#M6G1">
            <g xml:id="M6G2" n="1537bh" ref="textgrid:3rrvd" rend="above" corresp="#M6S2"/>
            <seg xml:id="M6S2" type="glyph-group" rend="beneath" corresp="#M6G2">
              <damage agent="environment" degree="1" quantity="1" unit="g">
                <supplied reason="damage" evidence="external" precision="high" ana="#CP">
                  <g xml:id="M6G3" n="24st" ref="textgrid:2sm22" rend="left_beside" corresp="#M6G4"/>
                </supplied>
              </damage>
              <damage agent="environment" degree="1" quantity="1" unit="g">
                <supplied reason="damage" evidence="external" precision="high" ana="#CP">
                  <g xml:id="M6G4" n="126do" ref="textgrid:3r4tc" rend="right_beside" corresp="#M6G3"/>
                </supplied>
              </damage>
            </seg>
          </seg>
        </ab>
        <ab xml:id="N6" n="*5002st.[[??.181br]:*713bb]" type="glyph-block">
          <damage agent="environment" degree="1" quantity="1" unit="g">
            <supplied reason="damage" evidence="external" precision="high" ana="#CP">
              <g xml:id="N6G1" n="5002st" ref="textgrid:3rb79" rend="left_beside" corresp="#N6S1"/>
            </supplied>
          </damage>
          <seg xml:id="N6S1" type="glyph-group" rend="right_beside" corresp="#N6G1">
            <seg xml:id="N6S2" type="glyph-group" rend="above" corresp="#N6G3">
              <damage agent="environment" degree="1" quantity="1" unit="g">
                <g xml:id="N6G2" rend="left_beside" corresp="#N6G3"/>
              </damage>
              <g xml:id="N6G3" n="181br" ref="textgrid:3r4xm" rend="right_beside" corresp="#N6G2"/>
            </seg>
            <damage agent="environment" degree="1" quantity="1" unit="g">
              <supplied reason="damage" evidence="external" precision="high" ana="#CP">
                <g xml:id="N6G4" n="713bb" ref="textgrid:3rhb6" rend="beneath" corresp="#N6S2"/>
              </supplied>
            </damage>
          </seg>
        </ab>
        <ab xml:id="M7" n="*1568ex.*681st" type="glyph-block">
          <damage agent="environment" degree="1" quantity="1" unit="g">
            <supplied reason="damage" evidence="external" precision="high" ana="#CP">
              <g xml:id="M7G1" n="1568ex" ref="textgrid:3rrv0" rend="left_beside" corresp="#M7G2"/>
            </supplied>
          </damage>
          <damage agent="environment" degree="1" quantity="1" unit="g">
            <supplied reason="damage" evidence="external" precision="high" ana="#CP">
              <g xml:id="M7G2" n="681st" ref="textgrid:3rgsz" rend="right_beside" corresp="#M7G1"/>
            </supplied>
          </damage>
        </ab>
        <ab xml:id="N7" n="*1.[*187:[*758°*110]]" type="glyph-block">
          <damage agent="environment" degree="1" quantity="1" unit="g">
            <supplied reason="damage" evidence="external" precision="high" ana="#CP">
              <g xml:id="N7G1" n="1" ref="textgrid:2skhj" rend="left_beside" corresp="#N7S1"/>
            </supplied>
          </damage>
          <seg xml:id="N7S1" type="glyph-group" rend="right_beside" corresp="#N7G1">
            <damage agent="environment" degree="1" quantity="1" unit="g">
              <supplied reason="damage" evidence="external" precision="high" ana="#CP">
                <g xml:id="N7G2" n="187" ref="textgrid:36zh3" rend="above" corresp="#N7S2"/>
              </supplied>
            </damage>
            <seg xml:id="N7S2" type="glyph-group" rend="beneath" corresp="#N7G2">
              <damage agent="environment" degree="1" quantity="1" unit="g">
                <supplied reason="damage" evidence="external" precision="high" ana="#CP">
                  <g xml:id="N7G3" n="758" ref="textgrid:3rcgq" rend="encloses" corresp="#N7G4"/>
                </supplied>
              </damage>
              <damage agent="environment" degree="1" quantity="1" unit="g">
                <supplied reason="damage" evidence="external" precision="high" ana="#CP">
                  <g xml:id="N7G4" n="110" ref="textgrid:3r4h5" rend="infixed_in" corresp="#N7G3"/>
                </supplied>
              </damage>
            </seg>
          </seg>
        </ab>
        <ab xml:id="M8" n="1578st.5010st" type="glyph-block">
          <g xml:id="M8G1" n="1578st" ref="textgrid:3rq92" rend="left_beside" corresp="#M8G2"/>
          <g xml:id="M8G2" n="5010st" ref="textgrid:3rb7t" rend="right_beside" corresp="#M8G1"/>
        </ab>
        <ab xml:id="N8" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="M9" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="N9" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="M10" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="N10" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="O1" n="59st:606st:23st" type="glyph-block">
          <g xml:id="O1G1" n="59st" ref="textgrid:30ng5" rend="above" corresp="#O1G2"/>
          <g xml:id="O1G2" n="606st" ref="textgrid:3r9t2" rend="above" corresp="#O1G3"/>
          <g xml:id="O1G3" n="23st" ref="textgrid:2stz0" rend="beneath" corresp="#O1G2"/>
        </ab>
        <ab xml:id="P1" n="1535bv:130bh" type="glyph-block">
          <g xml:id="P1G1" n="1535bv" ref="textgrid:3rx6j" rend="above" corresp="#P1G2"/>
          <g xml:id="P1G2" n="130bh" ref="textgrid:3r4tk" rend="beneath" corresp="#P1G1"/>
        </ab>
        <ab xml:id="O2" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="P2" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="O3" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="P3" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="O4" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="P4" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="O5" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="P5" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="O6" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="P6" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="O7" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="P7" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="O8" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="P8" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="O9" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="P9" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="O10" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="P10" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
      </div>
      <div type="textfield" xml:id="front">
        <ab xml:id="Q1" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="Q2" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="Q3" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
        <ab xml:id="Q4" n="#" type="glyph-block">
          <gap reason="effaced" extent="unknown" unit="g"/>
        </ab>
      </div>
    </body>
  </text>
</TEI>
```

## Dependencies

This software does not contain any data it just creates data and saves it into the TextGrid Repository 
(non-public).


## Releasing a new version

For releasing a new version of TEI Generator, please have a look at the [DARIAH-DE Release Management Page](https://wiki.de.dariah.eu/display/DARIAH3/DARIAH-DE+Release+Management#DARIAHDEReleaseManagement-Gitlabflow/Gitflow(develop,main,featurebranchesundtags)) or see the [Gitlab CI file](.gitlab-ci.yml).
